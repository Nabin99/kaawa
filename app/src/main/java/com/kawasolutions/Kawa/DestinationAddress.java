package com.kawasolutions.Kawa;

import android.app.ProgressDialog;
import android.content.Context;
import android.graphics.Color;
import android.util.Log;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.Toast;

import com.directions.route.AbstractRouting;
import com.directions.route.Route;
import com.directions.route.RouteException;
import com.directions.route.Routing;
import com.directions.route.RoutingListener;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.android.gms.maps.model.Polyline;
import com.google.android.gms.maps.model.PolylineOptions;
import com.kawasolutions.Kawa.Activities.Home;
import com.kawasolutions.Kawa.Constants.Key;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by apple on 5/13/16.
 */
public class DestinationAddress implements GoogleMap.OnMarkerDragListener,RoutingListener{

    private GoogleMap mapView;
    private Context context;
    private Marker destinationMarker;
    private LatLng destinationLocation;
    private LatLng updatedLocation;
    private ProgressDialog progressDialog;
    private List<Polyline> polylines;
    private Marker mapMarker;

    public DestinationAddress(Context context, GoogleMap mapView,LatLng destinationLocation, LatLng updatedLocation) {
        this.mapView = mapView;
        this.context = context;
        this.destinationLocation = destinationLocation;
        this.updatedLocation = updatedLocation;
    }

    public void markDestination(){
        mapView.animateCamera(CameraUpdateFactory.newLatLngZoom(destinationLocation, 15));
        destinationMarker = mapView.addMarker(new MarkerOptions().title("Destination").position(destinationLocation));
        destinationMarker.setIcon(BitmapDescriptorFactory.defaultMarker(BitmapDescriptorFactory.HUE_BLUE));
        destinationMarker.setDraggable(true);
        if(destinationLocation != null && updatedLocation != Key.DefaultCoordinates) {
            showPath();
        }
        mapView.setOnMarkerDragListener(this);
    }

    @Override
    public void onMarkerDragStart(Marker marker) {

    }

    @Override
    public void onMarkerDrag(Marker marker) {

    }

    @Override
    public void onMarkerDragEnd(Marker marker) {
        destinationLocation = marker.getPosition();
        if(destinationLocation != null && updatedLocation != Key.DefaultCoordinates){
            showPath();
        }
    }

    public void showPath() {

        if (destinationLocation != null) {

            Routing routing = new Routing.Builder()
                .travelMode(Routing.TravelMode.DRIVING)
                .withListener(this)
                .waypoints(updatedLocation, destinationLocation)
                .build();
        routing.execute();
        }

    }

    @Override
    public void onRoutingFailure(RouteException e) {
        Toast.makeText(context, e.getLocalizedMessage(), Toast.LENGTH_SHORT).show();
    }

    @Override
    public void onRoutingStart() {

    }

    @Override
    public void onRoutingSuccess(ArrayList<Route> route, int shortestrouteindex) {
        if(polylines != null) {
            if (polylines.size() > 0) {
                for (Polyline poly : polylines) {
                    poly.remove();
                }
            }
        }

        polylines = new ArrayList<>();
        //add route(s) to the map.
      //  for (int i = 0; i <route.size(); i++) {

            //In case of more than 5 alternative routes

            PolylineOptions polyOptions = new PolylineOptions();
            polyOptions.width(20);
            polyOptions.color(context.getResources().getColor(R.color.colorLightBlue));
            polyOptions.addAll(route.get(shortestrouteindex).getPoints());
            Polyline polyline = mapView.addPolyline(polyOptions);
            polylines.add(polyline);

      //  }

    }

    @Override
    public void onRoutingCancelled() {
    }

    public void clearMap(){
        if(polylines != null) {
            if (polylines.size() > 0) {
                for (Polyline poly : polylines) {
                    poly.remove();
                }
            }
        }

        destinationMarker.remove();

    }
}
