package com.kawasolutions.Kawa.Services;

import android.app.Notification;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.app.ProgressDialog;
import android.app.Service;
import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.location.Location;
import android.media.RingtoneManager;
import android.net.Uri;
import android.os.Bundle;
import android.os.IBinder;
import android.support.annotation.Nullable;
import android.support.v4.app.NotificationCompat;
import android.support.v4.content.LocalBroadcastManager;
import android.util.Log;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.VolleyError;
import com.google.android.gms.maps.model.LatLng;
import com.google.gson.Gson;
import com.kawasolutions.Kawa.Activities.Home;
import com.kawasolutions.Kawa.Activities.MainActivity;
import com.kawasolutions.Kawa.Activities.RateDriverActivity;
import com.kawasolutions.Kawa.Activities.RideInfo;
import com.kawasolutions.Kawa.Constants.SharedPrefKey;
import com.kawasolutions.Kawa.Constants.URLS;
import com.kawasolutions.Kawa.CustomToast;
import com.kawasolutions.Kawa.Enum.RideStatus;
import com.kawasolutions.Kawa.Enum.TYPEREQUEST;
import com.kawasolutions.Kawa.Enum.ToastType;
import com.kawasolutions.Kawa.Models.EndOfRide;
import com.kawasolutions.Kawa.Models.RideActivity;
import com.kawasolutions.Kawa.Models.UserProfile;
import com.kawasolutions.Kawa.NetworkRequestTest;
import com.kawasolutions.Kawa.R;
import com.kawasolutions.Kawa.Utils.SharedPref;
import com.kawasolutions.Kawa.Utils.VolleyErrorMessage;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.Timer;
import java.util.TimerTask;

/**
 * Created by apple on 5/8/17.
 */

public class RideInformationService extends Service implements NetworkRequestTest.NetworkListenerTest {

    private Timer timer;
    private String TAG="RideInfoServiceClass";
    private Double driverLatitude;
    private Double driverLongitude;

    @Nullable
    @Override
    public IBinder onBind(Intent intent) {
        Log.d(TAG,"ON BIND");
        return null;
    }
    
    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        final int rideId = intent.getExtras().getInt("rideId");
        Log.d(TAG,"On Start" + rideId);
        timer = new Timer();
        timer.scheduleAtFixedRate(new TimerTask() {
            @Override
            public void run() {
                sendRideInformationRequest(rideId);
            }
        }, 0, 15000);
        super.onStartCommand(intent, flags, startId);
        return START_NOT_STICKY;
    }

    public void sendRideInformationRequest(int rideId){
        UserProfile user = SharedPref.getUserObject(this, SharedPrefKey.fileName);
        if (user != null) {
            String url = URLS.rideActivityStatus + rideId;
            HashMap<String, String> mHeaders = new HashMap<>();
            mHeaders.put("authToken", user.getAuthToken());
            NetworkRequestTest networkRequest = new NetworkRequestTest(this,url, Request.Method.GET,null,mHeaders,TYPEREQUEST.RIDEACTIVITYSTATUS,null,null);
            networkRequest.setListener(this);
            networkRequest.getResult();
        }
    }

    @Override
    public void receiveResult(JSONObject result, String url, Context context, TYPEREQUEST typeRequest, @Nullable String userInfo, @Nullable ProgressDialog progressDialog) {
        Log.d(TAG,"Response:" + result);
        if(typeRequest == TYPEREQUEST.RIDEACTIVITYSTATUS){
            try {
                JSONObject rideData = result.getJSONObject("currentRideDetails");

                Double newDriverLatitude = rideData.getDouble("driverLatitude");
                Double newDriverLongitude = rideData.getDouble("driverLongitude");
                Boolean startRideStatus = rideData.getBoolean("startRideStatus");
                Boolean successStatus = rideData.getBoolean("successStatus");
                Boolean cancelledStatus = rideData.getBoolean("cancelledStatus");
                Boolean activeRideStatus = rideData.getBoolean("activeRideStatus");
                Boolean expireStatus = rideData.getBoolean("expireStatus");


                RideActivity rideActivity = RideActivity.getInstance();
                rideActivity.setDriverLocation(new LatLng(newDriverLatitude,newDriverLongitude));
                rideActivity.setRideStarted(startRideStatus);
                rideActivity.setRideSuccess(successStatus);
                rideActivity.setRideCancelled(cancelledStatus);
                rideActivity.setRideActive(activeRideStatus);
                rideActivity.setExpireStatus(expireStatus);
                EndOfRide endOfRide = null;
                if(rideActivity.getRideSuccess()){
                    endOfRide = getRateRequestData(rideData);
                }
                updateUIFromRideActivity(rideActivity,endOfRide);

            } catch (JSONException e){
                e.printStackTrace();
            }
        }
    }

    @Override
    public void receiveError(VolleyError error, Context context, TYPEREQUEST typeRequest, @Nullable String userInfo, @Nullable ProgressDialog progressDialog) {
        CustomToast.getInstance().showToast(context, VolleyErrorMessage.handleVolleyErrors(context,error),ToastType.ERROR,Toast.LENGTH_SHORT);
    }

    public void updateUIFromRideActivity(RideActivity rideActivity,@Nullable EndOfRide endOfRide){
        if(rideActivity.getExpireStatus()){
            stopSelf();
            SharedPref.saveToPreferences(this,SharedPrefKey.fileName,SharedPrefKey.KEY_ON_RIDE,false);
            SharedPref.saveToPreferences(this,SharedPrefKey.fileName,SharedPrefKey.KEY_RIDE_DETAILS,"");
            showNotification("Your ride has expired due to some technical reason.");
        } else if(rideActivity.getRideCancelled()){
            stopSelf();
            SharedPref.saveToPreferences(this,SharedPrefKey.fileName,SharedPrefKey.KEY_ON_RIDE,false);
            SharedPref.saveToPreferences(this,SharedPrefKey.fileName,SharedPrefKey.KEY_RIDE_DETAILS,"");
            //showNotification("Your ride has been cancelled");
        }
         else if(rideActivity.getRideSuccess()){
            stopSelf();
            if(endOfRide == null){
                SharedPref.saveToPreferences(this,SharedPrefKey.fileName,SharedPrefKey.KEY_ON_RIDE,false);
                SharedPref.saveToPreferences(this,SharedPrefKey.fileName,SharedPrefKey.KEY_RIDE_DETAILS,"");
            } else {
                //Show Rate Activity only if user is logged in and it is directed to the current user
                UserProfile userProfile = SharedPref.getUserObject(getApplicationContext(), SharedPrefKey.fileName);
                //   Integer rateCustomerId = SharedPref.readIntegerFromPreference(getApplicationContext(),SharedPrefKey.fileName,SharedPrefKey.KEY_RATE_CUSTOMER_ID);
                if (userProfile != null) {
                    if (userProfile.getCustomerID().equals(endOfRide.getCustomerId())) {
                        saveRateDetailsToSharedPref(endOfRide);

                        //commented..continuous notification on app since driver is not rated in the app.
//                        showNotification("Thank you for using Kawa.");
                    } else {
                        Log.d("Rate Request Customer:", "Not intended for this user");
                    }
                } else {
                    Log.d("Rate Request Customer:", "No user logged in");
                }
            }
        }
        else if(rideActivity.getRideStarted()){
            SharedPref.saveToPreferences(this,SharedPrefKey.fileName,SharedPrefKey.KEY_RIDE_STARTED,true);
        }
        Intent i = new Intent("rideInformation");
        Gson gson = new Gson();
        i.putExtra("rideDetails", gson.toJson(rideActivity));
        if(endOfRide != null){
            i.putExtra("rateDetails",gson.toJson(endOfRide));
        }
        LocalBroadcastManager.getInstance(getApplicationContext()).sendBroadcast(i);
    }

    public EndOfRide getRateRequestData(JSONObject data){
        EndOfRide endOfRide = null;
        try {
            JSONObject rideDetails = data.getJSONObject("rideDetails");
            Integer rideId = rideDetails.getInt("rideId");
            String date = rideDetails.getString("rideDate");
            String chargeEstimate = rideDetails.getString("chargeEstimate");
            String meterPrice = rideDetails.getString("meterPrice");
            String startLocation = rideDetails.getString("startLocation");
            String endLocation = rideDetails.getString("endLocation");
            String distance = rideDetails.getString("distance");
            String rideMap = rideDetails.getString("staticMap");
            Boolean extendedRange = rideDetails.getBoolean("extendedRange");
            Double extendedRangePrice = rideDetails.getDouble("extendedPrice");

            JSONObject customerDetails = rideDetails.getJSONObject("customerId");
            Integer customerId = customerDetails.getInt("customerId");

            JSONObject driverDetails = rideDetails.getJSONObject("driverId");
            String driverName = driverDetails.getString("firstName") + " " + driverDetails.getString("lastName");
            String driverImage = driverDetails.getString("image");

            JSONObject vehicleDetails = driverDetails.getJSONObject("vehicleId");
            String carNumber = vehicleDetails.getString("vehicleNo");


            endOfRide = new EndOfRide();
            endOfRide.setRideId(rideId);
            endOfRide.setDriverName(driverName);
            endOfRide.setStartLocation(startLocation);
            endOfRide.setEndLocation(endLocation);
            endOfRide.setCarNumber(carNumber);
            endOfRide.setDate(date);
            endOfRide.setDistance(distance);
            endOfRide.setChargeEstimate(chargeEstimate);
            endOfRide.setDriverImage(driverImage);
            endOfRide.setMeterPrice(meterPrice);
            endOfRide.setCustomerId(customerId);
            endOfRide.setRideMap(rideMap);
            endOfRide.setExtendedRange(extendedRange);
            endOfRide.setExtendedRangePrice(extendedRangePrice);

        } catch (JSONException e) {
            e.printStackTrace();
        }
        return endOfRide;
    }

    public void saveRateDetailsToSharedPref(EndOfRide endOfRide){
        Gson gson = new Gson();
        String rateDetailsString = gson.toJson(endOfRide);
        SharedPref.saveToPreferences(getApplicationContext(), SharedPrefKey.fileName,SharedPrefKey.KEY_RATE_REQUEST,true);
        SharedPref.saveToPreferences(getApplicationContext(),SharedPrefKey.fileName,SharedPrefKey.KEY_RATE_DETAILS,rateDetailsString);
        SharedPref.saveToPreferences(getApplicationContext(), SharedPrefKey.fileName,SharedPrefKey.KEY_RATE_CUSTOMER_ID,endOfRide.getCustomerId());
    }

    public void showNotification(String message){
        Intent intent = new Intent(this, MainActivity.class);
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_NEW_TASK);

        PendingIntent pendingIntent = PendingIntent.getActivity(this, 0 /* Request code */, intent,
                PendingIntent.FLAG_ONE_SHOT);

        Uri defaultSoundUri = RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION);
        NotificationCompat.Builder notificationBuilder = new NotificationCompat.Builder(this)
                .setSmallIcon(R.drawable.taxi_marker)
                .setLargeIcon(getLargeTaxiIcon())
                .setContentTitle("Kawa")
                .setStyle(new NotificationCompat.BigTextStyle().bigText(message))
                .setContentText(message)
                .setAutoCancel(true)
                .setSound(defaultSoundUri)
                .setPriority(Notification.PRIORITY_MAX)
                .setContentIntent(pendingIntent);

        NotificationManager notificationManager =
                (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);

        notificationManager.notify(0, notificationBuilder.build());
    }

    private Bitmap getLargeTaxiIcon(){
        return BitmapFactory.decodeResource(getResources(),R.drawable.taxi_marker);
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        if(timer != null){
            timer.cancel();
            timer.purge();
        }
    }
}
