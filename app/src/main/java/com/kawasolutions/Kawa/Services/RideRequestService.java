package com.kawasolutions.Kawa.Services;

import android.app.Notification;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.app.ProgressDialog;
import android.app.Service;
import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.media.RingtoneManager;
import android.net.Uri;
import android.os.Bundle;
import android.os.IBinder;
import android.support.annotation.Nullable;
import android.support.v4.app.NotificationCompat;
import android.support.v4.content.LocalBroadcastManager;
import android.util.Log;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.VolleyError;
import com.google.android.gms.maps.model.LatLng;
import com.google.gson.Gson;
import com.kawasolutions.Kawa.Activities.BulkNotificationActivity;
import com.kawasolutions.Kawa.Activities.MainActivity;
import com.kawasolutions.Kawa.Constants.Key;
import com.kawasolutions.Kawa.Constants.SharedPrefKey;
import com.kawasolutions.Kawa.Constants.URLS;
import com.kawasolutions.Kawa.Enum.RideStatus;
import com.kawasolutions.Kawa.Enum.TYPEREQUEST;
import com.kawasolutions.Kawa.Models.DriverProfile;
import com.kawasolutions.Kawa.Models.EndOfRide;
import com.kawasolutions.Kawa.Models.RideActivity;
import com.kawasolutions.Kawa.Models.RideDetails;
import com.kawasolutions.Kawa.Models.UserProfile;
import com.kawasolutions.Kawa.NetworkRequest;
import com.kawasolutions.Kawa.NetworkRequestTest;
import com.kawasolutions.Kawa.R;
import com.kawasolutions.Kawa.Utils.SharedPref;
import com.kawasolutions.Kawa.Utils.VolleyErrorMessage;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.Timer;
import java.util.TimerTask;

/**
 * Created by apple on 4/27/17.
 */

public class RideRequestService extends Service implements NetworkRequestTest.NetworkListenerTest{

   private Timer timer;
    private String TAG="ServiceClass";
    @Nullable
    @Override
    public IBinder onBind(Intent intent) {
        Log.d(TAG,"On BIND");
        return null;
    }

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        final int rideId = intent.getExtras().getInt("rideId");
            timer = new Timer();
            timer.scheduleAtFixedRate(new TimerTask() {
                @Override
                public void run() {
                    sendRideInformationRequest(rideId);
                }
            }, 0, 4000);
        super.onStartCommand(intent, flags, startId);
        return START_REDELIVER_INTENT;
    }

    @Override
    public void onCreate() {
        super.onCreate();
        //Save the ride request so that user cannot request another ride until previous ride expires
        SharedPref.saveToPreferences(this,SharedPrefKey.fileName,SharedPrefKey.KEY_RIDE_REQUEST,true);
        Log.d(TAG,"On CREATE");
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        //remove the current ride request when service is destroyed
        SharedPref.saveToPreferences(this,SharedPrefKey.fileName,SharedPrefKey.KEY_RIDE_REQUEST,false);
        if(timer != null){
            timer.cancel();
            timer.purge();
        }
        Log.d(TAG,"On DESTROY");

    }



    public void sendRideInformationRequest(int rideId){
        Log.d("RideInformationSErvice:","Started");
        UserProfile user = SharedPref.getUserObject(this, SharedPrefKey.fileName);
        if (user != null) {
            String url = URLS.rideActivityStatus + rideId;
            HashMap<String, String> mHeaders = new HashMap<>();
            mHeaders.put("authToken", user.getAuthToken());
            NetworkRequestTest networkRequest = new NetworkRequestTest(this,url, Request.Method.GET,null,mHeaders,TYPEREQUEST.RIDEACTIVITYSTATUS,null,null);
            networkRequest.setListener(this);
            networkRequest.getResult();
        }
    }

    @Override
    public void receiveResult(JSONObject result, String url, Context context, TYPEREQUEST typeRequest, @Nullable String userInfo, @Nullable ProgressDialog progressDialog) {
       Log.d("RideInformation:","" + result);
        try{
            if (result != null){
                try {
                    JSONObject rideData = result.getJSONObject("currentRideDetails");

                    Boolean startRideStatus = rideData.getBoolean("startRideStatus");
                    Boolean successStatus = rideData.getBoolean("successStatus");
                    Boolean cancelledStatus = rideData.getBoolean("cancelledStatus");
                    Boolean activeRideStatus = rideData.getBoolean("activeRideStatus");
                    Boolean expireStatus = rideData.getBoolean("expireStatus");
                    Boolean acceptedStatus = rideData.getBoolean("acceptStatus");

                    if(expireStatus || cancelledStatus){
                        stopSelf();
                        showNotification("All nearby drivers are busy but one should free up soon. Check back in a minute.");
                        Intent intent = new Intent("rideActivity");
                        intent.putExtra("status", RideStatus.EXPIRED.toString());
                        LocalBroadcastManager.getInstance(getApplicationContext()).sendBroadcast(intent);
                    } else if(acceptedStatus){
                        stopSelf();

                        JSONObject data = rideData.getJSONObject("driverInfo");
                        RideDetails rideDetails = getRideDetails(data);
                        saveDetailsToSharedPref(rideDetails);
                        showNotification(rideDetails.getDriverProfile().getFirstname() + " has accepted your request.");
                        Intent i = new Intent("rideActivity");
                        i.putExtra("status", RideStatus.ACCEPTED.toString());
                        Gson gson = new Gson();
                        i.putExtra("rideDetails", gson.toJson(rideDetails));
                        LocalBroadcastManager.getInstance(getApplicationContext()).sendBroadcast(i);
                    }

                } catch (JSONException e){
                    e.printStackTrace();
                }

            }

        }catch (Exception e){
            Log.d("ExceptionUser:" ,"" + e.toString());
        }
    }

    @Override
    public void receiveError(VolleyError error, Context context, TYPEREQUEST typeRequest, @Nullable String userInfo, @Nullable ProgressDialog progressDialog) {
    }

    public void showNotification(String message){
        Intent intent = new Intent(this, MainActivity.class);
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_NEW_TASK);

        PendingIntent pendingIntent = PendingIntent.getActivity(this, 0 /* Request code */, intent,
                PendingIntent.FLAG_ONE_SHOT);

        Uri defaultSoundUri = RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION);
        NotificationCompat.Builder notificationBuilder = new NotificationCompat.Builder(this)
                .setSmallIcon(R.drawable.taxi_marker)
                .setLargeIcon(getLargeTaxiIcon())
                .setContentTitle("Kawa")
                .setStyle(new NotificationCompat.BigTextStyle().bigText(message))
                .setContentText(message)
                .setAutoCancel(true)
                .setSound(defaultSoundUri)
                .setPriority(Notification.PRIORITY_MAX)
                .setContentIntent(pendingIntent);

        NotificationManager notificationManager =
                (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);

        notificationManager.notify(0, notificationBuilder.build());
    }

    public RideDetails getRideDetails(JSONObject data){
        Log.d("RideInformation333:","" + data);
        Gson gson = new Gson();
        RideDetails rideDetails = gson.fromJson(data.toString(),RideDetails.class);
        return rideDetails;
    }

    public void saveDetailsToSharedPref(RideDetails rideDetails){
        Gson gson = new Gson();
        String rideDetailsString = gson.toJson(rideDetails);
        SharedPref.saveToPreferences(getApplicationContext(), SharedPrefKey.fileName,SharedPrefKey.KEY_ON_RIDE,true);
        SharedPref.saveToPreferences(getApplicationContext(),SharedPrefKey.fileName,SharedPrefKey.KEY_RIDE_DETAILS,rideDetailsString);

        UserProfile userProfile = SharedPref.getUserObject(getApplicationContext(), SharedPrefKey.fileName);
        if(userProfile != null) {
            SharedPref.saveToPreferences(getApplicationContext(), SharedPrefKey.fileName,SharedPrefKey.KEY_RIDE_CUSTOMER_ID,userProfile.getCustomerID());
        }
    }

    private Bitmap getLargeTaxiIcon(){
        return BitmapFactory.decodeResource(getResources(),R.drawable.taxi_marker);
    }
}
