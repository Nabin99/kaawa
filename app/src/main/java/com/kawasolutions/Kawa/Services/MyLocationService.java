package com.kawasolutions.Kawa.Services;

import android.Manifest;
import android.app.ProgressDialog;
import android.app.Service;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.location.Location;
import android.location.LocationManager;
import android.os.Bundle;
import android.os.IBinder;
import android.support.annotation.Nullable;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.LocalBroadcastManager;
import android.util.Log;

import com.android.volley.Request;
import com.android.volley.VolleyError;
import com.kawasolutions.Kawa.Constants.SharedPrefKey;
import com.kawasolutions.Kawa.Constants.URLS;
import com.kawasolutions.Kawa.Enum.TYPEREQUEST;
import com.kawasolutions.Kawa.Models.UserProfile;
import com.kawasolutions.Kawa.NetworkRequestTest;
import com.kawasolutions.Kawa.Utils.SharedPref;

import org.json.JSONObject;

import java.util.HashMap;

/**
 * Created by apple on 4/29/16.
 */
public class MyLocationService extends Service implements NetworkRequestTest.NetworkListenerTest {

    private static final String TAG = "MyLocationService";
    private LocationManager mLocationManager = null;
    private static final int LOCATION_INTERVAL = 15000;
    private static final float LOCATION_DISTANCE = 0f;

    private class LocationListener implements android.location.LocationListener {

        Location mLastLocation;

        public LocationListener(String provider) {
            Log.e(TAG, "LocationListener: " + provider);
            mLastLocation = new Location(provider);
        }

        @Override
        public void onLocationChanged(Location location) {
            Log.e(TAG, "onLocationChanged: " + location);
            mLastLocation.set(location);
            updateLocation(location);
        }

        @Override
        public void onStatusChanged(String provider, int status, Bundle extras) {
            Log.e(TAG, "onStatusChanged: " + provider);

        }

        @Override
        public void onProviderEnabled(String provider) {
            Log.e(TAG, "onProviderEnabled: " + provider);
        }

        @Override
        public void onProviderDisabled(String provider) {
            Log.e(TAG, "onProviderDisabled: " + provider);
        }
    }

    LocationListener[] mLocationListeners = new LocationListener[]{
            new LocationListener(LocationManager.GPS_PROVIDER),
            new LocationListener(LocationManager.NETWORK_PROVIDER)
    };


    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        Log.e(TAG, "onStartCommand");
        super.onStartCommand(intent, flags, startId);
        return START_NOT_STICKY;
    }

    @Override
    public void onCreate() {
        Log.e(TAG, "onCreate");
        initializeLocationManager();

        try{
            mLocationManager.requestLocationUpdates(LocationManager.NETWORK_PROVIDER,LOCATION_INTERVAL,LOCATION_DISTANCE,mLocationListeners[1]);
            mLocationManager.requestLocationUpdates(LocationManager.GPS_PROVIDER,LOCATION_INTERVAL,LOCATION_DISTANCE,mLocationListeners[0]);
        } catch (SecurityException e){
            Log.i(TAG, "fail to request location update, ignore", e);
        } catch (IllegalArgumentException e){
            Log.i(TAG, "network provider doesnot exist", e);
        }
    }

    @Override
    public void onDestroy() {
        Log.e(TAG, "onDestroy");
        super.onDestroy();
        if (mLocationManager != null) {
            for(int i = 0; i < mLocationListeners.length; i++){
                try {
                    if (ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
                        return;
                    }
                    mLocationManager.removeUpdates(mLocationListeners[i]);
                } catch (Exception e){
                    Log.i(TAG, "fail to remove location listener, ignore", e);
                }
            }
        }
    }

    @Nullable
    @Override
    public IBinder onBind(Intent intent) {
        return null;
    }

    public void initializeLocationManager(){
        Log.e(TAG, "initializeLocationManager - LOCATION_INTERVAL: "+ LOCATION_INTERVAL + " LOCATION_DISTANCE: " + LOCATION_DISTANCE);

        if(mLocationManager == null){
            mLocationManager = (LocationManager) getApplicationContext().getSystemService(Context.LOCATION_SERVICE);
        }
    }


    @Override
    public void receiveResult(JSONObject result, String url, Context context, TYPEREQUEST typeRequest, @Nullable String userInfo, @Nullable ProgressDialog progressDialog) {
        if(typeRequest == TYPEREQUEST.UPDATE_USER_LOCATION){
            Log.d("Driver Location update:","" + result);
            //check if driver is online or not
            try{
                if (result != null){
                    if (result.has("success")){
                        Integer success = (Integer) result.get("success");
                        if (success == 1){
                            //
                        }
                    }
                }

            }catch (Exception e){
                Log.d("ExceptionDriver:" ,"" + e.toString());
            }
        }
    }

    @Override
    public void receiveError(VolleyError error, Context context, TYPEREQUEST typeRequest, @Nullable String userInfo, @Nullable ProgressDialog progressDialog) {
        if(typeRequest == TYPEREQUEST.UPDATE_USER_LOCATION){

        }
    }

    public void updateLocation(Location location) {

        Double lat = location.getLatitude();
        Double longitude = location.getLongitude();

        UserProfile userProfile = SharedPref.getUserObject(this, SharedPrefKey.fileName);
        if (userProfile != null) {
            HashMap<String, String> mHeaders = new HashMap<>();
            mHeaders.put("authToken", userProfile.getAuthToken());

            HashMap<String, Object> params = new HashMap<>();
            params.put("customerId", userProfile.getCustomerID());
            params.put("latitude", lat);
            params.put("longitude", longitude);

            NetworkRequestTest networkRequestTest = new NetworkRequestTest(this,URLS.updateUserLocation, Request.Method.POST,params,mHeaders, TYPEREQUEST.UPDATE_USER_LOCATION,null,null);
            networkRequestTest.setListener(this);
            networkRequestTest.getResult();
        }
    }
}
