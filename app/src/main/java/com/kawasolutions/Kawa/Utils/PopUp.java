package com.kawasolutions.Kawa.Utils;

import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.support.annotation.Nullable;
import android.support.design.widget.Snackbar;
import android.util.Log;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.Volley;
import com.kawasolutions.Kawa.Activities.Home;
import com.kawasolutions.Kawa.Activities.MainActivity;
import com.kawasolutions.Kawa.Constants.Key;
import com.kawasolutions.Kawa.Constants.SharedPrefKey;
import com.kawasolutions.Kawa.Constants.URLS;
import com.kawasolutions.Kawa.CustomToast;
import com.kawasolutions.Kawa.Enum.TYPEREQUEST;
import com.kawasolutions.Kawa.Enum.ToastType;
import com.kawasolutions.Kawa.Models.UserProfile;
import com.kawasolutions.Kawa.NetworkRequest;
import com.kawasolutions.Kawa.NetworkRequestTest;
import com.kawasolutions.Kawa.R;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;

/**
 * Created by apple on 4/22/16.
 */
public class PopUp implements NetworkRequestTest.NetworkListenerTest{

    private static ProgressDialog progressDialog;
    private static Snackbar snackbar;

    public void popUpMenu(Context context, int id){

        switch(id){

            case R.id.log_out:
                logout(context);
                break;

            default:
                break;
        }
    }


    public void logout(final Context context){
        AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(context);
        alertDialogBuilder.setTitle("Log Out");
        alertDialogBuilder.setMessage("Are you sure you want to log out?");

        alertDialogBuilder.setPositiveButton("LOG OUT", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                logUserOut(context);
                dialog.dismiss();
            }
        });
        alertDialogBuilder.setNegativeButton("CANCEL", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.dismiss();
            }
        });
        AlertDialog alertDialog = alertDialogBuilder.create();
        alertDialog.show();
    }

    public void logUserOut(Context context){
        UserProfile userProfile = SharedPref.getUserObject(context, SharedPrefKey.fileName);
        if(userProfile != null) {
            String url = URLS.logout + userProfile.getCustomerID() ;
            progressDialog = new ProgressDialog(context);
            progressDialog.setMessage("Logging out...");
            progressDialog.setCancelable(false);
            NetworkRequestTest networkRequestTest = new NetworkRequestTest(context,url, Request.Method.GET,null,null,TYPEREQUEST.LOG_OUT,progressDialog,null);
            networkRequestTest.setListener(this);
            networkRequestTest.getResult();
        }
    }

    public static void cancelRide(Context context){


    }

    @Override
    public void receiveResult(JSONObject result, String url, Context context, TYPEREQUEST typeRequest, @Nullable String userInfo, @Nullable ProgressDialog progressDialog) {
        if(progressDialog != null){
            progressDialog.dismiss();
        }
        if(typeRequest == TYPEREQUEST.LOG_OUT){
            try {
                if (result != null) {
                    if (result.has(Key.success)) {

                        Integer success = (Integer) result.get(Key.success);
                        if (success == 1) {
                            SharedPref.saveToPreferences(context, SharedPrefKey.fileName,SharedPrefKey.KEY_USERPROFILE,"");
                            context.startActivity(new Intent(context, MainActivity.class));
                            ((Home) context).finish();
                        } else {
                            CustomToast.getInstance().showToast(context,"Something went wrong...", ToastType.ERROR,Toast.LENGTH_SHORT);
                        }
                    }
                }
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }
    }

    @Override
    public void receiveError(VolleyError error, Context context, TYPEREQUEST typeRequest, @Nullable String userInfo, @Nullable ProgressDialog progressDialog) {
        if(progressDialog != null){
            progressDialog.dismiss();
        }
        CustomToast.getInstance().showToast(context, VolleyErrorMessage.handleVolleyErrors(context,error), ToastType.ERROR, Toast.LENGTH_SHORT);
    }
}
