package com.kawasolutions.Kawa;

import android.app.ProgressDialog;
import android.content.Context;
import android.graphics.Color;
import android.support.annotation.Nullable;
import android.widget.Toast;

import com.directions.route.Route;
import com.directions.route.RouteException;
import com.directions.route.Routing;
import com.directions.route.RoutingListener;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.android.gms.maps.model.Polyline;
import com.google.android.gms.maps.model.PolylineOptions;
import com.kawasolutions.Kawa.Constants.Key;
import com.kawasolutions.Kawa.Enum.ToastType;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by apple on 5/5/16.
 */
public class MapFunction implements RoutingListener{

    private Context context;
    private GoogleMap map;
    private LatLng driverLocation;
    @Nullable
    private LatLng destinationLocation;
    private Marker marker;
    private LatLng myLocation;
    private List<Polyline> polylines;

    public MapFunction(Context context, GoogleMap map, LatLng myLocation, LatLng driverLocation,@Nullable LatLng destinationLocation){
        this.context = context;
        this.map = map;
        this.myLocation = myLocation;
        this.driverLocation = driverLocation;
        this.destinationLocation = destinationLocation;
    }

    public void showPathWithDestination(){

        Routing routing = new Routing.Builder()
                .travelMode(Routing.TravelMode.DRIVING)
                .withListener(this)
                .waypoints(myLocation,driverLocation, destinationLocation)
                .build();
        routing.execute();
    }

    public void showPathToDriver(){
        Routing routing = new Routing.Builder()
                .travelMode(Routing.TravelMode.DRIVING)
                .withListener(this)
                .waypoints(myLocation,driverLocation)
                .build();
        routing.execute();
    }

    @Override
    public void onRoutingFailure(RouteException e) {
        CustomToast.getInstance().showToast(context,e.getLocalizedMessage(), ToastType.ERROR, Toast.LENGTH_SHORT);
    }

    @Override
    public void onRoutingStart() {

    }

    @Override
    public void onRoutingSuccess(ArrayList<Route> route, int shortestrouteindex) {

        if(polylines != null) {
            if (polylines.size() > 0) {
                for (Polyline poly : polylines) {
                    poly.remove();
                }
            }
        }

        polylines = new ArrayList<>();
        //add route(s) to the map.
        for (int i = 0; i <route.size(); i++) {

            //In case of more than 5 alternative routes

            PolylineOptions polyOptions = new PolylineOptions();
            polyOptions.width(20);
            polyOptions.color(context.getResources().getColor(R.color.colorLightBlue));
            polyOptions.addAll(route.get(i).getPoints());
            Polyline polyline = map.addPolyline(polyOptions);
            polylines.add(polyline);

            // Toast.makeText(context, "Route " + (i + 1) + ": distance - " + route.get(i).getDistanceValue() + ": duration - " + route.get(i).getDurationValue(), Toast.LENGTH_SHORT).show();
        }
    }

    @Override
    public void onRoutingCancelled() {
    }
}
