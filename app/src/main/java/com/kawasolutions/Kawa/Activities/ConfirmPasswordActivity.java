package com.kawasolutions.Kawa.Activities;

import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.PorterDuff;
import android.graphics.drawable.BitmapDrawable;
import android.net.Uri;
import android.os.Bundle;
import android.provider.MediaStore;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.design.widget.TextInputLayout;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.text.Editable;
import android.text.TextUtils;
import android.util.Base64;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.TextView;
import android.text.TextWatcher;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.ImageLoader;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.facebook.AccessToken;
import com.facebook.CallbackManager;
import com.facebook.FacebookActivity;
import com.facebook.FacebookCallback;
import com.facebook.FacebookException;
import com.facebook.FacebookSdk;
import com.facebook.GraphRequest;
import com.facebook.GraphResponse;
import com.facebook.Profile;
import com.facebook.login.LoginManager;
import com.facebook.login.LoginResult;
import com.facebook.login.widget.LoginButton;
import com.kawasolutions.Kawa.Constants.URLS;
import com.kawasolutions.Kawa.R;
import com.kawasolutions.Kawa.Constants.Key;
import com.kawasolutions.Kawa.Singleton.VolleySingleton;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.util.HashMap;
import java.util.Hashtable;
import java.util.Map;

import de.hdodenhof.circleimageview.CircleImageView;

public class ConfirmPasswordActivity extends AppCompatActivity implements View.OnClickListener {

    private String firstName;
    private String lastName;
    private String email;
    private String gender;

    private TextView first_name;
    private TextView last_name;
    private TextView email_fb;
    private TextView gender_fb;
    private RadioGroup gender_group;

    private TextInputLayout input_phone;
    private TextInputLayout input_password;
    private TextInputLayout input_confirm_password;
    private TextInputLayout inputCode;

    private EditText phone;
    private EditText password;
    private EditText confirmPassword;
    private EditText code;
    private Button submitBtn;


    private VolleySingleton volleySingleton;
    private RequestQueue requestQueue;

    private ProgressDialog progressDialog;
    private Context context;

    private CircleImageView circleImageView;
    private ImageLoader imageLoader;

    private String profilePicUrl;
    private String imageString;
    private Bitmap profileImage;

    private TextView verifyNumberTextView;
    private TextView termsView;

    private String finalEmail;
    private TextInputLayout inputEmail;
    private EditText editEmail;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        context = this;

        FacebookSdk.sdkInitialize(getApplicationContext());

        setContentView(R.layout.activity_confirm_password);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        toolbar.setTitleTextColor(getResources().getColor(R.color.colorPrimaryDark));
        setSupportActionBar(toolbar);
        getSupportActionBar().setTitle("Register");
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);

        volleySingleton = VolleySingleton.getInstance();
        imageLoader = volleySingleton.getImageLoader();


        first_name = (TextView) findViewById(R.id.name_fromFB);
        last_name = (TextView) findViewById(R.id.last_name_fromFB);
        email_fb = (TextView) findViewById(R.id.email_fromFB);
        gender_fb = (TextView) findViewById(R.id.gender_fromFB);

        input_phone = (TextInputLayout) findViewById(R.id.input_phone__register);
        input_password = (TextInputLayout) findViewById(R.id.input_password_register);
        input_confirm_password = (TextInputLayout) findViewById(R.id.input_confirm_password_register);
        inputCode = (TextInputLayout) findViewById(R.id.input_code);
        inputEmail = (TextInputLayout) findViewById(R.id.input_email_register);

        password = (EditText) findViewById(R.id.password_register);
        confirmPassword = (EditText) findViewById(R.id.confirmpassword_register);
        phone = (EditText) findViewById(R.id.phone_number);
        code = (EditText) findViewById(R.id.code);
        submitBtn = (Button) findViewById(R.id.register_button);
        circleImageView = (CircleImageView) findViewById(R.id.profile_image_confirm);
        editEmail = (EditText) findViewById(R.id.email_input);

        verifyNumberTextView = (TextView) findViewById(R.id.verify_number);
        termsView = (TextView) findViewById(R.id.terms);

        Intent intent = getIntent();
        Bundle bundle = intent.getExtras();
//        Log.d("Data....", "" + bundle.getString("first_name") + " " + bundle.getString("gender") + " " + bundle.getString("Profile_picture_url"));
        if (bundle.getString("first_name") != null) {
            firstName = bundle.getString("first_name");

        }
        if (bundle.getString("last_name") != null) {
            lastName = bundle.getString("last_name");

        }

        if (bundle.getString("gender") != null) {
            gender = bundle.getString("gender");

        }
        if (bundle.getString("email") != null) {
            email = bundle.getString("email");
        }

        if (bundle.getString("Profile_picture_url") != null) {
            profilePicUrl = bundle.getString("Profile_picture_url");
//            Log.d("data:profilePicUrl", "" + profilePicUrl);

            if (email == null || email.equals("")) {
                inputEmail.setVisibility(inputEmail.VISIBLE);

            } else {
                email_fb.setVisibility(email_fb.VISIBLE);
            }

            imageLoader.get(profilePicUrl, new ImageLoader.ImageListener() {
                @Override
                public void onResponse(ImageLoader.ImageContainer response, boolean isImmediate) {
                    if (response.getBitmap() != null) {
                        profileImage = response.getBitmap();
                        circleImageView.setImageBitmap(response.getBitmap());
                    }
                }

                @Override
                public void onErrorResponse(VolleyError error) {

                }
            });
        }

        String name = firstName + " " + lastName;

        first_name.setText(name);
        last_name.setText(lastName);
        if(!email.equals("")){
            email_fb.setText(email);
        }
        gender_fb.setText(gender);


        password.addTextChangedListener(new MyTextWatcher(password));
        confirmPassword.addTextChangedListener(new MyTextWatcher(confirmPassword));
        phone.addTextChangedListener(new MyTextWatcher(phone));
        editEmail.addTextChangedListener(new MyTextWatcher(editEmail));

        submitBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                submitForm();
            }
        });

        verifyNumberTextView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (validatePhone()) {
                    sendCodeToPhone();
                }
            }
        });

        termsView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                showTermsAndCondition();
            }
        });


    }

    public String getStringImage(Bitmap bmp) {
        ByteArrayOutputStream baos = new ByteArrayOutputStream();
        bmp.compress(Bitmap.CompressFormat.JPEG, 100, baos);
        byte[] imageBytes = baos.toByteArray();
        String encodedImage = Base64.encodeToString(imageBytes, Base64.DEFAULT);
        return encodedImage;
    }

    public void submitForm() {

        if (email == null || email.equals("")) {

            if (!validateEmail()) {
                return;
            }
        }
        if (!validatePassword()) {
            return;
        }
        if (!validateconfirmPassword()) {
            return;
        }

        if (!validatePhone()) {
            return;
        }

        if (!validateCode()) {
            return;
        }

        if (profileImage != null) {
            imageString = getStringImage(profileImage);
        }

        if (email == null || email.trim().equals("")) {
            finalEmail = editEmail.getText().toString();
        } else {
            finalEmail = email;
        }
        Log.d("finalEmail", " " + finalEmail);
        HashMap<String, String> params = new HashMap<String, String>();

        params.put("firstName", firstName);
        params.put("lastName", lastName);
        params.put("email", finalEmail);
        params.put("password", password.getText().toString());
        params.put("contactNo", phone.getText().toString());
        params.put("gender", gender);
        params.put("verificationCode", code.getText().toString());
        if (!imageString.isEmpty()) {
            params.put("image", imageString);
        }

        requestQueue = volleySingleton.getRequestQueue();
        JsonObjectRequest jsonObjectRequest = new JsonObjectRequest(Request.Method.POST, URLS.registerPost,
                new JSONObject(params), new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject response) {
                progressDialog.dismiss();
//                Log.d("Response:", "" + response);
                try {
                    if (response != null) {

                        Integer success = 0;

                        if (response.has(Key.success)) {

                            success = (Integer) response.get(Key.success);

                            if (success == 1) {
                                Toast.makeText(ConfirmPasswordActivity.this, "Account Registered Successfully", Toast.LENGTH_SHORT).show();
                                startActivity(new Intent(ConfirmPasswordActivity.this, LoginActivity.class));
                                finish();
                            } else {
                                String error = response.getString(Key.error);
                                Toast.makeText(ConfirmPasswordActivity.this, error, Toast.LENGTH_SHORT).show();
                            }
                        }
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Toast.makeText(ConfirmPasswordActivity.this, "No Connection Found", Toast.LENGTH_SHORT).show();
                progressDialog.dismiss();
            }
        });
        requestQueue.add(jsonObjectRequest);
        progressDialog = new ProgressDialog(context);
        progressDialog.setMessage("Registering your Account");
        progressDialog.setCanceledOnTouchOutside(false);
        progressDialog.show();
    }


    private boolean validatePassword() {
        if (password.getText().toString().trim().isEmpty()) {
            input_password.setError(getString(R.string.error_field_required));
            password.getBackground().setColorFilter(getResources().getColor(R.color.colorTextField), PorterDuff.Mode.SRC_ATOP);
            requestFocus(password);
            return false;
        } else if (password.getText().length() < 6) {
            input_password.setError(getString(R.string.error_password_length));
            password.getBackground().setColorFilter(getResources().getColor(R.color.colorTextField), PorterDuff.Mode.SRC_ATOP);
            requestFocus(password);
            return false;
        } else {
            input_password.setErrorEnabled(false);
        }

        return true;

    }

    private boolean validateconfirmPassword() {
        if (confirmPassword.getText().toString().trim().isEmpty()) {
            input_confirm_password.setError(getString(R.string.error_field_required));
            confirmPassword.getBackground().setColorFilter(getResources().getColor(R.color.colorTextField), PorterDuff.Mode.SRC_ATOP);
            requestFocus(confirmPassword);
            return false;
        } else if (!confirmPassword.getText().toString().equals(password.getText().toString())) {
            input_confirm_password.setError(getString(R.string.error_password_match));
            confirmPassword.getBackground().setColorFilter(getResources().getColor(R.color.colorTextField), PorterDuff.Mode.SRC_ATOP);
            requestFocus(confirmPassword);
            return false;
        } else {
            input_confirm_password.setErrorEnabled(false);
        }

        return true;

    }

    private boolean validatePhone() {
        if (phone.getText().toString().trim().isEmpty()) {
            input_phone.setError(getString(R.string.error_field_required));
            phone.getBackground().setColorFilter(getResources().getColor(R.color.colorTextField), PorterDuff.Mode.SRC_ATOP);
            requestFocus(phone);
            return false;
        } else if (phone.getText().length() < 10 || phone.getText().length() > 10) {
            input_phone.setError(getString(R.string.error_phone_length));
            phone.getBackground().setColorFilter(getResources().getColor(R.color.colorTextField), PorterDuff.Mode.SRC_ATOP);
            requestFocus(phone);
            return false;

        } else {
            input_phone.setErrorEnabled(false);
        }

        return true;
    }

    private boolean validateCode() {
        if (code.getText().toString().trim().isEmpty()) {
            inputCode.setError(getString(R.string.error_field_required));
            code.getBackground().setColorFilter(getResources().getColor(R.color.colorTextField), PorterDuff.Mode.SRC_ATOP);
            requestFocus(code);
            return false;
        } else {
            inputCode.setErrorEnabled(false);
        }

        return true;

    }

    private static boolean isValidEmail(String email) {
        return !TextUtils.isEmpty(email) && android.util.Patterns.EMAIL_ADDRESS.matcher(email).matches();
    }

    private boolean validateEmail() {
        String emailStr = editEmail.getText().toString().trim();

        if (emailStr.isEmpty()) {
            inputEmail.setError(getString(R.string.error_field_required));
            editEmail.getBackground().setColorFilter(getResources().getColor(R.color.colorTextField), PorterDuff.Mode.SRC_ATOP);
            requestFocus(editEmail);
            return false;
        } else if (!isValidEmail(emailStr)) {
            inputEmail.setError(getString(R.string.error_invalid_email));
            editEmail.getBackground().setColorFilter(getResources().getColor(R.color.colorTextField), PorterDuff.Mode.SRC_ATOP);
            requestFocus(editEmail);
            return false;
        } else {
            inputEmail.setErrorEnabled(false);
        }

        return true;
    }


    private class MyTextWatcher implements TextWatcher {

        View view;

        public MyTextWatcher(View view) {
            this.view = view;
        }

        @Override
        public void beforeTextChanged(CharSequence s, int start, int count, int after) {

        }

        @Override
        public void onTextChanged(CharSequence s, int start, int before, int count) {

        }

        @Override
        public void afterTextChanged(Editable s) {
            switch (view.getId()) {

                case R.id.password_register:
                    validatePassword();
                    break;
                case R.id.confirmpassword_register:
                    validateconfirmPassword();
                    break;
                case R.id.phone_number:
                    validatePhone();
                    break;
                case R.id.code:
                    validateCode();
                    break;

                default:
                    break;
            }

        }
    }

    private void requestFocus(View view) {
        if (view.requestFocus()) {
            getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_VISIBLE);
        }
    }


    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                Intent homeIntent = new Intent(this, RegisterActivity.class);
                homeIntent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                startActivity(homeIntent);
        }
        return super.onOptionsItemSelected(item);
    }

    public void sendCodeToPhone() {
        HashMap<String, String> params = new HashMap<String, String>();
        params.put("contactNo", phone.getText().toString());

        requestQueue = volleySingleton.getRequestQueue();
        JsonObjectRequest jsonObjectRequest = new JsonObjectRequest(Request.Method.POST, URLS.verifyNumber,
                new JSONObject(params), new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject response) {
                progressDialog.dismiss();
//                Log.d("Response:", "" + response);
                try {
                    if (response != null) {

                        Integer success = 0;

                        if (response.has(Key.success)) {

                            success = (Integer) response.get(Key.success);

                            if (success == 1) {
                                showAlert();
                            } else {
                                Toast.makeText(ConfirmPasswordActivity.this, "Verification Code Couldn't be sent to your Phone Number", Toast.LENGTH_SHORT).show();
                            }
                        }
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Toast.makeText(ConfirmPasswordActivity.this, "No Connection Found", Toast.LENGTH_SHORT).show();
                progressDialog.dismiss();
            }
        });
        requestQueue.add(jsonObjectRequest);
        progressDialog = new ProgressDialog(context);
        progressDialog.setMessage("Verifying your Phone Number.....");
        progressDialog.setCanceledOnTouchOutside(false);
        progressDialog.show();
    }

    public void showAlert() {
        AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(ConfirmPasswordActivity.this);
        alertDialogBuilder.setMessage("A SMS has been sent to your phone number with the verification Code. Please slide_in_right the Code and Register. Thank You!");
        alertDialogBuilder.setCancelable(true);

        alertDialogBuilder.setPositiveButton("OK", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.dismiss();
            }
        });

        AlertDialog alertDialog = alertDialogBuilder.create();
        alertDialog.show();
    }

    @Override
    public void onClick(View v) {
        int id = v.getId();
        switch (id) {
            case R.id.input_phone:
                requestFocus(phone);
                break;

            case R.id.input_password:
                requestFocus(password);
                break;

            case R.id.input_confirm_password:
                requestFocus(confirmPassword);
                break;

            case R.id.input_code:
                requestFocus(code);
                break;
        }
    }

    public void showTermsAndCondition() {
        String url = getResources().getString(R.string.termsAndConditionURL);
        Intent intent = new Intent(Intent.ACTION_VIEW).setData(Uri.parse(url));
        startActivity(intent);
    }
}


