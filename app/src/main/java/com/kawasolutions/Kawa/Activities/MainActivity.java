package com.kawasolutions.Kawa.Activities;

import android.app.ActivityOptions;
import android.app.AlarmManager;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.media.Image;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.view.PagerAdapter;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.text.Html;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.android.volley.Request;
import com.android.volley.VolleyError;
import com.facebook.AccessToken;
import com.facebook.AccessTokenTracker;
import com.facebook.CallbackManager;
import com.facebook.FacebookCallback;
import com.facebook.FacebookException;
import com.facebook.FacebookSdk;
import com.facebook.GraphRequest;
import com.facebook.GraphResponse;
import com.facebook.Profile;
import com.facebook.ProfileTracker;
import com.facebook.login.LoginManager;
import com.facebook.login.LoginResult;
import com.facebook.login.widget.LoginButton;
import com.kawasolutions.Kawa.BuildConfig;
import com.kawasolutions.Kawa.Constants.Key;
import com.kawasolutions.Kawa.Constants.SharedPrefKey;
import com.kawasolutions.Kawa.Constants.URLS;
import com.kawasolutions.Kawa.Enum.TYPEREQUEST;
import com.kawasolutions.Kawa.LoginStatus;
import com.kawasolutions.Kawa.Models.UserProfile;
import com.kawasolutions.Kawa.NetworkRequest;
import com.kawasolutions.Kawa.NetworkRequestTest;
import com.kawasolutions.Kawa.R;
import com.kawasolutions.Kawa.Utils.SharedPref;

import org.apache.commons.io.FileUtils;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.Calendar;
import java.util.HashMap;

public class MainActivity extends AppCompatActivity implements NetworkRequestTest.NetworkListenerTest{


    private Button registerBtn;
    private Button loginBtn;
    private ViewPager viewPager;
    private ViewPagerAdapter viewPagerAdapter;
    private LinearLayout dotsLayout;
    private TextView[] dots;
    private int[] layouts;
    private Animation slideRightAnim;
    private Animation slideUp;
    private LinearLayout mLayoutButtons;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        slideRightAnim = AnimationUtils.loadAnimation(this, R.anim.slide_in_left);
        slideUp = AnimationUtils.loadAnimation(this,R.anim.bottom_up);

        //For change of Version -- remove this in next version
//        Boolean clear = SharedPref.readBooleanFromPreference(this,SharedPrefKey.fileName,"CacheCleared",false);
//        if(!clear) {
//            removeCache();
//        }
        checkForUpdates();
        Boolean newUpdate = SharedPref.readBooleanFromPreference(this,SharedPrefKey.fileName,SharedPrefKey.KEY_NEW_APP_UPDATE,false);
        Boolean showUpdatePage = SharedPref.readBooleanFromPreference(this,SharedPrefKey.fileName,SharedPrefKey.SHOW_UPDATE_PAGE,false);
        if(newUpdate && showUpdatePage){
            startActivity(new Intent(MainActivity.this, NewUpdateActivity.class));
        } else {
            UserProfile profile = SharedPref.getUserObject(this, SharedPrefKey.fileName);
            if (profile != null && profile.getLoggedIn()) {
                startActivity(new Intent(MainActivity.this, Home.class));
                finish();
            } else {
                setContentView(R.layout.activity_main);
                registerBtn = (Button) findViewById(R.id.btn_register);
                loginBtn = (Button) findViewById(R.id.btn_login);
                viewPager = (ViewPager) findViewById(R.id.viewpager);
                dotsLayout = (LinearLayout) findViewById(R.id.layout_dots);
                layouts = new int[]{R.layout.slide1, R.layout.slide2, R.layout.slide3, R.layout.slide4,R.layout.slide5,R.layout.slide6};
                mLayoutButtons = (LinearLayout) findViewById(R.id.mLlayoutBottomButtons);
                mLayoutButtons.setAnimation(slideUp);

                //add bottom dots
                addBottomDots(0);
                viewPagerAdapter = new ViewPagerAdapter();
                viewPager.setAdapter(viewPagerAdapter);
                viewPager.setOffscreenPageLimit(6);
                viewPager.addOnPageChangeListener(viewPagerPageChangeListener);


                registerBtn.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        Intent intent = new Intent(MainActivity.this, RegisterActivity.class);
                        startActivity(intent);
                    }
                });

                loginBtn.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        Intent intent = new Intent(MainActivity.this, LoginActivity.class);
                        startActivity(intent);
                    }
                });
            }
        }
    }

    ViewPager.OnPageChangeListener viewPagerPageChangeListener = new ViewPager.OnPageChangeListener() {
        @Override
        public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {

        }

        @Override
        public void onPageSelected(int position) {
            addBottomDots(position);
        }

        @Override
        public void onPageScrollStateChanged(int state) {

        }
    };

    @Override
    public void receiveResult(JSONObject result, String url, Context context, TYPEREQUEST typeRequest, @Nullable String userInfo, @Nullable ProgressDialog progressDialog) {
        if(typeRequest == TYPEREQUEST.CHECK_APP_VERSION){
            try {
                if (result != null){
                    if (result.has(Key.success)){
                        Integer success = (Integer) result.get("success");
                        if (success == 1){
                            String latestAppVersionStr = result.getString("version");
                                Integer latest = Integer.parseInt(latestAppVersionStr);
                                Integer currentAppVersion = BuildConfig.VERSION_CODE;
                                if (latest > currentAppVersion) {
                                    Integer version = SharedPref.readIntegerFromPreference(this, SharedPrefKey.fileName, SharedPrefKey.KEY_NEW_APP_VERSION);
                                    if (!version.equals(latest)) {
                                        SharedPref.saveToPreferences(this, SharedPrefKey.fileName, SharedPrefKey.KEY_NEW_APP_UPDATE, true);
                                        SharedPref.saveToPreferences(this, SharedPrefKey.fileName, SharedPrefKey.KEY_NEW_APP_VERSION, latest);
                                        SharedPref.saveToPreferences(this, SharedPrefKey.fileName, SharedPrefKey.SHOW_UPDATE_PAGE, true);
                                    }
                                } else {
                                    SharedPref.saveToPreferences(this, SharedPrefKey.fileName, SharedPrefKey.KEY_NEW_APP_UPDATE, false);
                                }
                        } else {
                            Log.d("Get App Update Error:","Error while checking for app updates");
                        }
                    }
                }
            }
            catch (JSONException e) {
                e.printStackTrace();
            }
        }
    }

    @Override
    public void receiveError(VolleyError error, Context context, TYPEREQUEST typeRequest, @Nullable String userInfo, @Nullable ProgressDialog progressDialog) {

    }

    public class ViewPagerAdapter extends PagerAdapter{

        private LayoutInflater layoutInflater;

        @Override
        public int getCount() {
            return layouts.length;
        }

        @Override
        public boolean isViewFromObject(View view, Object object) {
            return view == object;
        }

        @Override
        public Object instantiateItem(ViewGroup container, int position) {
            layoutInflater = (LayoutInflater) getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            View view = layoutInflater.inflate(layouts[position],container,false);
            container.addView(view);
            if(position == 0){
                ImageView image = (ImageView) view.findViewById(R.id.car_image);
                image.startAnimation(slideRightAnim);
            }
            return view;
        }

        @Override
        public void destroyItem(ViewGroup container, int position, Object object) {
            ((ViewPager) container).removeView((View) object);
        }
    }

    private void addBottomDots(int currentpage){
        dots = new TextView[layouts.length];
        dotsLayout.removeAllViews();
        for(int i=0; i< dots.length; i++){
            dots[i] = new TextView(this);
            dots[i].setText(Html.fromHtml("&#8226;"));
            dots[i].setTextSize(35);
            dots[i].setTextColor(getResources().getColor(R.color.dot_inactive));
            dotsLayout.addView(dots[i]);
        }

        if(dots.length > 0){
            dots[currentpage].setTextColor(getResources().getColor(R.color.dot_active));
        }
    }

    private int getItem(int i){
        return viewPager.getCurrentItem() + i;
    }

    public void checkForUpdates(){
        NetworkRequestTest networkRequestTest = new NetworkRequestTest(this,URLS.checkAppVersion, Request.Method.GET,null,null,TYPEREQUEST.CHECK_APP_VERSION,null,null);
        networkRequestTest.setListener(this);
        networkRequestTest.getResult();
    }

    public void removeCache(){
        FileUtils.deleteQuietly(getCacheDir());
        SharedPref.saveToPreferences(this,SharedPrefKey.fileName,"CacheCleared",true);
    }
}
