package com.kawasolutions.Kawa.Activities;

import android.app.AlertDialog;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.PorterDuff;
import android.media.RingtoneManager;
import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.TextInputLayout;
import android.support.v4.app.NotificationCompat;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.kawasolutions.Kawa.Constants.Key;
import com.kawasolutions.Kawa.Constants.NotificationKeys;
import com.kawasolutions.Kawa.Constants.URLS;
import com.kawasolutions.Kawa.CustomToast;
import com.kawasolutions.Kawa.Enum.TYPEREQUEST;
import com.kawasolutions.Kawa.Enum.ToastType;
import com.kawasolutions.Kawa.NetworkRequest;
import com.kawasolutions.Kawa.NetworkRequestTest;
import com.kawasolutions.Kawa.R;
import com.kawasolutions.Kawa.Singleton.VolleySingleton;
import com.kawasolutions.Kawa.Utils.VolleyErrorMessage;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;

public class ForgotPasswordActivity extends AppCompatActivity implements View.OnClickListener,NetworkRequestTest.NetworkListenerTest{


    private TextInputLayout inputPhone;
    private EditText phone;
    private Button doneButton;

    private VolleySingleton volleySingleton;
    private RequestQueue requestQueue;
    private ProgressDialog progressDialog;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_forgot_password);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        toolbar.setTitleTextColor(getResources().getColor(R.color.colorPrimaryDark));
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);

        volleySingleton = VolleySingleton.getInstance();

        inputPhone = (TextInputLayout) findViewById(R.id.input_phone);

        phone = (EditText) findViewById(R.id.phone);

        doneButton = (Button) findViewById(R.id.button);

        phone.addTextChangedListener(new MyTextWatcher(phone));

        doneButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                submitForm();
            }
        });

    }

    private boolean validatePhone(){
        if (phone.getText().toString().trim().isEmpty()) {
            inputPhone.setError(getString(R.string.error_field_required));
            phone.getBackground().setColorFilter(getResources().getColor(R.color.colorTextField), PorterDuff.Mode.SRC_ATOP);
            requestFocus(phone);
            return false;
        } else if(phone.getText().length() < 10 || phone.getText().length() > 10){
            inputPhone.setError(getString(R.string.error_phone_length));
            phone.getBackground().setColorFilter(getResources().getColor(R.color.colorTextField), PorterDuff.Mode.SRC_ATOP);
            requestFocus(phone);
            return false;

        }else{
            inputPhone.setErrorEnabled(false);
        }

        return true;
    }

    private void requestFocus(View view) {
        if (view.requestFocus()) {
            getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_VISIBLE);
        }
    }

    @Override
    public void receiveResult(JSONObject result, String url, Context context, TYPEREQUEST typeRequest, @Nullable String userInfo, @Nullable ProgressDialog progressDialog) {
        if(progressDialog != null){
            progressDialog.dismiss();
        }
        if(typeRequest == TYPEREQUEST.FORGOT_PASSWORD){
            try {
                if (result != null){
                    if (result.has(Key.success)){
                        Integer success = (Integer) result.get(Key.success);
                        if (success == 1){
                            String email = result.getString("email");
                            String message = "An email has been sent to " + email + " which will guide you through the process of changing your password.";
                            showNotification(message);
                            showAlert(message,true);
                        }
                        else {
                            String message = "The number you have provided doesnt exist in our database";
                            showAlert(message,false);
                        }
                    }
                }
            }
            catch (JSONException e) {
                e.printStackTrace();
            }
        }
    }

    @Override
    public void receiveError(VolleyError error, Context context, TYPEREQUEST typeRequest, @Nullable String userInfo, @Nullable ProgressDialog progressDialog) {
        if(progressDialog != null){
            progressDialog.dismiss();
        }
        CustomToast.getInstance().showToast(context, VolleyErrorMessage.handleVolleyErrors(context,error), ToastType.ERROR,Toast.LENGTH_SHORT);
    }

    private class MyTextWatcher implements TextWatcher {

        View view;

        public MyTextWatcher(View view){
            this.view = view;
        }

        @Override
        public void beforeTextChanged(CharSequence s, int start, int count, int after) {

        }

        @Override
        public void onTextChanged(CharSequence s, int start, int before, int count) {

        }

        @Override
        public void afterTextChanged(Editable s) {
            switch (view.getId()){
                case R.id.phone:
                    validatePhone();
                    break;

                default:
                    break;
            }

        }
    }

    @Override
    public void onClick(View v) {
        int id = v.getId();
        switch (id) {
            case R.id.input_phone:
                requestFocus(phone);
                break;
        }
    }

    public void submitForm(){

        if (!validatePhone()){
            return;
        }

        String phoneStr = phone.getText().toString();

        HashMap<String, Object> params = new HashMap<>();
        params.put("contactNo", phoneStr);

        progressDialog = new ProgressDialog(this);
        progressDialog.setMessage("Please Wait...");
        progressDialog.setCancelable(false);
        Log.d("Forgoturl:","" + URLS.forgotPasswordURL);
        NetworkRequestTest networkRequestTest = new NetworkRequestTest(this,URLS.forgotPasswordURL, Request.Method.POST,params,null,TYPEREQUEST.FORGOT_PASSWORD,progressDialog,null);
        networkRequestTest.setListener(this);
        networkRequestTest.getResult();
    }

public void showAlert(String message, final Boolean moveToLoginPage){
    AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(ForgotPasswordActivity.this);
    alertDialogBuilder.setTitle("Forgot Password");
    alertDialogBuilder.setMessage(message);
    alertDialogBuilder.setCancelable(true);

    alertDialogBuilder.setPositiveButton("OK", new DialogInterface.OnClickListener() {
        @Override
        public void onClick(DialogInterface dialog, int which) {
            dialog.dismiss();
            if(moveToLoginPage){
                Intent i = new Intent(ForgotPasswordActivity.this, LoginActivity.class);
                i.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_NEW_TASK);
                startActivity(i);
                finish();
            }
        }
    });

    AlertDialog alertDialog = alertDialogBuilder.create();
    alertDialog.show();
}

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                Intent homeIntent = new Intent(this, LoginActivity.class);
                homeIntent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                startActivity(homeIntent);
        }
        return super.onOptionsItemSelected(item);
    }

    public void showNotification(String message){
        Intent i = new Intent(this, MainActivity.class);
        i.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_NEW_TASK);
        PendingIntent pendingIntent = PendingIntent.getActivity(this, 0 /* Request code */, i,
                PendingIntent.FLAG_ONE_SHOT);
        Uri defaultSoundUri = RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION);
        NotificationCompat.Builder notificationBuilder = new NotificationCompat.Builder(this)
                .setLargeIcon(getLargeTaxiIcon())
                .setSmallIcon(R.drawable.taxi_marker)
                .setStyle(new NotificationCompat.BigTextStyle().bigText(message))
                .setContentTitle("Kawa")
                .setContentText(message)
                .setAutoCancel(true)
                .setTicker(message)
                .setSound(defaultSoundUri)
                .setContentIntent(pendingIntent);

        NotificationManager notificationManager =
                (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);

        notificationManager.notify(0,notificationBuilder.build());
    }

    private Bitmap getLargeTaxiIcon(){
        return BitmapFactory.decodeResource(getResources(),R.drawable.taxi_marker);
    }
}
