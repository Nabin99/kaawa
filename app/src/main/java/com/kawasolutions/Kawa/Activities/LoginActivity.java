package com.kawasolutions.Kawa.Activities;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.graphics.PorterDuff;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.design.widget.TextInputLayout;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.MotionEvent;
import android.view.View;
import android.view.WindowManager;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.google.gson.Gson;
import com.kawasolutions.Kawa.Constants.Key;
import com.kawasolutions.Kawa.Constants.SharedPrefKey;
import com.kawasolutions.Kawa.Constants.URLS;
import com.kawasolutions.Kawa.CustomToast;
import com.kawasolutions.Kawa.Enum.TYPEREQUEST;
import com.kawasolutions.Kawa.Enum.ToastType;
import com.kawasolutions.Kawa.GCM.RegistrationIntentService;
import com.kawasolutions.Kawa.Models.UserProfile;
import com.kawasolutions.Kawa.NetworkRequest;
import com.kawasolutions.Kawa.NetworkRequestTest;
import com.kawasolutions.Kawa.R;
import com.kawasolutions.Kawa.Singleton.VolleySingleton;
import com.kawasolutions.Kawa.Utils.SharedPref;
import com.kawasolutions.Kawa.Utils.VolleyErrorMessage;

import org.json.JSONObject;

import java.util.HashMap;

public class LoginActivity extends AppCompatActivity implements NetworkRequestTest.NetworkListenerTest {

    private TextInputLayout inputPhone;
    private TextInputLayout inputPassword;
    private EditText phone;
    private EditText password;

    private Button LoginButton;
    private Context context;
    private TextView forgotPassTextView;
    private LinearLayout loginLayout;
    private ProgressDialog progressDialog;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);

        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        toolbar.setTitleTextColor(getResources().getColor(R.color.colorPrimaryDark));
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);

        context = this;
        loginLayout = (LinearLayout) findViewById(R.id.login_layout);
        inputPhone = (TextInputLayout) findViewById(R.id.input_phone);
        inputPassword = (TextInputLayout) findViewById(R.id.input_password);

        phone = (EditText) findViewById(R.id.phone);
        password = (EditText) findViewById(R.id.password);
        LoginButton = (Button) findViewById(R.id.login_form_button);

        forgotPassTextView = (TextView) findViewById(R.id.forgot_password);

        phone.addTextChangedListener(new MyTextWatcher(phone));
        password.addTextChangedListener(new MyTextWatcher(password));

        inputPhone.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                requestFocus(phone);
                showKeyboard(phone);
            }
        });

        inputPassword.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                requestFocus(password);
                showKeyboard(password);
            }
        });

        LoginButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                submitForm();
            }
        });

        forgotPassTextView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(LoginActivity.this,ForgotPasswordActivity.class));
            }
        });

        loginLayout.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View view, MotionEvent motionEvent) {
                if(motionEvent.getAction() == MotionEvent.ACTION_DOWN){
                    hideKeyboard();
                    return true;
                }
                return false;
            }
        });
    }

    public void submitForm(){
        if (!validatePhone()){
            return;
        }
        if (!validatePassword()){
            return;
        }

        String phone_number = phone.getText().toString();
        String pass = password.getText().toString();

        HashMap<String, Object> params = new HashMap<>();
        params.put("contactNo", phone_number);
        params.put("password", pass);

        progressDialog = new ProgressDialog(this);
        progressDialog.setMessage("Logging In...");
        progressDialog.setCancelable(false);
        NetworkRequestTest networkRequestTest = new NetworkRequestTest(this,URLS.loginPost, Request.Method.POST,params,null,TYPEREQUEST.LOGIN,progressDialog,null);
        networkRequestTest.setListener(this);
        networkRequestTest.getResult();
    }

    public boolean validatePhone(){
        if (phone.getText().toString().trim().isEmpty()) {
            inputPhone.setError(getString(R.string.error_field_required));
            phone.getBackground().setColorFilter(getResources().getColor(R.color.colorTextField), PorterDuff.Mode.SRC_ATOP);
            requestFocus(phone);
            return false;
        } else if(phone.getText().length() < 10){
            inputPhone.setError(getString(R.string.error_phone_length));
            phone.getBackground().setColorFilter(getResources().getColor(R.color.colorTextField), PorterDuff.Mode.SRC_ATOP);
            requestFocus(phone);
            return false;

        }else{
            inputPhone.setErrorEnabled(false);
        }
        return true;
    }

    public boolean validatePassword(){
        if (password.getText().toString().trim().isEmpty()) {
            inputPassword.setError(getString(R.string.error_field_required));
            password.getBackground().setColorFilter(getResources().getColor(R.color.colorTextField), PorterDuff.Mode.SRC_ATOP);
            requestFocus(password);
            return false;
        } else if (password.getText().length() < 6){
            inputPassword.setError(getString(R.string.error_password_length));
            password.getBackground().setColorFilter(getResources().getColor(R.color.colorTextField), PorterDuff.Mode.SRC_ATOP);
            requestFocus(password);
            return false;
        }
        else{
            inputPassword.setErrorEnabled(false);
        }

        return true;
    }

    @Override
    public void receiveResult(JSONObject result, String url, Context context, TYPEREQUEST typeRequest,String userInfo,ProgressDialog progressDialog) {
       if(progressDialog != null){
           progressDialog.dismiss();
       }
        if(typeRequest == TYPEREQUEST.LOGIN){
            try{
                if (result != null){
                    if (result.has(Key.success)){
                        Integer success = (Integer) result.get(Key.success);
                        if (success == 1){
                            JSONObject customerData = result.getJSONObject("customerDetail");

                            Integer customerID = customerData.getInt("customerId");
                            String firstname = customerData.getString("firstName");
                            String lastname = customerData.getString("lastName");
                            String email = customerData.optString("email");
                            String image_name = customerData.optString("image");
                            String gender = customerData.optString("gender");
                            Double rating = customerData.optDouble("averageRating");
                            Integer totalrequest = customerData.optInt("totalRequests");
                            Integer successrideCount = customerData.optInt("successRideCount");
                            Integer unsuccessrideCount = customerData.optInt("unsuccessRideCount");

                            JSONObject userData = customerData.getJSONObject("userId");
                            Integer userId = userData.getInt("userId");

                            JSONObject roleObject = userData.getJSONObject("roleId");
                            Integer roleID = roleObject.getInt("roleId");
                            String phone = userData.optString("contactNo");
                            String username = userData.optString("username");
                            String authToken = userData.getString("authToken");
                            Log.d("UserAuthToken:","" + authToken);

                            UserProfile userProfile = new UserProfile();
                            userProfile.setCustomerID(customerID);
                            userProfile.setFirstname(firstname);
                            userProfile.setLastname(lastname);
                            userProfile.setEmail(email);
                            userProfile.setImageName(image_name);
                            userProfile.setGender(gender);
                            userProfile.setRating(rating);
                            userProfile.setTotalRequest(totalrequest);
                            userProfile.setSuccessCount(successrideCount);
                            userProfile.setUnsuccessCount(unsuccessrideCount);
                            userProfile.setUserID(userId);
                            userProfile.setRoleID(roleID);
                            userProfile.setPhone(phone);
                            userProfile.setUsername(username);
                            userProfile.setAuthToken(authToken);
                            userProfile.setLoggedIn(true);

                            Gson gson = new Gson();
                            String userProfileString = gson.toJson(userProfile);
                            SharedPref.saveToPreferences(context,SharedPrefKey.fileName,SharedPrefKey.KEY_USERPROFILE,userProfileString);

                            UserProfile user = SharedPref.getUserObject(this,SharedPrefKey.fileName);
                            Intent i = new Intent(LoginActivity.this, Home.class);
                            i.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_NEW_TASK);
                            startActivity(i);
                            finish();

                        }else{
                            String error = result.getString(Key.error);
                            CustomToast.getInstance().showToast(this,error,ToastType.ERROR,Toast.LENGTH_LONG);
                        }
                    }
                }

            }catch (Exception e){
                e.printStackTrace();
            }
        }
    }

    @Override
    public void receiveError(VolleyError error, Context context, TYPEREQUEST typeRequest,String userInfo,ProgressDialog progressDialog) {
        if(progressDialog != null){
            progressDialog.dismiss();
        }
        CustomToast.getInstance().showToast(context,VolleyErrorMessage.handleVolleyErrors(context,error),ToastType.ERROR,Toast.LENGTH_LONG);
    }


    private class MyTextWatcher implements TextWatcher {

        View view;

        public MyTextWatcher(View view){
            this.view = view;
        }

        @Override
        public void beforeTextChanged(CharSequence s, int start, int count, int after) {

        }

        @Override
        public void onTextChanged(CharSequence s, int start, int before, int count) {

        }

        @Override
        public void afterTextChanged(Editable s) {

            switch (view.getId()){

                case R.id.phone:
                    validatePhone();
                    break;

                case R.id.password:
                    validatePassword();
                    break;
            }

        }
    }

    private void requestFocus(View view) {
        if (view.requestFocus()) {
            getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_VISIBLE);
        }
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                Intent homeIntent = new Intent(this, MainActivity.class);
                homeIntent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                startActivity(homeIntent);
        }
        return super.onOptionsItemSelected(item);
    }

    public void hideKeyboard(){
        InputMethodManager imm = (InputMethodManager) getSystemService(INPUT_METHOD_SERVICE);
        if(getCurrentFocus() != null) {
            imm.hideSoftInputFromWindow(getCurrentFocus().getWindowToken(), 0);
        }
    }

    private void showKeyboard(View view){
        InputMethodManager input = (InputMethodManager) getSystemService(INPUT_METHOD_SERVICE);
        input.showSoftInput(view, InputMethodManager.SHOW_IMPLICIT);
    }
}
