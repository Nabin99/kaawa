package com.kawasolutions.Kawa.Activities;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;

import com.kawasolutions.Kawa.Adapter.RecyclerViewAdapter;
import com.kawasolutions.Kawa.R;

import java.util.ArrayList;

public class CurrentRideActivity extends AppCompatActivity {

    private static final String TAG = "CurrentRideActivity";

    private ArrayList<String> mTaxiNumbers=new ArrayList<>();
    private ArrayList<String> mPhoneNumbers=new ArrayList<>();
    private ArrayList<String> mOTPCodes=new ArrayList<>();

    @Override
    protected void onCreate(Bundle savedInstanceState) {

        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_current_rides);
        Log.d(TAG, "onCreate: started");
        initListView();
    }

    private void initListView(){
        Log.d(TAG, "initListView: preparing list");

        mTaxiNumbers.add("ba 2 ja 1122");
        mPhoneNumbers.add("9801226700");
        mOTPCodes.add("123");

        mTaxiNumbers.add("ba 2 ja 1123");
        mPhoneNumbers.add("9801226701");
        mOTPCodes.add("124");

        mTaxiNumbers.add("ba 2 ja 1124");
        mPhoneNumbers.add("9801226702");
        mOTPCodes.add("443");

        mTaxiNumbers.add("ba 2 ja 3421");
        mPhoneNumbers.add("9801226703");
        mOTPCodes.add("657");

        initRecyclerView();
    }

    private void initRecyclerView(){
        Log.d(TAG, "initRecyclerView: started");
//        RecyclerView recyclerView=findViewById(R.id.recyclerView);
//        RecyclerViewAdapter rvAdapter=new RecyclerViewAdapter(mTaxiNumbers,mOTPCodes,mNames,this);
//        recyclerView.setAdapter(rvAdapter);
//        recyclerView.setLayoutManager(new LinearLayoutManager(this));
    }
}
