package com.kawasolutions.Kawa.Activities;

import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.PorterDuff;
import android.net.Uri;
import android.os.Bundle;
import android.os.CountDownTimer;
import android.support.design.widget.CoordinatorLayout;
import android.support.design.widget.Snackbar;
import android.support.design.widget.TextInputLayout;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.text.Editable;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.util.Log;
import android.view.MenuItem;
import android.view.MotionEvent;
import android.view.View;
import android.view.WindowManager;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.ImageLoader;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.Volley;
import com.facebook.AccessToken;
import com.facebook.AccessTokenTracker;
import com.facebook.CallbackManager;
import com.facebook.FacebookCallback;
import com.facebook.FacebookException;
import com.facebook.FacebookSdk;
import com.facebook.GraphRequest;
import com.facebook.GraphResponse;
import com.facebook.Profile;
import com.facebook.ProfileTracker;
import com.facebook.login.LoginManager;
import com.facebook.login.LoginResult;
import com.facebook.login.widget.LoginButton;
import com.kawasolutions.Kawa.Constants.Key;
import com.kawasolutions.Kawa.Constants.URLS;
import com.kawasolutions.Kawa.CustomToast;
import com.kawasolutions.Kawa.Enum.TYPEREQUEST;
import com.kawasolutions.Kawa.Enum.ToastType;
import com.kawasolutions.Kawa.NetworkRequest;
import com.kawasolutions.Kawa.NetworkRequestTest;
import com.kawasolutions.Kawa.R;
import com.kawasolutions.Kawa.ServerRequest;
import com.kawasolutions.Kawa.Singleton.VolleySingleton;
import com.kawasolutions.Kawa.Utils.VolleyErrorMessage;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.w3c.dom.Text;

import java.util.HashMap;

public class RegisterActivity extends AppCompatActivity implements View.OnClickListener,NetworkRequestTest.NetworkListenerTest {

    private TextInputLayout inputFirstName;
    private TextInputLayout inputLastName;
    private TextInputLayout inputEmail;
    private TextInputLayout inputPhone;
    private TextInputLayout inputPassword;
    private TextInputLayout inputconfirmPassword;
    private TextInputLayout inputCode;
    private EditText firstName;
    private EditText lastName;
    private EditText email;
    private EditText phone;
    private EditText password;
    private EditText confirmPassword;
    private EditText code;
    private Button submitBtn;
    private RadioGroup radiosexGroup;
    private RadioButton radiosexButton;
    private Context context;
    private TextView termsView;
    private LinearLayout registrationLayout;

    String first_name_text,last_name_text,email_text,gender_text;

    private LoginButton loginFbBtn;
    private CallbackManager callbackManager;

    private AccessTokenTracker tokenTracker;
    private ProfileTracker profileTracker;

    private ImageLoader imageLoader;

    private String profile_picture_url;

    private Button verifyNumberButton;
    private ProgressDialog progressDialog;


    FacebookCallback<LoginResult> mcallback = new FacebookCallback<LoginResult>() {
        @Override
        public void onSuccess(LoginResult loginResult) {

            AccessToken accessToken = loginResult.getAccessToken();
            final Profile profile = Profile.getCurrentProfile();

            GraphRequest request = GraphRequest.newMeRequest(
                    accessToken,
                    new GraphRequest.GraphJSONObjectCallback() {
                        @Override
                        public void onCompleted(JSONObject object, GraphResponse response) {

                            email_text = "";
                            try {

                                JSONObject data = response.getJSONObject();

                                if (data.has("picture")) {
                                    final String profilePicUrl = data.getJSONObject("picture").getJSONObject("data").getString("url");
                                    profile_picture_url = profilePicUrl;

                                }
                                first_name_text = object.getString("first_name");

                                if (data.has("email")) {
                                    Log.d("email status:","has email");
                                    email_text = object.getString("email");
                                }
                                if (data.has("last_name")) {
                                    last_name_text = object.getString("last_name");
                                }

                                if (data.has("gender")) {
                                    gender_text = object.getString("gender");
                                }

                                Intent intent = new Intent(RegisterActivity.this, ConfirmPasswordActivity.class);
                                Bundle bundle = new Bundle();

                                bundle.putString("first_name", first_name_text);
                                if (last_name_text != null) {
                                    bundle.putString("last_name", last_name_text);
                                }
                                if (email_text!= null) {
                                    bundle.putString("email", email_text);
                                }

                                if (gender_text != null) {
                                    bundle.putString("gender", gender_text);
                                }

                                if (profile_picture_url != null) {
                                    bundle.putString("Profile_picture_url", profile_picture_url);
                                }
                                intent.putExtras(bundle);
                                startActivity(intent);

                                //logs out profile immediately after loggin in
                                if (profile != null) {
                                    LoginManager.getInstance().logOut();
                                    Log.d("Facebook Check:", "" + "Profile not null");
                                } else {
                                    loginFbBtn.performClick();
                                }

                            } catch (JSONException e) {
                                e.printStackTrace();
                                Log.d("Facebook error:","Error");
                            }


                        }
                    });
            Bundle parameters = new Bundle();
            parameters.putString("fields", "id,first_name,last_name,email,gender,birthday,picture.type(large)");

            request.setParameters(parameters);
            request.executeAsync();

}


        @Override
        public void onCancel() {
            LoginManager.getInstance().logOut();
        }

        @Override
        public void onError(FacebookException error) {
            Log.d("Fb Error:", "" + error.toString());
        }
    };


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        //Initialize facebook SDK before setting Content View
        FacebookSdk.sdkInitialize(getApplicationContext());
        setContentView(R.layout.activity_register);

        context = this;
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        toolbar.setTitleTextColor(getResources().getColor(R.color.colorPrimaryDark));
        setSupportActionBar(toolbar);
        if(getSupportActionBar() != null) {
            getSupportActionBar().setTitle("Register for KAWA");
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
            getSupportActionBar().setDisplayShowHomeEnabled(true);
        }
        registrationLayout = (LinearLayout) findViewById(R.id.registration_layout);
        inputFirstName = (TextInputLayout) findViewById(R.id.input_firstname);
        inputLastName = (TextInputLayout) findViewById(R.id.input_lastname);
        inputEmail = (TextInputLayout) findViewById(R.id.input_email);
        inputPhone = (TextInputLayout) findViewById(R.id.input_phone);
        inputPassword = (TextInputLayout) findViewById(R.id.input_password);
        inputconfirmPassword = (TextInputLayout) findViewById(R.id.input_confirm_password);
        inputCode = (TextInputLayout) findViewById(R.id.input_code);

        firstName = (EditText) findViewById(R.id.first_name);
        lastName = (EditText) findViewById(R.id.last_name);
        email = (EditText) findViewById(R.id.email);
        password = (EditText) findViewById(R.id.password);
        confirmPassword = (EditText) findViewById(R.id.confirmpassword);
        phone = (EditText) findViewById(R.id.phone);
        code = (EditText) findViewById(R.id.code);
        radiosexGroup = (RadioGroup) findViewById(R.id.gender_group);
        submitBtn = (Button) findViewById(R.id.email_register_button);
        //loginFbBtn = (LoginButton) findViewById(R.id.btn_fb_register);
        termsView = (TextView) findViewById(R.id.terms);

        verifyNumberButton = (Button) findViewById(R.id.verify_number);

        Bundle bundle = getIntent().getExtras();
        if(bundle != null){
            firstName.setText(bundle.getString("first_name"));
            if(bundle.getString("last_name") != null){
                lastName.setText(bundle.getString("last_name"));
            }
            if(bundle.getString("email") != null){
                email.setText(bundle.getString("email"));
            }
        }

        firstName.addTextChangedListener(new MyTextWatcher(firstName));
        lastName.addTextChangedListener(new MyTextWatcher(lastName));
        email.addTextChangedListener(new MyTextWatcher(email));
        password.addTextChangedListener(new MyTextWatcher(password));
        confirmPassword.addTextChangedListener(new MyTextWatcher(confirmPassword));
        phone.addTextChangedListener(new MyTextWatcher(phone));
        code.addTextChangedListener(new MyTextWatcher(code));

        submitBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                submitForm();
            }
        });

        inputFirstName.setOnClickListener(this);
        inputLastName.setOnClickListener(this);
        inputEmail.setOnClickListener(this);
        inputPhone.setOnClickListener(this);
        inputPassword.setOnClickListener(this);
        inputconfirmPassword.setOnClickListener(this);


        callbackManager = CallbackManager.Factory.create();
        //loginFbBtn.setReadPermissions("email");
        //loginFbBtn.registerCallback(callbackManager, mcallback);

        verifyNumberButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (validatePhone()) {
                    sendCodeToPhone();
                }
            }
        });

        termsView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                showTermsAndCondition();
            }
        });

        registrationLayout.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View view, MotionEvent motionEvent) {
                if(motionEvent.getAction() == MotionEvent.ACTION_DOWN) {
                    hideKeyboard();
                    return true;
                }
                return false;
            }
        });
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        callbackManager.onActivityResult(requestCode,resultCode,data);
    }

    public void submitForm(){

        if (!validateFirstName()){
            return;
        }
        if (!validateLastName()){
            return;
        }
        if (!validateEmail()){
            return;
        }
        if (!validatePassword()){
            return;
        }
        if (!validateconfirmPassword()){
            return;
        }

        if (!validatePhone()){
            return;
        }

        if(!validateCode()){
            return;
        }

        String fname = firstName.getText().toString();
        String lname = lastName.getText().toString();
        String mail = email.getText().toString();
        String number = phone.getText().toString();
        String pass = password.getText().toString();
        String codeText = code.getText().toString();

        int selectedId = radiosexGroup.getCheckedRadioButtonId();
        radiosexButton = (RadioButton) findViewById(selectedId);
        String sex = radiosexButton.getText().toString();

        
        HashMap<String, Object> params = new HashMap<>();
        params.put("firstName", fname);
        params.put("lastName", lname);
        params.put("email", mail);
        params.put("password",pass);
        params.put("contactNo", number);
        params.put("gender", sex);
        params.put("verificationCode",codeText);

//        NetworkRequest networkRequest = new NetworkRequest(this,URLS.registerPost,Request.Method.POST, TYPEREQUEST.REGISTER,params,false,"Registering your account..",null);
//        networkRequest.setListener(this);
//        networkRequest.getResult();
        progressDialog = new ProgressDialog(this);
        progressDialog.setMessage("Registering your account...");
        progressDialog.setCancelable(false);
        NetworkRequestTest networkRequestTest = new NetworkRequestTest(this,URLS.registerPost, Request.Method.POST,params,null,TYPEREQUEST.REGISTER,progressDialog,null);
        networkRequestTest.setListener(this);
        networkRequestTest.getResult();
    }

    private boolean validateFirstName(){
        if (firstName.getText().toString().trim().isEmpty()) {
            inputFirstName.setError(getString(R.string.error_field_required));
            firstName.getBackground().setColorFilter(getResources().getColor(R.color.colorTextField), PorterDuff.Mode.SRC_ATOP);
            requestFocus(firstName);
            return false;
        } else {
            inputFirstName.setErrorEnabled(false);
        }

        return true;
    }

    private boolean validateLastName(){
        if (lastName.getText().toString().trim().isEmpty()) {
            inputLastName.setError(getString(R.string.error_field_required));
            lastName.getBackground().setColorFilter(getResources().getColor(R.color.colorTextField), PorterDuff.Mode.SRC_ATOP);
            requestFocus(lastName);
            return false;
        } else {
            inputLastName.setErrorEnabled(false);
        }

        return true;

    }

    private boolean validateEmail(){
        String emailStr = email.getText().toString().trim();

        if (emailStr.isEmpty()) {
            inputEmail.setError(getString(R.string.error_field_required));
            email.getBackground().setColorFilter(getResources().getColor(R.color.colorTextField), PorterDuff.Mode.SRC_ATOP);
            requestFocus(email);
            return false;
        } else if (!isValidEmail(emailStr)){
            inputEmail.setError(getString(R.string.error_invalid_email));
            email.getBackground().setColorFilter(getResources().getColor(R.color.colorTextField), PorterDuff.Mode.SRC_ATOP);
            requestFocus(email);
            return false;
        }

        else {
            inputEmail.setErrorEnabled(false);
        }

        return true;
    }

    private boolean validatePassword(){
        if (password.getText().toString().trim().isEmpty()) {
            inputPassword.setError(getString(R.string.error_field_required));
            password.getBackground().setColorFilter(getResources().getColor(R.color.colorTextField), PorterDuff.Mode.SRC_ATOP);
            requestFocus(password);
            return false;
        } else if (password.getText().length() < 6){
            inputPassword.setError(getString(R.string.error_password_length));
            password.getBackground().setColorFilter(getResources().getColor(R.color.colorTextField), PorterDuff.Mode.SRC_ATOP);
            requestFocus(password);
            return false;
        }
        else{
            inputPassword.setErrorEnabled(false);
        }

        return true;

    }

    private boolean validateconfirmPassword(){
        if (confirmPassword.getText().toString().trim().isEmpty()) {
            inputconfirmPassword.setError(getString(R.string.error_field_required));
            confirmPassword.getBackground().setColorFilter(getResources().getColor(R.color.colorTextField), PorterDuff.Mode.SRC_ATOP);
            requestFocus(confirmPassword);
            return false;
        } else if (!confirmPassword.getText().toString().equals(password.getText().toString())){
            inputconfirmPassword.setError(getString(R.string.error_password_match));
            confirmPassword.getBackground().setColorFilter(getResources().getColor(R.color.colorTextField), PorterDuff.Mode.SRC_ATOP);
            requestFocus(confirmPassword);
            return false;
        }
        else{
            inputconfirmPassword.setErrorEnabled(false);
        }

        return true;

    }

    private boolean validatePhone(){
        if (phone.getText().toString().trim().isEmpty()) {
            inputPhone.setError(getString(R.string.error_field_required));
            phone.getBackground().setColorFilter(getResources().getColor(R.color.colorTextField), PorterDuff.Mode.SRC_ATOP);
            requestFocus(phone);
            return false;
        } else if(phone.getText().length() < 10 || phone.getText().length() > 10){
            inputPhone.setError(getString(R.string.error_phone_length));
            phone.getBackground().setColorFilter(getResources().getColor(R.color.colorTextField), PorterDuff.Mode.SRC_ATOP);
            requestFocus(phone);
            return false;

        }else{
            inputPhone.setErrorEnabled(false);
        }

        return true;
    }

    private boolean validateCode(){
        if (code.getText().toString().trim().isEmpty()) {
            inputCode.setError(getString(R.string.error_field_required));
            code.getBackground().setColorFilter(getResources().getColor(R.color.colorTextField), PorterDuff.Mode.SRC_ATOP);
            requestFocus(code);
            return false;
        } else {
            inputCode.setErrorEnabled(false);
        }

        return true;

    }

    private static boolean isValidEmail(String email) {
        return !TextUtils.isEmpty(email) && android.util.Patterns.EMAIL_ADDRESS.matcher(email).matches();
    }

    @Override
    public void onClick(View v) {
        int id = v.getId();
        switch (id){
            case R.id.input_firstname:
                requestFocus(firstName);
                showKeyboard(firstName);
                break;

            case R.id.input_lastname:
                requestFocus(lastName);
                showKeyboard(lastName);
                break;

            case R.id.input_email:
                requestFocus(email);
                showKeyboard(email);
                break;

            case R.id.input_phone:
                requestFocus(phone);
                showKeyboard(phone);
                break;

            case R.id.input_password:
                requestFocus(password);
                showKeyboard(password);
                break;

            case R.id.input_confirm_password:
                requestFocus(confirmPassword);
                showKeyboard(confirmPassword);
                break;

            case R.id.input_code:
                requestFocus(code);
                showKeyboard(code);
                break;
        }
    }

    @Override
    public void receiveResult(JSONObject result, String url, Context context, TYPEREQUEST typeRequest,String userInfo,ProgressDialog progressDialog) {
        if(progressDialog != null){
            progressDialog.dismiss();
        }
        if(typeRequest == TYPEREQUEST.REGISTER){
            try {
                if (result != null) {
                    if (result.has(Key.success)) {

                       Integer success = (Integer) result.get(Key.success);

                        if (success == 1) {
                            CustomToast.getInstance().showToast(context,"Account Registered Successfully",ToastType.SUCCESS,Toast.LENGTH_SHORT);
                            startActivity(new Intent(RegisterActivity.this, LoginActivity.class));
                            finish();
                        } else {
                            String error = result.getString(Key.error);
                            CustomToast.getInstance().showToast(context,error, ToastType.ERROR,Toast.LENGTH_LONG);
                        }
                    }
                }
            } catch (JSONException e){
                e.printStackTrace();
            }

        } else if(typeRequest == TYPEREQUEST.VERIFY_NUMBER){
            try {
                if (result != null){
                    if (result.has(Key.success)){

                        Integer success = (Integer) result.get(Key.success);

                        if (success == 1){
                            disableGetCodeButton();
                            showAlert();
                        } else {
                            CustomToast.getInstance().showToast(context,"Verification code couldn't be sent to your phone number..",ToastType.ERROR,Toast.LENGTH_SHORT);
                        }
                    }
                }
            }
            catch (JSONException e) {
                e.printStackTrace();
            }
        }
    }

    public void showAlertWithTestCode(){

    }

    private void disableGetCodeButton(){
        verifyNumberButton.setEnabled(false);
        verifyNumberButton.setTextColor(getResources().getColor(R.color.colorDivider));
        CountDownTimer timer = new CountDownTimer(60000,1000){

            @Override
            public void onTick(long millisUntilFinished) {
            }

            @Override
            public void onFinish() {
                    runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            verifyNumberButton.setEnabled(true);
                            verifyNumberButton.setTextColor(getResources().getColor(R.color.colorAccent));

                        }
                    });
                }
            };
        timer.start();
    }

    @Override
    public void receiveError(VolleyError error, Context context, TYPEREQUEST typeRequest,String userInfo,ProgressDialog progressDialog) {
        if(progressDialog != null){
            progressDialog.dismiss();
        }
        CustomToast.getInstance().showToast(context,VolleyErrorMessage.handleVolleyErrors(context,error),ToastType.ERROR,Toast.LENGTH_LONG);
    }

    private class MyTextWatcher implements TextWatcher{

        View view;

        public MyTextWatcher(View view){
            this.view = view;
        }

        @Override
        public void beforeTextChanged(CharSequence s, int start, int count, int after) {

        }

        @Override
        public void onTextChanged(CharSequence s, int start, int before, int count) {

        }

        @Override
        public void afterTextChanged(Editable s) {
            switch (view.getId()){
                case R.id.first_name:
                    validateFirstName();
                    break;
                case R.id.last_name:
                    validateLastName();
                    break;
                case R.id.email:
                    validateEmail();
                    break;
                case R.id.password:
                    validatePassword();
                    break;
                case R.id.confirmpassword:
                    validateconfirmPassword();
                    break;
                case R.id.phone:
                    validatePhone();
                    break;
                case R.id.code:
                    validateCode();
                    break;
                default:
                    break;
            }

        }
    }

    private void requestFocus(View view) {
        if (view.requestFocus()) {
            getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_VISIBLE);
        }
    }

    private void showKeyboard(View view){
        InputMethodManager input = (InputMethodManager) getSystemService(INPUT_METHOD_SERVICE);
        input.showSoftInput(view, InputMethodManager.SHOW_IMPLICIT);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                Intent homeIntent = new Intent(this, MainActivity.class);
                homeIntent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                startActivity(homeIntent);
        }
        return super.onOptionsItemSelected(item);
    }

    public void sendCodeToPhone(){
        HashMap<String, Object> params = new HashMap<>();
        params.put("contactNo", phone.getText().toString());

        progressDialog = new ProgressDialog(this);
        progressDialog.setMessage("Verifying your phone number...");
        progressDialog.setCancelable(false);
        NetworkRequestTest networkRequestTest = new NetworkRequestTest(this,URLS.verifyNumber, Request.Method.POST,params,null,TYPEREQUEST.VERIFY_NUMBER,progressDialog,null);
        networkRequestTest.setListener(this);
        networkRequestTest.getResult();
    }

    public void showAlert(){
        AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(RegisterActivity.this);
        alertDialogBuilder.setMessage("A SMS has been sent to your phone number with the verification Code. Please enter the code and Register. Thank You!");
        alertDialogBuilder.setCancelable(true);

        alertDialogBuilder.setPositiveButton("OK", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.dismiss();
            }
        });

        AlertDialog alertDialog = alertDialogBuilder.create();
        alertDialog.show();
    }

    public void showTermsAndCondition(){
        String url = getResources().getString(R.string.termsAndConditionURL);
        Intent intent = new Intent(Intent.ACTION_VIEW).setData(Uri.parse(url));
        startActivity(intent);
    }

    public void hideKeyboard(){
        InputMethodManager imm = (InputMethodManager) getSystemService(INPUT_METHOD_SERVICE);
        if(getCurrentFocus() != null) {
            imm.hideSoftInputFromWindow(getCurrentFocus().getWindowToken(), 0);
        }
    }

}
