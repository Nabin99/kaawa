package com.kawasolutions.Kawa.Activities;

import android.app.Activity;
import android.app.AlarmManager;
import android.app.PendingIntent;
import android.content.Intent;
import android.net.Uri;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import com.kawasolutions.Kawa.AlarmService;
import com.kawasolutions.Kawa.BuildConfig;
import com.kawasolutions.Kawa.Constants.SharedPrefKey;
import com.kawasolutions.Kawa.R;
import com.kawasolutions.Kawa.Utils.SharedPref;

import java.util.Calendar;

public class NewUpdateActivity extends Activity {

    private Button notNowBtn;
    private Button updateBtn;
    private TextView textView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_new_update);

        notNowBtn = (Button) findViewById(R.id.not_now);
        updateBtn = (Button) findViewById(R.id.update_button);
        textView = (TextView) findViewById(R.id.textView);

        String text = "A new version is available for this app. Please update";
        textView.setText(text);
        notNowBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                SharedPref.saveToPreferences(NewUpdateActivity.this, SharedPrefKey.fileName,SharedPrefKey.SHOW_UPDATE_PAGE,false);
                setAlarmToChangeShowUpdateStatus();
                goToMainActivity();
            }
        });

        updateBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                SharedPref.saveToPreferences(NewUpdateActivity.this, SharedPrefKey.fileName,SharedPrefKey.SHOW_UPDATE_PAGE,false);
                setAlarmToChangeShowUpdateStatus();
                showUpdatePage();
            }
        });
    }
    public void goToMainActivity(){
        Intent i = new Intent(NewUpdateActivity.this, MainActivity.class);
        i.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_NEW_TASK);
        startActivity(i);
        finish();
    }

    public void setAlarmToChangeShowUpdateStatus(){
            Intent intent = new Intent(this, AlarmService.class);
            PendingIntent pendingIntent = PendingIntent.getBroadcast(this,0,intent,0);
            Calendar calendar = Calendar.getInstance();
            calendar.add(Calendar.DAY_OF_MONTH,1);
            AlarmManager am = (AlarmManager) getSystemService(ALARM_SERVICE);
            am.setRepeating(AlarmManager.RTC,calendar.getTimeInMillis(),AlarmManager.INTERVAL_DAY,pendingIntent);
    }

    public void showUpdatePage(){
        String url = "market://details?id=" + BuildConfig.APPLICATION_ID;
        Intent intent = new Intent(Intent.ACTION_VIEW).setData(Uri.parse(url));
        startActivity(intent);
    }
}
