package com.kawasolutions.Kawa.Activities;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.drawable.BitmapDrawable;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.ImageLoader;
import com.kawasolutions.Kawa.Constants.Key;
import com.kawasolutions.Kawa.Constants.SharedPrefKey;
import com.kawasolutions.Kawa.Constants.URLS;
import com.kawasolutions.Kawa.CustomToast;
import com.kawasolutions.Kawa.Enum.TYPEREQUEST;
import com.kawasolutions.Kawa.Enum.ToastType;
import com.kawasolutions.Kawa.Models.UserProfile;
import com.kawasolutions.Kawa.NetworkRequest;
import com.kawasolutions.Kawa.NetworkRequestTest;
import com.kawasolutions.Kawa.R;
import com.kawasolutions.Kawa.Singleton.VolleySingleton;
import com.kawasolutions.Kawa.Utils.SharedPref;
import com.kawasolutions.Kawa.Utils.VolleyErrorMessage;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;

import de.hdodenhof.circleimageview.CircleImageView;

public class EditProfileActivity extends AppCompatActivity implements NetworkRequestTest.NetworkListenerTest{
    private Button logout_button;
    private ProgressDialog progressDialog;
    private Context context;

    private CircleImageView profileImage;
    private TextView customerName;
    private TextView customerPhone;

    private VolleySingleton volleySingleton;
    private RequestQueue requestQueue;
    private ImageLoader imageLoader;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_edit_profile);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        context = this;
        toolbar.setTitle("My Profile");
        toolbar.setTitleTextColor(getResources().getColor(R.color.colorPrimaryDark));
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);

        volleySingleton = VolleySingleton.getInstance();
        requestQueue = volleySingleton.getRequestQueue();
        imageLoader = volleySingleton.getImageLoader();

        logout_button = (Button) findViewById(R.id.btnLogout);

        profileImage = (CircleImageView) findViewById(R.id.profile_image);
        customerName = (TextView) findViewById(R.id.editcustomerName);
        customerPhone = (TextView) findViewById(R.id.editcustomerPhone);

        String firstname = SharedPref.readFromPreference(context, SharedPrefKey.fileName, "pref_firstname", "");
        String lastname = SharedPref.readFromPreference(context, SharedPrefKey.fileName, "pref_lastname", "");
        String phone = SharedPref.readFromPreference(context, SharedPrefKey.fileName, "pref_phone", "");
        String imageName = SharedPref.readFromPreference(context, SharedPrefKey.fileName, "pref_image_name", "");

        if (firstname != null) {
            customerName.setText(firstname + " " + lastname);
        }

        if (phone != null) {
            customerPhone.setText(phone);
        }

        String imageURL = URLS.profileImageURL + imageName;
        Log.d("profile URL:", "" + imageURL);

        imageLoader.get(imageURL, new ImageLoader.ImageListener() {
            @Override
            public void onResponse(ImageLoader.ImageContainer response, boolean isImmediate) {
                profileImage.setImageBitmap(response.getBitmap());
            }

            @Override
            public void onErrorResponse(VolleyError error) {
                // Toast.makeText(Home.this, "Cannot Load Image", Toast.LENGTH_SHORT).show();
                if(((BitmapDrawable) getResources().getDrawable(R.drawable.profile_image_boy)).getBitmap() != null) {
                    Bitmap icon = ((BitmapDrawable) getResources().getDrawable(R.drawable.profile_image_boy)).getBitmap();
                    profileImage.setImageBitmap(icon);
                }
            }
        });


        logout_button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                UserProfile userProfile = SharedPref.getUserObject(EditProfileActivity.this, SharedPrefKey.fileName);
                if (userProfile != null) {

                    String url = URLS.logout + userProfile.getCustomerID();
                    progressDialog = new ProgressDialog(EditProfileActivity.this);
                    progressDialog.setMessage("Logging Out...");
                    progressDialog.setCancelable(false);
                    NetworkRequestTest networkRequestTest = new NetworkRequestTest(EditProfileActivity.this,url, Request.Method.GET,null,null,TYPEREQUEST.LOG_OUT,null,null);
                    networkRequestTest.setListener(EditProfileActivity.this);
                    networkRequestTest.getResult();
                }
            }
        });
    }

    public static void cancelRide(Context context) {


    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                Intent homeIntent = new Intent(this, MainActivity.class);
                homeIntent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                startActivity(homeIntent);
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public void receiveResult(JSONObject result, String url, Context context, TYPEREQUEST typeRequest, @Nullable String userInfo, @Nullable ProgressDialog progressDialog) {
        if (progressDialog != null) {
            progressDialog.dismiss();
        }
        if(typeRequest == TYPEREQUEST.LOG_OUT){
            try {
                if (result != null) {

                    if (result.has(Key.success)) {

                        Integer success = (Integer) result.get(Key.success);

                        if (success == 1) {
                            SharedPref.saveToPreferences(context,SharedPrefKey.fileName,SharedPrefKey.KEY_USERPROFILE,"");

                            Intent intent = new Intent(EditProfileActivity.this, MainActivity.class);
                            intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                            startActivity(intent);
                            finish();
                        } else {
                            CustomToast.getInstance().showToast(context,"Something went wrong...", ToastType.ERROR, Toast.LENGTH_SHORT);
                        }
                    }
                }
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }
    }

    @Override
    public void receiveError(VolleyError error, Context context, TYPEREQUEST typeRequest, @Nullable String userInfo, @Nullable ProgressDialog progressDialog) {
        if(progressDialog != null){
            progressDialog.dismiss();
        }
        CustomToast.getInstance().showToast(context, VolleyErrorMessage.handleVolleyErrors(context, error), ToastType.ERROR, Toast.LENGTH_SHORT);
    }
}