package com.kawasolutions.Kawa.Activities;

import android.app.AlertDialog;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentFilter;
import android.graphics.Bitmap;
import android.graphics.drawable.BitmapDrawable;
import android.net.ConnectivityManager;
import android.net.Uri;
import android.os.PersistableBundle;
import android.support.annotation.Nullable;
import android.support.v4.app.FragmentManager;
import android.os.Bundle;
import android.support.design.widget.NavigationView;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.MotionEvent;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.ImageLoader;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.GoogleApiAvailability;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.model.LatLng;
import com.google.gson.Gson;
import com.kawasolutions.Kawa.BroadcastReceiver.ConnectivityChangeReceiver;
import com.kawasolutions.Kawa.Constants.Key;
import com.kawasolutions.Kawa.Constants.SharedPrefKey;
import com.kawasolutions.Kawa.Constants.URLS;
import com.kawasolutions.Kawa.CustomToast;
import com.kawasolutions.Kawa.Enum.TYPEREQUEST;
import com.kawasolutions.Kawa.Enum.ToastType;
import com.kawasolutions.Kawa.Fragments.DrawerFragments.AboutFragment;
import com.kawasolutions.Kawa.Fragments.DrawerFragments.BookMyRideFragment;
import com.kawasolutions.Kawa.Fragments.DrawerFragments.CurrentRideListFragment;
import com.kawasolutions.Kawa.Fragments.DrawerFragments.EmergencyContactFragment;
import com.kawasolutions.Kawa.Fragments.DrawerFragments.LastTripsFragment;
import com.kawasolutions.Kawa.Fragments.DrawerFragments.MainFragment;
import com.kawasolutions.Kawa.Fragments.DrawerFragments.RateCardFragment;
import com.kawasolutions.Kawa.Fragments.DrawerFragments.SupportFragment;
import com.kawasolutions.Kawa.GCM.RegistrationIntentService;
import com.kawasolutions.Kawa.LoginStatus;
import com.kawasolutions.Kawa.Models.EndOfRide;
import com.kawasolutions.Kawa.Models.RideDetails;
import com.kawasolutions.Kawa.Models.UserProfile;
import com.kawasolutions.Kawa.NetworkRequest;
import com.kawasolutions.Kawa.NetworkRequestTest;
import com.kawasolutions.Kawa.R;
import com.kawasolutions.Kawa.RideCancellation;
import com.kawasolutions.Kawa.Services.MyLocationService;
import com.kawasolutions.Kawa.Singleton.VolleySingleton;
import com.kawasolutions.Kawa.Utils.PopUp;
import com.kawasolutions.Kawa.Utils.SharedPref;
import com.kawasolutions.Kawa.Utils.VolleyErrorMessage;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;

import de.hdodenhof.circleimageview.CircleImageView;
import it.sephiroth.android.library.tooltip.BuildConfig;
import it.sephiroth.android.library.tooltip.Tooltip;


public class Home extends AppCompatActivity
        implements NavigationView.OnNavigationItemSelectedListener,
        MainFragment.OnFragmentInteractionListener,
        BookMyRideFragment.OnFragmentInteractionListener,
        LastTripsFragment.OnFragmentInteractionListener,
        EmergencyContactFragment.OnFragmentInteractionListener,
        SupportFragment.OnFragmentInteractionListener,
        AboutFragment.OnFragmentInteractionListener,
        RateCardFragment.OnFragmentInteractionListener,
        CurrentRideListFragment.OnFragmentInteractionListener, NetworkRequestTest.NetworkListenerTest
{
    ConnectivityChangeReceiver connectivityChangeReceiver = new ConnectivityChangeReceiver();
    Context context;
    private FragmentManager fm;
    private UserProfile userProfile;
    private ProgressDialog progressDialog;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        context = this;
        if(!checkForPlayServices()){
            return;
        }
            setContentView(R.layout.activity_home);
            fm = getSupportFragmentManager();
            //set Toolbar
            Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
            toolbar.setTitleTextColor(getResources().getColor(R.color.colorPrimaryDark));
            setSupportActionBar(toolbar);

            VolleySingleton volleySingleton = VolleySingleton.getInstance();
            RequestQueue requestQueue = volleySingleton.getRequestQueue();
            ImageLoader imageLoader = volleySingleton.getImageLoader();

            DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
            ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(
                    this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
            drawer.setDrawerListener(toggle);
            toggle.syncState();
            NavigationView navigationView = (NavigationView) findViewById(R.id.nav_view);
            navigationView.setCheckedItem(R.id.nav_book_my_ride);
            navigationView.setNavigationItemSelectedListener(this);

            //get the registration token and save to server
            startService(new Intent(Home.this, RegistrationIntentService.class));
            //Add this users current app version to server
            addAppVersion();
            View hview = navigationView.getHeaderView(0);

            TextView userText = (TextView) hview.findViewById(R.id.username);
            TextView userPhone = (TextView) hview.findViewById(R.id.phone);
            final CircleImageView profileImage = (CircleImageView) hview.findViewById(R.id.profile_image);
            Button logOutBtn = (Button) hview.findViewById(R.id.log_out_button);

            userProfile = SharedPref.getUserObject(this, SharedPrefKey.fileName);
            if (userProfile != null) {
                if (userProfile.getFirstname() != null && userProfile.getLastname() != null) {
                    userText.setText(userProfile.getFirstname() + " " + userProfile.getLastname());
                }
                if (userProfile.getPhone() != null) {
                    userPhone.setText(userProfile.getPhone());
                }
            }

            if (userProfile.getImageName() != null) {
                String imageURL = URLS.profileImageURL + userProfile.getImageName();
                imageLoader.get(imageURL, new ImageLoader.ImageListener() {
                    @Override
                    public void onResponse(ImageLoader.ImageContainer response, boolean isImmediate) {
                        profileImage.setImageBitmap(response.getBitmap());
                    }

                    @Override
                    public void onErrorResponse(VolleyError error) {
                        if (((BitmapDrawable) getResources().getDrawable(R.drawable.profile_image_boy)).getBitmap() != null) {
                            Bitmap icon = ((BitmapDrawable) getResources().getDrawable(R.drawable.profile_image_boy)).getBitmap();
                            profileImage.setImageBitmap(icon);
                        }
                    }
                });
            }

            logOutBtn.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {

                    AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(Home.this);
                    alertDialogBuilder.setTitle("Log Out");
                    alertDialogBuilder.setMessage("Are you sure you want to log out?");

                    alertDialogBuilder.setPositiveButton("LOG OUT", new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            UserProfile userProfile = SharedPref.getUserObject(Home.this, SharedPrefKey.fileName);
                            if (userProfile != null) {
                                String url = URLS.logout + userProfile.getCustomerID();
                                progressDialog = new ProgressDialog(Home.this);
                                progressDialog.setMessage("Logging out...");
                                progressDialog.setCancelable(false);
                                NetworkRequestTest networkRequestTest = new NetworkRequestTest(Home.this,url, Request.Method.GET,null,null,TYPEREQUEST.LOG_OUT,progressDialog,null);
                                networkRequestTest.setListener(Home.this);
                                networkRequestTest.getResult();
                            }
                            dialog.dismiss();
                        }
                    });
                    alertDialogBuilder.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            dialog.dismiss();
                        }
                    });
                    AlertDialog alertDialog = alertDialogBuilder.create();
                    alertDialog.show();
                }
            });
            fm.beginTransaction().replace(R.id.content_frame, new MainFragment()).commit();
    }


    @Override
    public void onBackPressed() {
        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START);
        } else {
            super.onBackPressed();
        }
    }

    @Override
    protected void onStart() {
        super.onStart();
        registerReceiver(connectivityChangeReceiver, new IntentFilter(ConnectivityManager.CONNECTIVITY_ACTION));
    }

    @Override
    protected void onStop() {
        super.onStop();
        unregisterReceiver(connectivityChangeReceiver);
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.

        if (userProfile != null && userProfile.getLoggedIn()) {
            getMenuInflater().inflate(R.menu.menu_loggedin, menu);
        } else {
            getMenuInflater().inflate(R.menu.home, menu);

        }
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();

        PopUp pop = new PopUp();
        pop.popUpMenu(context, id);
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }



    @SuppressWarnings("StatementWithEmptyBody")
    @Override
    public boolean onNavigationItemSelected(MenuItem item) {
        // Handle navigation view item clicks here.
        int id = item.getItemId();

         if (id == R.id.nav_book_my_ride) {

            MainFragment mainFragment = new MainFragment();
            fm.beginTransaction().setCustomAnimations(R.anim.slide_out_left, R.anim.slide_in_right).
             replace(R.id.content_frame, mainFragment).commit();

        } else if (id == R.id.nav_last_trips) {

            fm.beginTransaction().setCustomAnimations(R.anim.slide_in_right, R.anim.slide_out_left).replace(R.id.content_frame, new LastTripsFragment()).commit();

        } else if (id == R.id.nav_rate_card) {

             fm.beginTransaction().setCustomAnimations(R.anim.slide_in_right, R.anim.slide_out_left).replace(R.id.content_frame, new RateCardFragment()).commit();

        } else if (id == R.id.nav_emergency_contact) {
            fm.beginTransaction().setCustomAnimations(R.anim.slide_in_right, R.anim.slide_out_left).replace(R.id.content_frame, new EmergencyContactFragment()).commit();

        } else if (id == R.id.nav_support) {
            fm.beginTransaction().setCustomAnimations(R.anim.slide_in_right, R.anim.slide_out_left).replace(R.id.content_frame, new SupportFragment()).commit();
        } else if (id == R.id.nav_about) {
            fm.beginTransaction().setCustomAnimations(R.anim.slide_in_right, R.anim.slide_out_left).replace(R.id.content_frame, new AboutFragment()).commit();
        }else if (id == R.id.nav_current_rides){
//             Intent i=new Intent(Home.this,CurrentRideActivity.class);
//             i.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_NEW_TASK);
//             startActivity(i);
//             finish();

             fm.beginTransaction().setCustomAnimations(R.anim.slide_in_right, R.anim.slide_out_left).replace(R.id.content_frame, new CurrentRideListFragment()).commit();
         }

        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        drawer.closeDrawer(GravityCompat.START);
        return true;
    }

    @Override
    public boolean onTouchEvent(MotionEvent event) {
        InputMethodManager imm = (InputMethodManager)getSystemService(Context.
                INPUT_METHOD_SERVICE);
        if(getCurrentFocus() != null) {
            imm.hideSoftInputFromWindow(getCurrentFocus().getWindowToken(), 0);
        }
        return true;
    }

    @Override
    protected void onPause() {
        super.onPause();
        stopService(new Intent(this,MyLocationService.class));
    }

    @Override
    protected void onResume() {
        super.onResume();
        Log.d("ACTIVITY SEQUENCE:","ON RESUME");
        startService(new Intent(this, MyLocationService.class));
        UserProfile userProfile = SharedPref.getUserObject(this, SharedPrefKey.fileName);
        Boolean nextRideClicked=SharedPref.getBooleanFromPrefNextRide(this,SharedPrefKey.fileName,SharedPrefKey.KEY_NEXT_RIDE_CLICKED);
        if(userProfile != null && !nextRideClicked) {
            Boolean rateRequest = SharedPref.readBooleanFromPreference(this,SharedPrefKey.fileName,SharedPrefKey.KEY_RATE_REQUEST,false);
            Integer rateCustomerId = SharedPref.readIntegerFromPreference(this,SharedPrefKey.fileName,SharedPrefKey.KEY_RATE_CUSTOMER_ID);

           Log.e("RAte request id:","" + userProfile.getCustomerID() + "  " + rateCustomerId);
//            if(rateRequest && userProfile.getCustomerID().equals(rateCustomerId)){
//                Gson gson = new Gson();
//                String rateDetailsString = SharedPref.readFromPreference(this,SharedPrefKey.fileName,SharedPrefKey.KEY_RATE_DETAILS,null);
//                if(rateDetailsString != null){
//                    EndOfRide endOfRide = gson.fromJson(rateDetailsString,EndOfRide.class);
//                    Intent intent = new Intent(this, RateDriverActivity.class);
//                    Bundle bundle = new Bundle();
//                    Gson gson1 = new Gson();
//                    String rateString = gson1.toJson(endOfRide);
//                    bundle.putString("endOfRideDetails",rateString);
//                    bundle.putBoolean("activityRelaunced",true);
//                    intent.putExtras(bundle);
//                    intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_NEW_TASK);
//                    startActivity(intent);
//                    return;
//                }
//            }


            Boolean onRide = SharedPref.readBooleanFromPreference(this,SharedPrefKey.fileName,SharedPrefKey.KEY_ON_RIDE,false);

            //setting onRide=false so that the page will be directed to main page
            onRide=false;
            Integer rideCustomerId = SharedPref.readIntegerFromPreference(this,SharedPrefKey.fileName,SharedPrefKey.KEY_RIDE_CUSTOMER_ID);
            if(onRide && userProfile.getCustomerID().equals(rideCustomerId)){
                Gson gson = new Gson();
                String rideDetailsString = SharedPref.readFromPreference(this,SharedPrefKey.fileName,SharedPrefKey.KEY_RIDE_DETAILS,"");
                if(!rideDetailsString.equals("")){
                    RideDetails rideDetails = gson.fromJson(rideDetailsString,RideDetails.class);

                    Log.d("RideInformation111:","" + rideDetailsString);
                    Log.d("RideInformation222:","" + rideDetails);

                    Intent i = new Intent(this, RideInfo.class);
                    i.putExtra("rideId", rideDetails.getRideId());
                    i.putExtra("driverFirstName", rideDetails.getDriverProfile().getFirstname());
                    i.putExtra("driverLastName",rideDetails.getDriverProfile().getLastname());
                    i.putExtra("driverPhone", rideDetails.getDriverProfile().getPhone());
                    i.putExtra("driverLatitude", rideDetails.getDriverProfile().getLocation().latitude);
                    i.putExtra("driverLongitude", rideDetails.getDriverProfile().getLocation().longitude);
                    i.putExtra("carModel", rideDetails.getDriverProfile().getVehicleType());
                    i.putExtra("carNumber", rideDetails.getDriverProfile().getVehicleNo());
                    i.putExtra("driverRating", rideDetails.getDriverProfile().getRating());
                    i.putExtra("driverImage",rideDetails.getDriverProfile().getImageName());
                    i.putExtra("trackRideURL",rideDetails.getTrackRideUrl());
                    i.putExtra("otpCode",rideDetails.getOtpCode());
                    i.putExtra("extendedRange",rideDetails.getExtendedRange());
                    i.putExtra("extendedRangePrice",rideDetails.getExtendedRangePrice());
                    if(rideDetails.getDestinationLocation() != null){
                        i.putExtra("destinationLatitude", rideDetails.getDestinationLocation().latitude);
                        i.putExtra("destinationLongitude", rideDetails.getDestinationLocation().longitude);
                    }
                    i.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_NEW_TASK);
                    startActivity(i);
                }
            }
        }
        SharedPref.saveToPreferences(this,SharedPrefKey.fileName,SharedPrefKey.KEY_NEXT_RIDE_CLICKED,false);

    }



    @Override
    public void onSaveInstanceState(Bundle outState, PersistableBundle outPersistentState) {
        super.onSaveInstanceState(outState, outPersistentState);
    }

    @Override
    protected void onRestoreInstanceState(Bundle savedInstanceState) {
        super.onRestoreInstanceState(savedInstanceState);
    }

    @Override
    public void onFragmentInteraction(String title) {
        if(getSupportActionBar() != null) {
            if(title.equals("")){
                getSupportActionBar().setDisplayShowTitleEnabled(false);
            } else {
                getSupportActionBar().setDisplayShowTitleEnabled(true);
                getSupportActionBar().setTitle(title);
            }
        }
    }

    @Override
    public void receiveResult(JSONObject result, String url, Context context, TYPEREQUEST typeRequest, @Nullable String userInfo, @Nullable ProgressDialog progressDialog) {
        if(progressDialog != null){
            progressDialog.dismiss();
        }
        if(typeRequest == TYPEREQUEST.LOG_OUT){
            try {
                if (result != null) {
                    if (result.has(Key.success)) {

                        Integer success = (Integer) result.get(Key.success);
                        if (success == 1) {
                            SharedPref.saveToPreferences(context, SharedPrefKey.fileName,SharedPrefKey.KEY_USERPROFILE,"");
                            context.startActivity(new Intent(context, MainActivity.class));
                            ((Home) context).finish();
                        } else {
                            CustomToast.getInstance().showToast(context,"Something went wrong...",ToastType.ERROR,Toast.LENGTH_SHORT);
                        }
                    }
                }
            } catch (JSONException e) {
                e.printStackTrace();
            }
        } else if(typeRequest == TYPEREQUEST.ADD_APP_VERSION){
            // REsponse from Server
        }
    }

    @Override
    public void receiveError(VolleyError error, Context context, TYPEREQUEST typeRequest, @Nullable String userInfo, @Nullable ProgressDialog progressDialog) {
        if(progressDialog != null){
            progressDialog.dismiss();
        }
        if(typeRequest != TYPEREQUEST.ADD_APP_VERSION) {
            CustomToast.getInstance().showToast(context, VolleyErrorMessage.handleVolleyErrors(context, error), ToastType.ERROR, Toast.LENGTH_SHORT);
        }
    }

    private boolean checkForPlayServices(){
        GoogleApiAvailability gApi = GoogleApiAvailability.getInstance();
        int resultCode = gApi.isGooglePlayServicesAvailable(this);
        if(resultCode != ConnectionResult.SUCCESS){
            if(gApi.isUserResolvableError(resultCode)){
                Dialog errorDialog = gApi.getErrorDialog(this,resultCode,1);
                errorDialog.setOnCancelListener(new DialogInterface.OnCancelListener() {
                    @Override
                    public void onCancel(DialogInterface dialog) {
                        dialog.dismiss();
                        finish();
                    }
                });
                errorDialog.setCancelable(false);
                errorDialog.show();
            } else {
                Toast.makeText(context, "Your device doesn't support this app..", Toast.LENGTH_SHORT).show();
                finish();
            }
            return false;
        }
        return true;
    }

    public void addAppVersion(){
        UserProfile userProfile = SharedPref.getUserObject(this, SharedPrefKey.fileName);
        if(userProfile != null) {
            HashMap<String, String> mHeaders = new HashMap<>();
            mHeaders.put("authToken", userProfile.getAuthToken());

            HashMap<String,Object> params = new HashMap<>();
            params.put("userId", userProfile.getUserID());
            params.put("versionName", BuildConfig.VERSION_NAME);
            params.put("versionCode", BuildConfig.VERSION_CODE);
            NetworkRequestTest networkRequestTest = new NetworkRequestTest(this,URLS.addAppVersion, Request.Method.POST,params,mHeaders,TYPEREQUEST.ADD_APP_VERSION,null,null);
            networkRequestTest.setListener(this);
            networkRequestTest.getResult();
        }
    }

}
