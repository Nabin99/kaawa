package com.kawasolutions.Kawa.Activities;

import android.Manifest;
import android.animation.ValueAnimator;
import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.graphics.Color;
import android.graphics.Point;
import android.graphics.drawable.BitmapDrawable;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.os.Looper;
import android.os.PersistableBundle;
import android.os.SystemClock;
import android.provider.Settings;
import android.support.annotation.Nullable;
import android.support.design.widget.CoordinatorLayout;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.LocalBroadcastManager;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.CardView;
import android.support.v7.widget.Toolbar;
import android.text.TextUtils;
import android.util.Log;
import android.view.Gravity;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Interpolator;
import android.view.animation.LinearInterpolator;
import android.widget.Button;
import android.widget.FrameLayout;
import android.widget.ImageButton;
import android.widget.LinearLayout;
import android.widget.RatingBar;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.ImageLoader;
import com.android.volley.toolbox.JsonObjectRequest;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.Projection;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.CameraPosition;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.android.gms.vision.Frame;
import com.google.gson.Gson;
import com.kawasolutions.Kawa.Constants.Key;
import com.kawasolutions.Kawa.Constants.NotificationKeys;
import com.kawasolutions.Kawa.Constants.SharedPrefKey;
import com.kawasolutions.Kawa.Constants.URLS;
import com.kawasolutions.Kawa.CustomToast;
import com.kawasolutions.Kawa.Enum.RideStatus;
import com.kawasolutions.Kawa.Enum.TYPEREQUEST;
import com.kawasolutions.Kawa.Enum.ToastType;
import com.kawasolutions.Kawa.Fragments.DrawerFragments.MainFragment;
import com.kawasolutions.Kawa.LatLngInterpolator;
import com.kawasolutions.Kawa.Models.EndOfRide;
import com.kawasolutions.Kawa.Models.RideActivity;
import com.kawasolutions.Kawa.Models.RideDetails;
import com.kawasolutions.Kawa.Models.UserProfile;
import com.kawasolutions.Kawa.NetworkConnection;
import com.kawasolutions.Kawa.NetworkRequest;
import com.kawasolutions.Kawa.NetworkRequestTest;
import com.kawasolutions.Kawa.NotificationFunction;
import com.kawasolutions.Kawa.R;
import com.kawasolutions.Kawa.MapFunction;
import com.kawasolutions.Kawa.RideCancellation;
import com.kawasolutions.Kawa.Services.RideInformationService;
import com.kawasolutions.Kawa.Singleton.TimerSingleton;
import com.kawasolutions.Kawa.Singleton.VolleySingleton;
import com.kawasolutions.Kawa.Utils.SharedPref;
import com.kawasolutions.Kawa.Utils.VolleyErrorMessage;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;
import java.util.Timer;
import java.util.TimerTask;

import de.hdodenhof.circleimageview.CircleImageView;
import de.keyboardsurfer.android.widget.crouton.Configuration;
import de.keyboardsurfer.android.widget.crouton.Crouton;
import de.keyboardsurfer.android.widget.crouton.Style;
import it.sephiroth.android.library.tooltip.Tooltip;

import static com.kawasolutions.Kawa.R.id.date;
import static com.kawasolutions.Kawa.R.id.distance;
import static java.security.AccessController.getContext;

public class RideInfo extends AppCompatActivity implements LocationListener,OnMapReadyCallback,NetworkRequestTest.NetworkListenerTest{

    private static final String TAG = "app status detail";
    private SupportMapFragment supportMapFragment;
    private String drivername;
    private String driverphone;
    private String carmodel;
    private String carnumber;
    @Nullable
    private Double driverrating;
    private LinearLayout driverDetailLayout;
    private TextView name;
    private TextView phone;
    private TextView carModel;
    private TextView carNumber;
    private TextView driverRating;
    private TextView otpCodeView;
    private Integer rideId;
    private Integer driverId;
    private Double driverLatitude;
    private Double driverLongitude;
    private ImageButton calldriverBtn;
    private FloatingActionButton fab;
    private NetworkConnection networkConnection;
    private CoordinatorLayout layout;
    private Snackbar snackbar;
    private ProgressDialog progressDialog;
    private LinearLayout ratingLayout;
    private TextView driverNameRate;
    private RatingBar ratingBar;
    private Button rateButton;
    private Float driverRateValue;
    private Marker driverMarker;
    private Marker destinationMarker;
    public static Timer updateTimer;
    private LatLng driverLocation;
    private LatLng destinationLocation;
    private String otpCode;
    private Location beforeLocation;
    private String trackRideUrl;
    private FloatingActionButton btnAddNewRide;
    private FrameLayout frameLayout;
    private CardView cardView;
    private TextView extendedRangeText;
    private Boolean extendedRange;
    private Double extendedRangePrice;

    @Nullable
    private Double destinationLatitude;
    @Nullable
    private Double destinationLongitude;

    private LocationManager locationManager;
    private LocationListener locationListener;

    private LatLng myLocation = Key.DefaultCoordinates;
    private Marker myLocationMarker;

    private GoogleMap map;
    private CameraPosition cameraPosition;

    public static boolean active = false;
    @Nullable
    private String driverImage;

    private CircleImageView driverImageView;

    private VolleySingleton volleySingleton;
    private RequestQueue requestQueue;
    private ImageLoader imageLoader;

    private Handler rideActivitiesHandler = new Handler();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_ride_info);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        toolbar.setTitleTextColor(getResources().getColor(R.color.colorPrimaryDark));
        setSupportActionBar(toolbar);

        //set Ride started to false on Activity launch
        SharedPref.saveToPreferences(this,SharedPrefKey.fileName,SharedPrefKey.KEY_RIDE_STARTED,false);

        networkConnection = new NetworkConnection(this);
        locationManager = (LocationManager) getSystemService(LOCATION_SERVICE);

        ratingBar = (RatingBar) findViewById(R.id.ratingBar);
        driverRating = (TextView) findViewById(R.id.driver_rating);
        driverDetailLayout = (LinearLayout) findViewById(R.id.driver_details);
        driverDetailLayout.setVisibility(View.INVISIBLE);
        frameLayout = (FrameLayout) findViewById(R.id.frame_layout);

        layout = (CoordinatorLayout) findViewById(R.id.layout);
        supportMapFragment = (SupportMapFragment) getSupportFragmentManager().findFragmentById(R.id.ride_map);
        cardView = (CardView) findViewById(R.id.card_view);
        extendedRangeText = (TextView) findViewById(R.id.extended_range_text);

        name = (TextView) findViewById(R.id.name);
        phone = (TextView) findViewById(R.id.phone);
        carModel = (TextView) findViewById(R.id.car_brand);
        carNumber = (TextView) findViewById(R.id.car_number);
        calldriverBtn = (ImageButton) findViewById(R.id.call_image);
        driverImageView = (CircleImageView) findViewById(R.id.profile_image);
        otpCodeView = (TextView) findViewById(R.id.otp_code);
        btnAddNewRide = (FloatingActionButton) findViewById(R.id.btnAddNewRide);

        volleySingleton = VolleySingleton.getInstance();
        requestQueue = volleySingleton.getRequestQueue();
        imageLoader = volleySingleton.getImageLoader();

        getDataFromBundle();

        //Clear request Notification if exists in Notification Bar
        if(rideId != null){
            NotificationFunction.cancelNotification(this, NotificationKeys.REQUEST_TAG,rideId);
        }
        if(driverImage != null){
            showDriverImage(driverImage);
        }
        if(otpCode != null){
            otpCodeView.setText(otpCode);
        }

        if(extendedRange){
            cardView.setVisibility(View.VISIBLE);
            extendedRangeText.setText("Extra Charge: Rs " + extendedRangePrice);
        }
        supportMapFragment.getMapAsync(this);

        calldriverBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(driverphone != null){
                    Intent intent = new Intent(Intent.ACTION_DIAL);
                    intent.setData(Uri.parse("tel:" + driverphone));
                    startActivity(intent);
                }
            }
        });

//        shareDetailsButton.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//                UserProfile userProfile = SharedPref.getUserObject(RideInfo.this, SharedPrefKey.fileName);
//                if(userProfile != null) {
//                    if(trackRideUrl != null) {
//                        String message = "Track my KAWA ride here: " + trackRideUrl + ". Use OTP: " + otpCode + " to track the ride.";
//                        Intent sendIntent = new Intent(Intent.ACTION_VIEW);
//                        sendIntent.putExtra("sms_body", message);
//                        sendIntent.setData(Uri.parse("sms:"));
//                        //sendIntent.setType("vnd.android-dir/mms-sms");
//                        startActivity(sendIntent);
//                    } else {
//                        CustomToast.getInstance().showToast(RideInfo.this,"Couldn't Share Ride Details....",ToastType.ERROR,Toast.LENGTH_SHORT);
//                    }
//                }
//            }
//        });
        btnAddNewRide.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                SharedPref.saveToPreferences(getApplicationContext(),SharedPrefKey.fileName,SharedPrefKey.KEY_NEXT_RIDE_CLICKED,true);
                Intent i=new Intent(RideInfo.this,Home.class);
                i.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_NEW_TASK);
                startActivity(i);
                finish();
            }
        });

        addToolTipOnFloatingButton();
    }

    public void addToolTipOnFloatingButton(){
        Tooltip.make(this, new Tooltip.Builder(101)
                            .anchor(btnAddNewRide, Tooltip.Gravity.TOP)
                            .closePolicy(new Tooltip.ClosePolicy()
                                    .insidePolicy(true, false)
                            .outsidePolicy(true, false), 12000)
                            .activateDelay(8000)
                            .showDelay(300)
                            .text("Book New Ride From Here.")
                            .maxWidth(500)
                            .withStyleId(R.style.ToolTipLayoutDefaultStyle)
                            .withArrow(true)
                            .withOverlay(true)
                            .floatingAnimation(Tooltip.AnimationBuilder.DEFAULT)
                .build()
        ).show();
    }

    public Bitmap createScaledTaxiMarker(){
        BitmapDrawable bitmapDrawable = (BitmapDrawable) getResources().getDrawable(R.drawable.taxi_marker);
        Bitmap b = bitmapDrawable.getBitmap();
        final float scale = getResources().getDisplayMetrics().density;
        int p = (int) (Key.CAR_SIZE * scale + 0.5f);
        return Bitmap.createScaledBitmap(b,p,p,false);
    }

    public void getDataFromBundle(){

        Bundle data = getIntent().getExtras();

        rideId = data.getInt("rideId");
        driverId = data.getInt("driverId");
        drivername = data.getString("driverFirstName") + " " + data.getString("driverLastName");
        driverphone = data.getString("driverPhone");
        carmodel = data.getString("carModel");
        carnumber = data.getString("carNumber");
        driverLatitude = data.getDouble("driverLatitude");
        driverLongitude = data.getDouble("driverLongitude");
        driverrating = data.getDouble("driverRating");
        driverImage = data.getString("driverImage");
        destinationLatitude = data.getDouble("destinationLatitude",0);
        destinationLongitude = data.getDouble("destinationLongitude",0);
        otpCode = data.getString("otpCode");
        trackRideUrl = data.getString("trackRideURL");
        extendedRange = data.getBoolean("extendedRange");
        extendedRangePrice = data.getDouble("extendedRangePrice");
        Log.d("BundleInformaiton:", " " +extendedRange + extendedRangePrice);
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, String[] permissions, int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);

        switch (requestCode) {
            case 10:
                if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    if (ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) == PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION) == PackageManager.PERMISSION_GRANTED) {
                        requestLocationUpdate();
                    } else {
                        configureLocation();
                    }
                } else {
                    configureLocation();
                }

        }
    }

    public void configureLocation() {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            if (ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {

                requestPermissions(new String[]{Manifest.permission.ACCESS_FINE_LOCATION, Manifest.permission.ACCESS_COARSE_LOCATION, Manifest.permission.INTERNET}, 10);
            } else {
                requestLocationUpdate();
            }
        } else {
            requestLocationUpdate();
        }
    }

    private void requestLocationUpdate() {
        locationManager.requestLocationUpdates(LocationManager.GPS_PROVIDER, 7000, 0, this);
        locationManager.requestLocationUpdates(LocationManager.NETWORK_PROVIDER, 7000, 0, this);
    }

    @Override
    public void onSaveInstanceState(Bundle outState, PersistableBundle outPersistentState) {
        super.onSaveInstanceState(outState, outPersistentState);
    }

    @Override
    protected void onRestoreInstanceState(Bundle savedInstanceState) {
        super.onRestoreInstanceState(savedInstanceState);
    }

    @Override
    protected void onPause() {
        super.onPause();
        Log.d(TAG, "app status details " + "ON PAUSE");
        active = false;
       // stopCheckingRideActivities();
        unregisterReceiver(mConnectivityChangeReceiver);
        LocalBroadcastManager.getInstance(this).unregisterReceiver(rideInformationReceiver);
    }

    @Override
    protected void onResume() {
        active = true;
        Log.d(TAG, "app status details " + "ON RESUME");
        super.onResume();
        Log.d(TAG, "app status details " + updateTimer);
        //Listen for connectivity change
        registerReceiver(mConnectivityChangeReceiver,new IntentFilter(ConnectivityManager.CONNECTIVITY_ACTION));
        LocalBroadcastManager.getInstance(this).registerReceiver(rideInformationReceiver,
                new IntentFilter("rideInformation"));
        UserProfile userProfile = SharedPref.getUserObject(this, SharedPrefKey.fileName);
        if(userProfile != null) {
            Boolean rateRequest = SharedPref.readBooleanFromPreference(this, SharedPrefKey.fileName, SharedPrefKey.KEY_RATE_REQUEST, false);
            Integer rateCustomerId = SharedPref.readIntegerFromPreference(this, SharedPrefKey.fileName, SharedPrefKey.KEY_RATE_CUSTOMER_ID);

            if (rateRequest && userProfile.getCustomerID().equals(rateCustomerId)) {
//                Gson gson = new Gson();
//                String rateDetailsString = SharedPref.readFromPreference(this, SharedPrefKey.fileName, SharedPrefKey.KEY_RATE_DETAILS, null);
//                if (rateDetailsString != null) {
//                    EndOfRide endOfRide = gson.fromJson(rateDetailsString, EndOfRide.class);
//                    Intent intent = new Intent(this, RateDriverActivity.class);
//                    Bundle bundle = new Bundle();
//                    Gson gson1 = new Gson();
//                    String rateString = gson1.toJson(endOfRide);
//                    bundle.putString("endOfRideDetails", rateString);
//                    bundle.putBoolean("activityRelaunced", true);
//                    intent.putExtras(bundle);
//                    intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_NEW_TASK);
//                    startActivity(intent);
//                }
            }


        }
    }

    @Override
    protected void onStop() {
        super.onStop();
        Log.d(TAG, "app status details " + "ON STOP");
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        Log.d(TAG, "app status details " + "ON DESTROY");

    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        Log.d("Ride Info:(Options)", "" + SharedPref.readBooleanFromPreference(this,SharedPrefKey.fileName,SharedPrefKey.KEY_RIDE_STARTED,false));

        Boolean rideStarted = SharedPref.readBooleanFromPreference(this,SharedPrefKey.fileName,SharedPrefKey.KEY_RIDE_STARTED,false);
        if(!rideStarted) {
            // Inflate the menu; this adds items to the action bar if it is present.
            getMenuInflater().inflate(R.menu.cancel_ride, menu);
        }
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        if(id == R.id.cancel_ride){
            RideCancellation rideCancellation = new RideCancellation(this);
            rideCancellation.cancel(rideId);
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onLocationChanged(Location location) {
        myLocation = new LatLng(location.getLatitude(),location.getLongitude());
        if (myLocationMarker == null) {
            map.animateCamera(CameraUpdateFactory.newLatLngZoom(myLocation, 13));
            myLocationMarker = map.addMarker(new MarkerOptions().title("Current Location").position(myLocation) .anchor(0.5f,0.5f));
            myLocationMarker.setIcon(BitmapDescriptorFactory.defaultMarker());

            MapFunction mapFunction = new MapFunction(this,map,myLocation,driverLocation,destinationLocation);
            if(destinationLocation != null){
                mapFunction.showPathWithDestination();
            }
            else {
                mapFunction.showPathToDriver();
            }
        } else {
            if(SharedPref.readBooleanFromPreference(this,SharedPrefKey.fileName,SharedPrefKey.KEY_RIDE_STARTED,false)){
                myLocationMarker.setIcon(BitmapDescriptorFactory.fromBitmap(createScaledTaxiMarker()));
                if(beforeLocation != null){
                    rotateMarker(myLocationMarker,beforeLocation.bearingTo(location));
                }
                myLocationMarker.setTitle("Ride In Progress...");
            }
                LatLng newLocation = new LatLng(location.getLatitude(),location.getLongitude());
                animateMarkerToHC(newLocation,myLocationMarker);
        }
        beforeLocation = location;
    }

    @Override
    public void onStatusChanged(String provider, int status, Bundle extras) {

    }

    @Override
    public void onProviderEnabled(String provider) {
        configureLocation();
    }

    @Override
    public void onProviderDisabled(String provider) {
    }

    @Override
    public void onMapReady(GoogleMap googleMap) {
        map = googleMap;
        googleMap.setMapType(GoogleMap.MAP_TYPE_NORMAL);
        googleMap.animateCamera(CameraUpdateFactory.newLatLngZoom(new LatLng(driverLatitude,driverLongitude), 14));
        addDetailsToMap();
        configureLocation();
        //checkRideActivitiesStatus();
        Log.d("RideID10:","" + rideId);
        if(rideId != null){
            Log.d("RideID9:","" + rideId);

            Intent intent = new Intent(this, RideInformationService.class);
            intent.putExtra("rideId",rideId);
            startService(intent);
        }
    }

    public void addDetailsToMap(){

        name.setText(drivername);
        phone.setText(driverphone);
        carModel.setText(carmodel);
        carNumber.setText(carnumber);

        if(driverRating != null) {
            driverRating.setText(driverrating + "");
        } else {
            driverRating.setText("0");
        }

        driverLocation = new LatLng(driverLatitude,driverLongitude);
        Log.d("Destination11:","" + destinationLatitude);
        if(destinationLatitude != null && destinationLongitude != null){
            destinationLocation = new LatLng(destinationLatitude,destinationLongitude);
        }
        setDriverMarker(driverLocation);

        if(destinationLocation != null){
            setDestinationMarker(destinationLocation);
        }
        driverDetailLayout.setVisibility(View.VISIBLE);
    }

    public void setDriverMarker(LatLng location){
        driverMarker = map.addMarker(new MarkerOptions() .title("Driver Location") .position(location) .anchor(0.5f,0.5f));
        driverMarker.setIcon(BitmapDescriptorFactory.fromBitmap(createScaledTaxiMarker()));
    }

    public void rotateMarker(final Marker marker, final float toRotation) {
        final Handler handler = new Handler();
        final long start = SystemClock.uptimeMillis();
        final float startRotation = marker.getRotation();
        final long duration = 1555;

        final Interpolator interpolator = new LinearInterpolator();

        handler.post(new Runnable() {
            @Override
            public void run() {
                long elapsed = SystemClock.uptimeMillis() - start;
                float t = interpolator.getInterpolation((float) elapsed / duration);

                float rot = t * toRotation + (1 - t) * startRotation;

                marker.setRotation(-rot > 180 ? rot / 2 : rot);
                if (t < 1.0) {
                    // Post again 16ms later.
                    handler.postDelayed(this, 16);
                }
            }
        });
    }

    public void setDestinationMarker(LatLng location){
        destinationMarker = map.addMarker(new MarkerOptions() .title("Destination") .position(location) .anchor(0.5f,0.5f));
        destinationMarker.setIcon(BitmapDescriptorFactory.defaultMarker(BitmapDescriptorFactory.HUE_BLUE));
    }

    public void checkRideActivitiesStatus(){
        updateTimer = TimerSingleton.getInstance();
        updateTimer.schedule(new TimerTask() {
                                 @Override
                                 public void run() {
                                     Log.d("Ride Info:", "Timer for Ride Activities Started");
                                     runOnUiThread(new Runnable() {
                                         public void run() {
                                             getRideActivitiesFromNetwork();
                                         }
                                     });
                                 }
        }, 0, 15000);
    }

    public void stopCheckingRideActivities(){
        if(updateTimer != null){
            updateTimer.cancel();
            updateTimer.purge();
        }
    }

    public void getRideActivitiesFromNetwork(){
        UserProfile userProfile = SharedPref.getUserObject(RideInfo.this, SharedPrefKey.fileName);
        if(userProfile != null) {
            String url = URLS.rideActivityStatus + rideId;
            HashMap<String, String> mHeaders = new HashMap<>();
            mHeaders.put("authToken", userProfile.getAuthToken());
            NetworkRequestTest networkRequestTest = new NetworkRequestTest(RideInfo.this,url, Request.Method.GET,null,mHeaders,TYPEREQUEST.RIDEACTIVITYSTATUS,null,null);
            networkRequestTest.setListener(this);
            networkRequestTest.getResult();
        }
    }

    @Override
    public void onBackPressed() {

    }

    public void showDriverImage(String driverImage) {
        if (driverImage != null) {
            String imageURL = URLS.driverImageURL + driverImage;
            imageLoader.get(imageURL, new ImageLoader.ImageListener() {
                @Override
                public void onResponse(ImageLoader.ImageContainer response, boolean isImmediate) {
                    driverImageView.setImageBitmap(response.getBitmap());
                }

                @Override
                public void onErrorResponse(VolleyError error) {
                    if (((BitmapDrawable) getResources().getDrawable(R.drawable.profile_image_boy)).getBitmap() != null) {
                        Bitmap icon = ((BitmapDrawable) getResources().getDrawable(R.drawable.profile_image_boy)).getBitmap();
                        driverImageView.setImageBitmap(icon);
                    }
                }
            });
        }
    }

    @Override
    public void receiveResult(JSONObject result, String url, Context context, TYPEREQUEST typeRequest, @Nullable String userInfo, @Nullable ProgressDialog progressDialog) {
       if(progressDialog != null){
           progressDialog.dismiss();
       }

        if(typeRequest == TYPEREQUEST.RIDEACTIVITYSTATUS){
            Log.d("RideInfo","" + result);
            try {
                JSONObject rideData = result.getJSONObject("currentRideDetails");

                Double newDriverLatitude = rideData.getDouble("driverLatitude");
                Double newDriverLongitude = rideData.getDouble("driverLongitude");
                Boolean startRideStatus = rideData.getBoolean("startRideStatus");
                Boolean successStatus = rideData.getBoolean("successStatus");
                Boolean cancelledStatus = rideData.getBoolean("cancelledStatus");
                Boolean activeRideStatus = rideData.getBoolean("activeRideStatus");
                Boolean expireStatus = rideData.getBoolean("expireStatus");


                RideActivity rideActivity = RideActivity.getInstance();
                rideActivity.setDriverLocation(new LatLng(newDriverLatitude,newDriverLongitude));
                rideActivity.setRideStarted(startRideStatus);
                rideActivity.setRideSuccess(successStatus);
                rideActivity.setRideCancelled(cancelledStatus);
                rideActivity.setRideActive(activeRideStatus);
                rideActivity.setExpireStatus(expireStatus);
                EndOfRide endOfRide = null;
                if(rideActivity.getRideSuccess()){
                    endOfRide = getRateRequestData(rideData);
                }
                updateUIFromRideActivity(rideActivity,endOfRide);

            } catch (JSONException e){
                e.printStackTrace();
            }
        }
    }

    public void updateUIFromRideActivity(RideActivity rideActivity,@Nullable EndOfRide endOfRide){
        if(rideActivity.getExpireStatus() || rideActivity.getRideCancelled()){
            if(updateTimer != null){
                updateTimer.cancel();
                updateTimer.purge();
            }
            SharedPref.saveToPreferences(this,SharedPrefKey.fileName,SharedPrefKey.KEY_ON_RIDE,false);
            SharedPref.saveToPreferences(this,SharedPrefKey.fileName,SharedPrefKey.KEY_RIDE_DETAILS,"");

            CustomToast.getInstance().showToast(this,"Something went wrong... Your ride was expired",ToastType.ERROR,Toast.LENGTH_SHORT);

            Intent i = new Intent(RideInfo.this, Home.class);
            i.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_NEW_TASK);
            startActivity(i);
            finish();
        }

        if(rideActivity.getRideSuccess()){
            if(updateTimer != null){
                updateTimer.cancel();
                updateTimer.purge();
            }
            if(endOfRide == null){
                SharedPref.saveToPreferences(this,SharedPrefKey.fileName,SharedPrefKey.KEY_ON_RIDE,false);
                SharedPref.saveToPreferences(this,SharedPrefKey.fileName,SharedPrefKey.KEY_RIDE_DETAILS,"");

                CustomToast.getInstance().showToast(this,"Something went wrong... No Ride Details found",ToastType.ERROR,Toast.LENGTH_SHORT);

                Intent i = new Intent(RideInfo.this, Home.class);
                i.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_NEW_TASK);
                startActivity(i);
                finish();
            } else {
                //Show Rate Activity only if user is logged in and it is directed to the current user
                UserProfile userProfile = SharedPref.getUserObject(getApplicationContext(), SharedPrefKey.fileName);
                //   Integer rateCustomerId = SharedPref.readIntegerFromPreference(getApplicationContext(),SharedPrefKey.fileName,SharedPrefKey.KEY_RATE_CUSTOMER_ID);
                if (userProfile != null) {
                    if (userProfile.getCustomerID().equals(endOfRide.getCustomerId())) {
//                        saveRateDetailsToSharedPref(endOfRide);
//                        // String message = data.getString("message");
//                        //showRateNotification(message);
//                        Intent intent = new Intent(this, RateDriverActivity.class);
//                        Bundle bundle = new Bundle();
//                        Gson gson = new Gson();
//                        bundle.putString("endOfRideDetails", gson.toJson(endOfRide));
//                        intent.putExtras(bundle);
//                        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_NEW_TASK);
//                        startActivity(intent);
                    } else {
                        Log.d("Rate Request Customer:", "Not intended for this user");
                    }
                } else {
                    Log.d("Rate Request Customer:", "No user logged in");
                }
            }
//            SharedPref.saveToPreferences(this,SharedPrefKey.fileName,SharedPrefKey.KEY_ON_RIDE,false);
//            SharedPref.saveToPreferences(this,SharedPrefKey.fileName,SharedPrefKey.KEY_RIDE_DETAILS,"");
//
//            Intent i = new Intent(RideInfo.this, Home.class);
//            i.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_NEW_TASK);
//            startActivity(i);
//            finish();
        }
        else if(rideActivity.getRideStarted()){
            SharedPref.saveToPreferences(this,SharedPrefKey.fileName,SharedPrefKey.KEY_RIDE_STARTED,true);
            Log.d("Ride Info:(Frm network)", "" + SharedPref.readBooleanFromPreference(this,SharedPrefKey.fileName,SharedPrefKey.KEY_RIDE_STARTED,false));

            invalidateOptionsMenu();

          if(driverMarker != null){
            driverMarker.remove();
         }
        } else {
            Location beforeLocation = new Location("");
            beforeLocation.setLatitude(driverLatitude);
            beforeLocation.setLongitude(driverLongitude);

            Location afterLocation = new Location("");
            afterLocation.setLatitude(rideActivity.getDriverLocation().latitude);
            afterLocation.setLongitude(rideActivity.getDriverLocation().longitude);
            Float bearing = beforeLocation.bearingTo(afterLocation);
            rotateMarker(driverMarker,bearing);
            animateMarkerToHC(rideActivity.getDriverLocation(),driverMarker);
        }
    }

    public void animateMarkerToHC(final LatLng destinationLocation, final Marker marker){
        Log.d("LocationChange:","Inside ANimation");
        if(marker != null){
            final LatLng startPosition = marker.getPosition();
            final LatLng endPosition = destinationLocation;

            final LatLngInterpolator latLngInterpolator = new LatLngInterpolator.Spherical();
            ValueAnimator valueAnimator = ValueAnimator.ofFloat(0,1);
            valueAnimator.addUpdateListener(new ValueAnimator.AnimatorUpdateListener() {
                @Override
                public void onAnimationUpdate(ValueAnimator animation) {
                    float v = animation.getAnimatedFraction();
                    LatLng newPosition = latLngInterpolator.interpolate(v,startPosition,endPosition);
                    marker.setPosition(newPosition);
                }
            });
            valueAnimator.setDuration(3000);
            valueAnimator.start();
        }
    }

    public void animateMarker(final LatLng toPosition,final boolean hideMarke) {
        final Handler handler = new Handler();
        final long start = SystemClock.uptimeMillis();
        Projection proj = map.getProjection();
        Point startPoint = proj.toScreenLocation(driverMarker.getPosition());
        Log.d("Driver Start Position:","" + driverMarker.getPosition().latitude + "   " + driverMarker.getPosition().longitude);
        Log.d("Driver End Position:","" + toPosition.latitude + "   " + toPosition.longitude);

        final LatLng startLatLng = proj.fromScreenLocation(startPoint);
        final long duration = 5000;

        final Interpolator interpolator = new LinearInterpolator();

        handler.post(new Runnable() {
            @Override
            public void run() {
                long elapsed = SystemClock.uptimeMillis() - start;
                float t = interpolator.getInterpolation((float) elapsed
                        / duration);
                double lng = t * toPosition.longitude + (1 - t)
                        * startLatLng.longitude;
                double lat = t * toPosition.latitude + (1 - t)
                        * startLatLng.latitude;
                driverMarker.setPosition(new LatLng(lat, lng));
                if (t < 1.0) {
                    // Post again 16ms later.
                    handler.postDelayed(this, 16);
                } else {
                    if (hideMarke) {
                        driverMarker.setVisible(false);
                    } else {
                        driverMarker.setVisible(true);
                    }
                }
            }
        });
    }

    public EndOfRide getRateRequestData(JSONObject data){
        EndOfRide endOfRide = null;
        try {
            JSONObject rideDetails = data.getJSONObject("rideDetails");
            Integer rideId = rideDetails.getInt("rideId");
            String date = rideDetails.getString("rideDate");
            String chargeEstimate = rideDetails.getString("chargeEstimate");
            String meterPrice = rideDetails.getString("meterPrice");
            String startLocation = rideDetails.getString("startLocation");
            String endLocation = rideDetails.getString("endLocation");
            String distance = rideDetails.getString("distance");
            String rideMap = rideDetails.getString("staticMap");

            JSONObject customerDetails = rideDetails.getJSONObject("customerId");
            Integer customerId = customerDetails.getInt("customerId");

            JSONObject driverDetails = rideDetails.getJSONObject("driverId");
            String driverName = driverDetails.getString("firstName") + " " + driverDetails.getString("lastName");
            String driverImage = driverDetails.getString("image");

            JSONObject vehicleDetails = driverDetails.getJSONObject("vehicleId");
            String carNumber = vehicleDetails.getString("vehicleNo");

            endOfRide = new EndOfRide();
            endOfRide.setRideId(rideId);
            endOfRide.setDriverName(driverName);
            endOfRide.setStartLocation(startLocation);
            endOfRide.setEndLocation(endLocation);
            endOfRide.setCarNumber(carNumber);
            endOfRide.setDate(date);
            endOfRide.setDistance(distance);
            endOfRide.setChargeEstimate(chargeEstimate);
            endOfRide.setDriverImage(driverImage);
            endOfRide.setMeterPrice(meterPrice);
            endOfRide.setCustomerId(customerId);
            endOfRide.setRideMap(rideMap);

        } catch (JSONException e) {
            e.printStackTrace();
        }
        return endOfRide;
    }

    public void saveRateDetailsToSharedPref(EndOfRide endOfRide){
        Gson gson = new Gson();
        String rateDetailsString = gson.toJson(endOfRide);
        SharedPref.saveToPreferences(getApplicationContext(), SharedPrefKey.fileName,SharedPrefKey.KEY_RATE_REQUEST,true);
        SharedPref.saveToPreferences(getApplicationContext(),SharedPrefKey.fileName,SharedPrefKey.KEY_RATE_DETAILS,rateDetailsString);
        SharedPref.saveToPreferences(getApplicationContext(), SharedPrefKey.fileName,SharedPrefKey.KEY_RATE_CUSTOMER_ID,endOfRide.getCustomerId());

    }

    @Override
    public void receiveError(VolleyError error, Context context, TYPEREQUEST typeRequest, @Nullable String userInfo, @Nullable ProgressDialog progressDialog) {
       if(progressDialog != null){
           progressDialog.dismiss();
       }
        Log.d("Ride Info:", "" + VolleyErrorMessage.handleVolleyErrors(RideInfo.this, error));
    }

    public BroadcastReceiver mConnectivityChangeReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            NetworkInfo networkInfo = intent.getParcelableExtra(ConnectivityManager.EXTRA_NETWORK_INFO);
            if(networkInfo != null && networkInfo.getDetailedState() == NetworkInfo.DetailedState.CONNECTED){
                Crouton.clearCroutonsForActivity(RideInfo.this);
            } else {
                Crouton.showText(RideInfo.this,"No Internet Connection",Style.ALERT,frameLayout);
            }
        }
    };

    public BroadcastReceiver rideInformationReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            Gson gson = new Gson();
            String rideDetailsString = intent.getExtras().getString("rideDetails");
            RideActivity ride = gson.fromJson(rideDetailsString,RideActivity.class);
            if(ride.getExpireStatus() || ride.getRideCancelled()){
                showDialog("Ride Expired","Your ride has expired due to some technical reason.");
            } else if(ride.getRideSuccess()){
                String rateDetails = intent.getExtras().getString("rateDetails","");
                if(!rateDetails.equals("")){
//                    Intent i = new Intent(RideInfo.this, RateDriverActivity.class);
//                    Bundle bundle = new Bundle();
//                    bundle.putString("endOfRideDetails", rateDetails);
//                    i.putExtras(bundle);
//                    i.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_NEW_TASK);
//                    startActivity(i);
                }
            } else if(ride.getRideStarted()){
                invalidateOptionsMenu();
                if(driverMarker != null){
                    driverMarker.remove();
                }
            } else {
                Location beforeLocation = new Location("");
                beforeLocation.setLatitude(driverLatitude);
                beforeLocation.setLongitude(driverLongitude);

                Location afterLocation = new Location("");
                afterLocation.setLatitude(ride.getDriverLocation().latitude);
                afterLocation.setLongitude(ride.getDriverLocation().longitude);
                Float bearing = beforeLocation.bearingTo(afterLocation);
                driverMarker.setRotation(bearing);
                //rotateMarker(driverMarker,bearing);
                animateMarkerToHC(ride.getDriverLocation(),driverMarker);

                //set driverLatitude and longitude to current driver location
                driverLatitude = ride.getDriverLocation().latitude;
                driverLatitude = ride.getDriverLocation().longitude;
            }
        }
    };


    public void showDialog(String title,String message){
        AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(this);
        alertDialogBuilder.setTitle(title);
        alertDialogBuilder.setMessage(message);
        alertDialogBuilder.setCancelable(false);
        alertDialogBuilder.setPositiveButton("OK", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.dismiss();
                Intent i = new Intent(RideInfo.this, MainActivity.class);
                i.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_NEW_TASK);
                startActivity(i);
                finish();
            }
        });

        AlertDialog alertDialog = alertDialogBuilder.create();
        alertDialog.show();
    }
}
