package com.kawasolutions.Kawa.Activities;

import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.content.pm.Signature;
import android.graphics.Bitmap;
import android.graphics.drawable.BitmapDrawable;
import android.location.Address;
import android.location.Geocoder;
import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.text.format.DateFormat;
import android.util.Base64;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.RatingBar;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.ImageLoader;
import com.android.volley.toolbox.JsonObjectRequest;
import com.directions.route.Route;
import com.directions.route.RouteException;
import com.directions.route.Routing;
import com.directions.route.RoutingListener;
import com.facebook.AccessToken;
import com.facebook.CallbackManager;
import com.facebook.FacebookCallback;
import com.facebook.FacebookDialog;
import com.facebook.FacebookException;
import com.facebook.FacebookSdk;
import com.facebook.GraphRequest;
import com.facebook.GraphResponse;
import com.facebook.Profile;
import com.facebook.login.LoginManager;
import com.facebook.login.LoginResult;
import com.facebook.share.model.ShareLinkContent;
import com.facebook.share.model.ShareOpenGraphAction;
import com.facebook.share.model.ShareOpenGraphContent;
import com.facebook.share.model.ShareOpenGraphObject;
import com.facebook.share.widget.ShareDialog;
import com.google.android.gms.maps.model.LatLng;
import com.google.gson.Gson;
import com.kawasolutions.Kawa.Constants.Key;
import com.kawasolutions.Kawa.Constants.SharedPrefKey;
import com.kawasolutions.Kawa.Constants.URLS;
import com.kawasolutions.Kawa.CustomToast;
import com.kawasolutions.Kawa.Enum.TYPEREQUEST;
import com.kawasolutions.Kawa.Enum.ToastType;
import com.kawasolutions.Kawa.Models.EndOfRide;
import com.kawasolutions.Kawa.Models.RideDetails;
import com.kawasolutions.Kawa.Models.UserProfile;
import com.kawasolutions.Kawa.NetworkRequest;
import com.kawasolutions.Kawa.NetworkRequestTest;
import com.kawasolutions.Kawa.R;
import com.kawasolutions.Kawa.Singleton.VolleySingleton;
import com.kawasolutions.Kawa.Utils.SharedPref;
import com.kawasolutions.Kawa.Utils.VolleyErrorMessage;

import org.json.JSONException;
import org.json.JSONObject;
import org.w3c.dom.Text;

import java.net.URI;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import de.hdodenhof.circleimageview.CircleImageView;

public class RateDriverActivity extends AppCompatActivity implements RoutingListener,NetworkRequestTest.NetworkListenerTest{

    private CircleImageView profileImageView;
    private TextView driverName;
    private TextView date;
    private TextView source;
    private TextView destination;
    private TextView distance;
    private TextView price;
    private TextView extraCharge;
    private RatingBar ratingBar;
    private TextView carTextView;
    private Button rateBtn;

    private String driver_name;
    private String ride_date;
    private String startLocation;
    private String endLocation;
    private Integer rideId;
    private Integer rideDistance;
    private String fullStartAddress;
    private String fullEndAddress;
    private String meterPrice;
    @Nullable
    private String driverImage;
    private String vehicleNo;
    private ProgressDialog progressDialog;

    private VolleySingleton volleySingleton;
    private RequestQueue requestQueue;
    private ImageLoader imageLoader;

    private Float driverRateValue;
    private String distanceValue;
    private String chargeEstimate;

    private TextView subTotal;
    private TextView total;
    private Double totalCharge;
    private TextView terms;

    private Button fbShareButton;
    private ShareDialog shareDialog;
    private CallbackManager callbackManager;
    private LoginManager loginManager;
    private String rideMap;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        facebookSDKInitialize();
        setContentView(R.layout.activity_rate_driver);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        toolbar.setTitleTextColor(getResources().getColor(R.color.colorPrimaryDark));
        setSupportActionBar(toolbar);
        profileImageView = (CircleImageView) findViewById(R.id.profile_image);
        driverName = (TextView) findViewById(R.id.driver_name);
        source = (TextView) findViewById(R.id.source);
        destination = (TextView) findViewById(R.id.destination);
        distance = (TextView) findViewById(R.id.distance);
        ratingBar = (RatingBar) findViewById(R.id.ratingBar);
        rateBtn = (Button) findViewById(R.id.rate_btn);
        carTextView = (TextView) findViewById(R.id.taxiNumber);
        ratingBar = (RatingBar) findViewById(R.id.ratingBar);

        subTotal= (TextView)findViewById(R.id.subTotal);
        extraCharge = (TextView) findViewById(R.id.extraCharge);
        total=(TextView)findViewById(R.id.finalTotal);

        fbShareButton = (Button) findViewById(R.id.fb_share_btn);

        volleySingleton = VolleySingleton.getInstance();
        requestQueue = volleySingleton.getRequestQueue();
        imageLoader = volleySingleton.getImageLoader();
        terms = (TextView) findViewById(R.id.terms);

        String rateDetailsString = getIntent().getExtras().getString("endOfRideDetails");
        Boolean activityRelaunced = getIntent().getExtras().getBoolean("activityRelaunced");
        if(activityRelaunced){
            CustomToast.getInstance().showToast(this,"Please rate your previous driver before continuing...",ToastType.INFO,Toast.LENGTH_SHORT);
        }
        Gson gson = new Gson();
        EndOfRide endOfRide = gson.fromJson(rateDetailsString,EndOfRide.class);

        if(endOfRide != null){
             driver_name = endOfRide.getDriverName();
             ride_date = endOfRide.getDate();
             startLocation = endOfRide.getStartLocation();
             endLocation = endOfRide.getEndLocation();
             rideId = endOfRide.getRideId();
             distanceValue = endOfRide.getDistance();
             chargeEstimate = endOfRide.getChargeEstimate();
             driverImage = endOfRide.getDriverImage();
             vehicleNo = endOfRide.getCarNumber();
             meterPrice = endOfRide.getMeterPrice();
            rideMap = endOfRide.getRideMap();
            if(driverImage != null) {
                showDriverImage(driverImage);
            }

            driverName.setText(driver_name);
            source.setText(fullStartAddress);
            destination.setText(fullEndAddress);

            if(distanceValue != null){
                distance.setText(" " + distanceValue + " km");
            }

            subTotal.setText(meterPrice);
            extraCharge.setText(endOfRide.getExtendedRangePrice() + "");
            if(vehicleNo != null){
                carTextView.setText(vehicleNo);
            }
            source.setText(startLocation);
            destination.setText(endLocation);

            totalCharge = Key.SERVICE_CHARGE + Double.valueOf(meterPrice) + endOfRide.getExtendedRangePrice();

            total.setText("Rs " + String.valueOf(totalCharge));
        }

        ratingBar.setOnRatingBarChangeListener(new RatingBar.OnRatingBarChangeListener() {
            @Override
            public void onRatingChanged(RatingBar ratingBar, float rating, boolean fromUser) {

                if (rateBtn.getVisibility() == View.INVISIBLE) {
                    rateBtn.setVisibility(View.VISIBLE);
                }
                driverRateValue = rating;

            }
        });

        rateBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                sendRequest();
            }
        });

        terms.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                showTermsAndCondition();
            }
        });

        shareDialog = new ShareDialog(this);
        fbShareButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                List<String> permissionNeeds = Arrays.asList("publish_actions");
                loginManager = LoginManager.getInstance();
                loginManager.logInWithPublishPermissions(RateDriverActivity.this,permissionNeeds);
                loginManager.registerCallback(callbackManager,facebookLoginResult);
            }
        });
    }


    @Override
    public void onRoutingFailure(RouteException e) {
        Toast.makeText(RateDriverActivity.this, "Cannot Calculate Distance", Toast.LENGTH_SHORT).show();
    }

    @Override
    public void onRoutingStart() {

    }

    @Override
    public void onRoutingSuccess(ArrayList<Route> route, int shortestrouteindex) {
        progressDialog.dismiss();
        rideDistance = route.get(shortestrouteindex).getDistanceValue();
        if(rideDistance != null){
            distance.setText(String.valueOf(rideDistance/1000) + " " + "km");
            price.setText(String.valueOf((rideDistance/1000) * 40));
        }
    }

    @Override
    public void onRoutingCancelled() {
            progressDialog.dismiss();
    }


    public static String convertDate(String dateInMilliseconds,String dateFormat) {
        return DateFormat.format(dateFormat, Long.parseLong(dateInMilliseconds)).toString();
    }


    public void sendRequest(){

        UserProfile userProfile = SharedPref.getUserObject(this, SharedPrefKey.fileName);
        if(userProfile != null) {

            HashMap<String,Object> params = new HashMap<>();
            params.put("rideId", rideId);
            params.put("driverRating", String.valueOf(Math.round(driverRateValue)));

            HashMap<String, String> mHeaders = new HashMap<>();
            mHeaders.put("authToken", userProfile.getAuthToken());

            progressDialog = new ProgressDialog(this);
            progressDialog.setMessage("Rating your driver...");
            progressDialog.setCancelable(false);
            NetworkRequestTest networkRequestTest = new NetworkRequestTest(this,URLS.updateDriverRating, Request.Method.POST,params,mHeaders,TYPEREQUEST.UPDATE_DRIVER_RATING,progressDialog,null);
            networkRequestTest.setListener(this);
            networkRequestTest.getResult();
        }
    }

    public void showDriverImage(String imageName){
        String imageURL =URLS.driverImageURL + imageName;

        imageLoader.get(imageURL, new ImageLoader.ImageListener() {
            @Override
            public void onResponse(ImageLoader.ImageContainer response, boolean isImmediate) {
                profileImageView.setImageBitmap(response.getBitmap());
            }

            @Override
            public void onErrorResponse(VolleyError error) {
                if (((BitmapDrawable) getResources().getDrawable(R.drawable.profile_image_boy)).getBitmap() != null) {
                    Bitmap icon = ((BitmapDrawable) getResources().getDrawable(R.drawable.profile_image_boy)).getBitmap();
                    profileImageView.setImageBitmap(icon);
                }
            }
        });
    }

    @Override
    public void receiveResult(JSONObject result, String url, Context context, TYPEREQUEST typeRequest, @Nullable String userInfo, @Nullable ProgressDialog progressDialog) {
        if(progressDialog != null){
            progressDialog.dismiss();
        }
        if(typeRequest == TYPEREQUEST.UPDATE_DRIVER_RATING){
            try {
                if (result != null) {
                    if (result.has(Key.success)) {

                        Integer success = (Integer) result.get(Key.success);

                        if (success == 1) {
                            CustomToast.getInstance().showToast(RateDriverActivity.this,"You have successfully rated your Driver", ToastType.SUCCESS,Toast.LENGTH_SHORT);
                            SharedPref.saveToPreferences(this,SharedPrefKey.fileName,SharedPrefKey.KEY_ON_RIDE,false);
                            SharedPref.saveToPreferences(this,SharedPrefKey.fileName,SharedPrefKey.KEY_RIDE_DETAILS,"");
                            SharedPref.saveToPreferences(this,SharedPrefKey.fileName,SharedPrefKey.KEY_RIDE_STARTED,false);
                            SharedPref.saveToPreferences(this,SharedPrefKey.fileName,SharedPrefKey.KEY_RIDE_CUSTOMER_ID,0);
                            SharedPref.saveToPreferences(this,SharedPrefKey.fileName,SharedPrefKey.KEY_RATE_REQUEST,false);
                            SharedPref.saveToPreferences(this,SharedPrefKey.fileName,SharedPrefKey.KEY_RATE_DETAILS,"");
                            SharedPref.saveToPreferences(this,SharedPrefKey.fileName,SharedPrefKey.KEY_RATE_CUSTOMER_ID,0);

                            if(RideInfo.updateTimer != null){
                                RideInfo.updateTimer.cancel();
                                RideInfo.updateTimer.purge();
                            }

                            Intent i = new Intent(RateDriverActivity.this, Home.class);
                            i.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_NEW_TASK);
                            startActivity(i);
                            finish();
                        } else {
                            CustomToast.getInstance().showToast(RateDriverActivity.this,getResources().getString(R.string.error), ToastType.ERROR,Toast.LENGTH_SHORT);
                        }
                    }
                }else {
                    CustomToast.getInstance().showToast(RateDriverActivity.this,getResources().getString(R.string.error), ToastType.ERROR,Toast.LENGTH_SHORT);
                }
            }catch(Exception e){
                e.printStackTrace();
            }

        }
    }

    @Override
    public void receiveError(VolleyError error, Context context, TYPEREQUEST typeRequest, @Nullable String userInfo, @Nullable ProgressDialog progressDialog) {
        if(progressDialog != null){
            progressDialog.dismiss();
        }
        CustomToast.getInstance().showToast(RateDriverActivity.this, VolleyErrorMessage.handleVolleyErrors(context,error), ToastType.ERROR,Toast.LENGTH_SHORT);
    }

    public void showTermsAndCondition(){
        String url = getResources().getString(R.string.termsAndConditionURL);
        Intent intent = new Intent(Intent.ACTION_VIEW).setData(Uri.parse(url));
        startActivity(intent);
    }

    public void facebookSDKInitialize(){
        FacebookSdk.sdkInitialize(getApplicationContext());
        callbackManager = CallbackManager.Factory.create();
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        callbackManager.onActivityResult(requestCode,resultCode,data);
    }

    FacebookCallback<LoginResult> facebookLoginResult = new FacebookCallback<LoginResult>() {
        @Override
        public void onSuccess(LoginResult loginResult) {
            shareDialog = new ShareDialog(RateDriverActivity.this);
            if(ShareDialog.canShow(ShareLinkContent.class)){
                AccessToken accessToken = loginResult.getAccessToken();
                final Profile profile = Profile.getCurrentProfile();

                GraphRequest request = GraphRequest.newMeRequest(
                        accessToken,
                        new GraphRequest.GraphJSONObjectCallback() {
                            @Override
                            public void onCompleted(JSONObject object, GraphResponse response) {
                                try {
                                    String first_name = object.getString("first_name");
                                    ShareLinkContent linkContent = new ShareLinkContent.Builder()
                                            .setContentTitle(first_name + " went on a ride with KAWA")
                                            .setContentDescription("From " + startLocation + " to " + endLocation)
                                            .setImageUrl(Uri.parse(rideMap))
                                            .setContentUrl(Uri.parse("http://www.kawarides.com"))
                                            .build();

                                    shareDialog.show(linkContent);

                                } catch (JSONException e) {
                                    e.printStackTrace();
                                    Log.d("Facebook error:","Error");
                                }


                            }
                        });
                Bundle parameters = new Bundle();
                parameters.putString("fields", "id,first_name,last_name,email,gender,birthday,picture.type(large)");

                request.setParameters(parameters);
                request.executeAsync();
            }
        }

        @Override
        public void onCancel() {

        }

        @Override
        public void onError(FacebookException error) {

        }
    };
}
