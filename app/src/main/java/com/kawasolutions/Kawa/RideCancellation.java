package com.kawasolutions.Kawa;

import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.support.annotation.Nullable;
import android.support.design.widget.CoordinatorLayout;
import android.support.design.widget.Snackbar;
import android.util.Log;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.Volley;
import com.kawasolutions.Kawa.Activities.Home;
import com.kawasolutions.Kawa.Activities.RideInfo;
import com.kawasolutions.Kawa.Constants.Key;
import com.kawasolutions.Kawa.Constants.SharedPrefKey;
import com.kawasolutions.Kawa.Constants.URLS;
import com.kawasolutions.Kawa.Enum.TYPEREQUEST;
import com.kawasolutions.Kawa.Enum.ToastType;
import com.kawasolutions.Kawa.Models.UserProfile;
import com.kawasolutions.Kawa.Utils.SharedPref;
import com.kawasolutions.Kawa.Utils.VolleyErrorMessage;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;

/**
 * Created by apple on 5/10/16.
 */
public class RideCancellation implements NetworkRequestTest.NetworkListenerTest{

    private Context context;
    private ProgressDialog progressDialog;
    public RideCancellation(Context context){
        this.context = context;
    }

    public void cancel(final Integer rideId){
        AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(context);
        alertDialogBuilder.setTitle("Cancel Ride");
        alertDialogBuilder.setMessage("Are you sure you want to cancel your ride?");
        alertDialogBuilder.setCancelable(false);

        alertDialogBuilder.setPositiveButton("YES", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                cancelRide(rideId);
            }
        });

        alertDialogBuilder.setNegativeButton("NO", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.dismiss();
            }
        });

        AlertDialog alertDialog = alertDialogBuilder.create();
        alertDialog.show();
    }

    private void cancelRide(Integer rideId){

        UserProfile userProfile = SharedPref.getUserObject(context, SharedPrefKey.fileName);
        if (userProfile != null) {

            HashMap<String, Object> params = new HashMap<>();
            params.put("rideId", rideId);

            HashMap<String, String> mHeaders = new HashMap<>();
            mHeaders.put("authToken", userProfile.getAuthToken());

            progressDialog = new ProgressDialog(context);
            progressDialog.setMessage("Cancelling your ride...");
            progressDialog.setCancelable(false);
            NetworkRequestTest networkRequestTest = new NetworkRequestTest(context,URLS.cancelRide, Request.Method.POST,params,mHeaders,TYPEREQUEST.CANCEL_RIDE,progressDialog,null);
            networkRequestTest.setListener(this);
            networkRequestTest.getResult();
        }
    }

    @Override
    public void receiveResult(JSONObject result, String url, Context context, TYPEREQUEST typeRequest, @Nullable String userInfo, @Nullable ProgressDialog progressDialog) {
        if(progressDialog != null){
            progressDialog.dismiss();
        }
        if(typeRequest == TYPEREQUEST.CANCEL_RIDE){
            try {
                if (result.has(Key.success)) {
                    Integer success = (Integer) result.get(Key.success);
                    if (success == 1) {
                        CustomToast.getInstance().showToast(context, context.getResources().getString(R.string.ride_cancel), ToastType.SUCCESS, Toast.LENGTH_LONG);
                        //Clear Ride details from sharedPref
                        SharedPref.saveToPreferences(context, SharedPrefKey.fileName, SharedPrefKey.KEY_ON_RIDE, false);
                        SharedPref.saveToPreferences(context, SharedPrefKey.fileName, SharedPrefKey.KEY_RIDE_DETAILS, "");
                        SharedPref.saveToPreferences(context, SharedPrefKey.fileName, SharedPrefKey.KEY_RIDE_STARTED, false);

                        if(RideInfo.updateTimer != null){
                            RideInfo.updateTimer.cancel();
                            RideInfo.updateTimer.purge();
                        }

                        Intent i = new Intent(context, Home.class);
                        i.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_NEW_TASK);
                        context.startActivity(i);
                    } else {
                        CustomToast.getInstance().showToast(context, "Something went Wrong..", ToastType.ERROR, Toast.LENGTH_SHORT);
                    }
                } else {
                    Integer error = (Integer) result.get("error");
                    if(error == 1)// Error 1 means ride has already started
                    {
                        SharedPref.saveToPreferences(context,SharedPrefKey.fileName,SharedPrefKey.KEY_RIDE_STARTED,true);
                        CustomToast.getInstance().showToast(context, "You cannot cancel the ride. Ride has already started", ToastType.ERROR, Toast.LENGTH_SHORT);
                    } else if(error == 2)//Error 2 means ride has already completed
                    {
                        CustomToast.getInstance().showToast(context, "This ride has already completed...", ToastType.SUCCESS, Toast.LENGTH_LONG);
                        //Clear Ride details from sharedPref
                        SharedPref.saveToPreferences(context, SharedPrefKey.fileName, SharedPrefKey.KEY_ON_RIDE, false);
                        SharedPref.saveToPreferences(context, SharedPrefKey.fileName, SharedPrefKey.KEY_RIDE_DETAILS, "");

                        Intent i = new Intent(context, Home.class);
                        i.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_NEW_TASK);
                        context.startActivity(i);
                    }
                }
            } catch (Exception e){
                e.printStackTrace();
            }
        }
    }

    @Override
    public void receiveError(VolleyError error, Context context, TYPEREQUEST typeRequest, @Nullable String userInfo, @Nullable ProgressDialog progressDialog) {
        if(progressDialog != null){
            progressDialog.dismiss();
        }
        CustomToast.getInstance().showToast(context, VolleyErrorMessage.handleVolleyErrors(context,error), ToastType.ERROR,Toast.LENGTH_SHORT);
    }
}
