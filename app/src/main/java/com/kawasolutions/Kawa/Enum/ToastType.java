package com.kawasolutions.Kawa.Enum;

/**
 * Created by apple on 2/7/17.
 */

public enum ToastType {
    SUCCESS,
    ERROR,
    INFO,
    NORMAL
}
