package com.kawasolutions.Kawa.Enum;

import java.io.Serializable;

/**
 * Created by apple on 4/28/17.
 */

public enum RideStatus{
    EXPIRED,
    CANCELLED,
    ACCEPTED,
    STARTED,
    SUCCESS
}
