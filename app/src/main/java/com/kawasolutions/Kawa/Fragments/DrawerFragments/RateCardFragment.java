package com.kawasolutions.Kawa.Fragments.DrawerFragments;

import android.app.ProgressDialog;
import android.content.Context;
import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.VolleyError;
import com.kawasolutions.Kawa.Constants.SharedPrefKey;
import com.kawasolutions.Kawa.Constants.URLS;
import com.kawasolutions.Kawa.CustomToast;
import com.kawasolutions.Kawa.Enum.TYPEREQUEST;
import com.kawasolutions.Kawa.Enum.ToastType;
import com.kawasolutions.Kawa.Models.UserProfile;
import com.kawasolutions.Kawa.NetworkRequest;
import com.kawasolutions.Kawa.NetworkRequestTest;
import com.kawasolutions.Kawa.R;
import com.kawasolutions.Kawa.Utils.SharedPref;
import com.kawasolutions.Kawa.Utils.VolleyErrorMessage;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;

/**
 * A simple {@link Fragment} subclass.
 * Activities that contain this fragment must implement the
 * {@link RateCardFragment.OnFragmentInteractionListener} interface
 * to handle interaction events.
 */
public class RateCardFragment extends Fragment implements NetworkRequestTest.NetworkListenerTest {

    private OnFragmentInteractionListener mListener;
    private TextView tvServiceCharge;
    private ProgressDialog progressDialog;

    public RateCardFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        final View view= inflater.inflate(R.layout.fragment_rate_card, container, false);
        tvServiceCharge=(TextView)view.findViewById(R.id.tvServiceCharge);
        getServiceCharge();
        return view;
    }

    // TODO: Rename method, update argument and hook method into UI event
    public void onButtonPressed(String title) {
        if (mListener != null) {
            mListener.onFragmentInteraction(title);
        }
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof OnFragmentInteractionListener) {
            mListener = (OnFragmentInteractionListener) context;
        } else {
            throw new RuntimeException(context.toString()
                    + " must implement OnFragmentInteractionListener");
        }

        onButtonPressed("Rate Card");
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }

    @Override
    public void receiveResult(JSONObject result, String url, Context context, TYPEREQUEST typeRequest, @Nullable String userInfo, @Nullable ProgressDialog progressDialog) {
        if(progressDialog != null){
            progressDialog.dismiss();
        }
        if(typeRequest==TYPEREQUEST.GET_SERVICE_CHARGE_AMOUNT){
            try {
                if (result != null){
                    if (result.has("serviceCharge")){
                        if(result.get("serviceCharge")!=null){
                            JSONArray jsonArrayServiceCharge=result.getJSONArray("serviceCharge");
                            if(jsonArrayServiceCharge.length()>0){
                                JSONObject serviceChargeDetail=jsonArrayServiceCharge.getJSONObject(0);
                                Double serviceChargeAmount=serviceChargeDetail.getDouble("amount");
                                tvServiceCharge.setText(getResources().getString(R.string.serviceChargeTextRs)+" "+serviceChargeAmount+" "+getResources().getString(R.string.serviceChargeTextEachRide));
                            }
                        }
                    }
                }
            }
            catch (JSONException e) {
                e.printStackTrace();
            }}
    }

    @Override
    public void receiveError(VolleyError error, Context context, TYPEREQUEST typeRequest, @Nullable String userInfo, @Nullable ProgressDialog progressDialog) {
        if(progressDialog != null){
            progressDialog.dismiss();
        }
        CustomToast.getInstance().showToast(context, VolleyErrorMessage.handleVolleyErrors(context,error), ToastType.ERROR, Toast.LENGTH_SHORT);
    }

    /**
     * This interface must be implemented by activities that contain this
     * fragment to allow an interaction in this fragment to be communicated
     * to the activity and potentially other fragments contained in that
     * activity.
     * <p>
     * See the Android Training lesson <a href=
     * "http://developer.android.com/training/basics/fragments/communicating.html"
     * >Communicating with Other Fragments</a> for more information.
     */
    public interface OnFragmentInteractionListener {
        // TODO: Update argument type and name
        void onFragmentInteraction(String title);
    }
    private void getServiceCharge(){

        UserProfile userProfile= SharedPref.getUserObject(getActivity(), SharedPrefKey.fileName);
        if(userProfile!=null) {
            String url = URLS.getServiceChargeAmount;
            HashMap<String,String> mHeaders= new HashMap<>();
            mHeaders.put("authToken", userProfile.getAuthToken());

            progressDialog = new ProgressDialog(getActivity());
            progressDialog.setMessage("Getting Service Charge Amount.");
            progressDialog.setCancelable(false);

            NetworkRequestTest networkRequestTest = new NetworkRequestTest(getActivity(), url, Request.Method.GET, null, mHeaders, TYPEREQUEST.GET_SERVICE_CHARGE_AMOUNT, progressDialog, null);
            networkRequestTest.setListener(this);
            networkRequestTest.getResult();
        }


    }
}
