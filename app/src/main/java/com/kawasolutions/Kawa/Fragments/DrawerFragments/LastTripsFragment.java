package com.kawasolutions.Kawa.Fragments.DrawerFragments;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.format.DateFormat;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.Volley;
import com.kawasolutions.Kawa.Activities.MainActivity;
import com.kawasolutions.Kawa.Adapter.LastRideAdapter;
import com.kawasolutions.Kawa.Constants.Key;
import com.kawasolutions.Kawa.Constants.SharedPrefKey;
import com.kawasolutions.Kawa.Constants.URLS;
import com.kawasolutions.Kawa.CustomToast;
import com.kawasolutions.Kawa.DividerItemDecoration;
import com.kawasolutions.Kawa.Enum.TYPEREQUEST;
import com.kawasolutions.Kawa.Enum.ToastType;
import com.kawasolutions.Kawa.Models.LastTrips;
import com.kawasolutions.Kawa.Models.UserProfile;
import com.kawasolutions.Kawa.NetworkRequest;
import com.kawasolutions.Kawa.NetworkRequestTest;
import com.kawasolutions.Kawa.R;
import com.kawasolutions.Kawa.Utils.SharedPref;
import com.kawasolutions.Kawa.Utils.VolleyErrorMessage;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;


public class LastTripsFragment extends Fragment implements NetworkRequestTest.NetworkListenerTest{
    private OnFragmentInteractionListener mListener;

    private ArrayList<LastTrips> lastTripsList = new ArrayList<>();
    private RecyclerView recyclerView;
    private LastRideAdapter lastRideAdapter;
    private TextView noRide;
    private LinearLayout noTripsLayout;
    @Nullable
    private String startLocation;
    @Nullable
    private String endLocation;
    @Nullable
    private String imageName;
    @Nullable
    private Double price;
    private Double extendedPrice;
    private ProgressDialog progressDialog;
    public LastTripsFragment() {
        // Required empty public constructor
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof OnFragmentInteractionListener) {
            mListener = (OnFragmentInteractionListener) context;
        } else {
            throw new RuntimeException(context.toString()
                    + " must implement OnFragmentInteractionListener");
        }
        onButtonPressed("Last Trips");
    }

    public void onButtonPressed(String title) {
        if (mListener != null) {
            mListener.onFragmentInteraction(title);
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_last_trips, container, false);

        recyclerView = (RecyclerView) view.findViewById(R.id.recycler_view);
        noRide = (TextView) view.findViewById(R.id.no_last_trips);
        noTripsLayout = (LinearLayout) view.findViewById(R.id.no_data_layout);
        lastRideAdapter = new LastRideAdapter(lastTripsList);

        RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(getContext());
        recyclerView.setLayoutManager(mLayoutManager);
        recyclerView.addItemDecoration(new DividerItemDecoration(getActivity(), LinearLayoutManager.VERTICAL));
        recyclerView.setAdapter(lastRideAdapter);

        getServiceCharge();
        getLastRidesData();
        return view;
    }


    public void getLastRidesData(){

        UserProfile userProfile = SharedPref.getUserObject(getActivity(), SharedPrefKey.fileName);
        if (userProfile != null) {

            String url = URLS.lastTrips + userProfile.getCustomerID();

            HashMap<String, String> mHeaders = new HashMap<>();
            mHeaders.put("authToken", userProfile.getAuthToken());

            progressDialog = new ProgressDialog(getActivity());
            progressDialog.setMessage("Getting Ride Info...");
            progressDialog.setCancelable(false);
            NetworkRequestTest networkRequestTest = new NetworkRequestTest(getActivity(),url, Request.Method.GET,null,mHeaders,TYPEREQUEST.LAST_TRIPS,progressDialog,null);
            networkRequestTest.setListener(this);
            networkRequestTest.getResult();
        }

    }

    public void parseJSONResponse(JSONObject response){

        try {
            JSONArray details = response.getJSONArray("lastTripList");

            if(details.length() > 0) {
                if(noTripsLayout.getVisibility() == View.VISIBLE){
                    noTripsLayout.setVisibility(View.INVISIBLE);
                }
                for (int i = 0; i < details.length(); i++) {

                    LastTrips lastTrips = new LastTrips();
                    JSONObject lastRideDetail = details.getJSONObject(i);
                    String driverId = lastRideDetail.getString("driverId");
                    String rideId = lastRideDetail.getString("rideId");
                    String driverName = lastRideDetail.getString("firstName") + " " + lastRideDetail.getString("lastName");
                    String car_no = lastRideDetail.getString("vehicleNo");
                    String date = lastRideDetail.getString("rideDate");
                    String status = lastRideDetail.getString("successStatus");
                    String cancelStatus = lastRideDetail.getString("cancelStatus");

                    if(lastRideDetail.isNull("startLocation")){
                        startLocation = null;
                    } else {
                        startLocation = lastRideDetail.getString("startLocation");
                    }
                    if(lastRideDetail.isNull("endLocation")){
                        endLocation = null;
                    } else {
                        endLocation = lastRideDetail.getString("endLocation");
                    }
                    if(lastRideDetail.isNull("image")){
                        imageName = null;
                    } else {
                        imageName = lastRideDetail.getString("image");
                    }
                    if(lastRideDetail.isNull("meterPrice")){
                        price = 0.0;
                    } else {
                        price = lastRideDetail.getDouble("meterPrice");
                    }
                    if(lastRideDetail.isNull("extendedPrice")){
                        extendedPrice = 0.0;
                    }else {
                        extendedPrice = lastRideDetail.getDouble("extendedPrice");
                    }

                    String carType = lastRideDetail.getString("vehicleType");

                    lastTrips.setDriverId(driverId);
                    lastTrips.setRideId(rideId);
                    lastTrips.setDriverName(driverName);
                    lastTrips.setCarNo(car_no);
                    lastTrips.setDate(date);
                    lastTrips.setStatus(status);
                    lastTrips.setCancelStatus(cancelStatus);
                    lastTrips.setStartLocation(startLocation);
                    lastTrips.setEndLocation(endLocation);
                    lastTrips.setImageName(imageName);
                    lastTrips.setCarType(carType);
                    lastTrips.setPrice(price);
                    lastTrips.setExtendedPrice(extendedPrice);

                    lastTripsList.add(lastTrips);

                    lastRideAdapter.notifyDataSetChanged();
                }
            } else {
                if(noTripsLayout.getVisibility() == View.INVISIBLE){
                    noTripsLayout.setVisibility(View.VISIBLE);
                    noRide.setText(getResources().getString(R.string.no_trips));
                }

            }
        } catch (JSONException e) {
            e.printStackTrace();
        }

    }

    @Override
    public void receiveResult(JSONObject result, String url, Context context, TYPEREQUEST typeRequest, @Nullable String userInfo, @Nullable ProgressDialog progressDialog) {
       if(progressDialog != null){
           progressDialog.dismiss();
       }

        if(typeRequest == TYPEREQUEST.LAST_TRIPS){
            try {
                if (result != null){
                    if (result.has(Key.success)){

                       Integer success = (Integer) result.get(Key.success);

                        if (success == 1){
                            parseJSONResponse(result);
                        } else {
                            Log.d("Last Trips Error:","Error in Last Trip fragment");
                        }
                    }
                }
            }
            catch (JSONException e) {
                e.printStackTrace();
            }
        }
        if(typeRequest==TYPEREQUEST.GET_SERVICE_CHARGE_AMOUNT){
            try {
                if (result != null){
                    if (result.has("serviceCharge")){
                        if(result.get("serviceCharge")!=null){
                            JSONArray jsonArrayServiceCharge=result.getJSONArray("serviceCharge");
                            if(jsonArrayServiceCharge.length()>0){
                                JSONObject serviceChargeDetail=jsonArrayServiceCharge.getJSONObject(0);
                                Key.SERVICE_CHARGE=serviceChargeDetail.getDouble("amount");
                            }
                        }
                    }
                }
            }
            catch (JSONException e) {
                e.printStackTrace();
            }}
    }

    @Override
    public void receiveError(VolleyError error, Context context, TYPEREQUEST typeRequest, @Nullable String userInfo, @Nullable ProgressDialog progressDialog) {
       if(progressDialog != null){
           progressDialog.dismiss();
       }
        CustomToast.getInstance().showToast(context, VolleyErrorMessage.handleVolleyErrors(context,error), ToastType.ERROR,Toast.LENGTH_SHORT);
    }


    public interface OnFragmentInteractionListener {
        // TODO: Update argument type and name
        void onFragmentInteraction(String title);
    }

    private void getServiceCharge(){

        UserProfile userProfile= SharedPref.getUserObject(getActivity(), SharedPrefKey.fileName);
        if(userProfile!=null) {
            String url = URLS.getServiceChargeAmount;
            HashMap<String,String> mHeaders= new HashMap<>();
            mHeaders.put("authToken", userProfile.getAuthToken());

//            progressDialog = new ProgressDialog(getActivity());
//            progressDialog.setMessage("Getting Service Charge Amount.");
//            progressDialog.setCancelable(false);

            NetworkRequestTest networkRequestTest = new NetworkRequestTest(getActivity(), url, Request.Method.GET, null, mHeaders, TYPEREQUEST.GET_SERVICE_CHARGE_AMOUNT, progressDialog, null);
            networkRequestTest.setListener(this);
            networkRequestTest.getResult();
        }


    }
}
