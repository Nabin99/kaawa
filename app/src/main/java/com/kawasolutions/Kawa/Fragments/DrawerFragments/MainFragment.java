package com.kawasolutions.Kawa.Fragments.DrawerFragments;

import android.Manifest;
import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.app.SearchManager;
import android.content.BroadcastReceiver;
import android.content.DialogInterface;
import android.content.IntentFilter;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.graphics.Color;
import android.graphics.drawable.BitmapDrawable;
import android.location.Address;
import android.location.Geocoder;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.os.Build;
import android.os.CountDownTimer;
import android.os.Handler;
import android.os.SystemClock;
import android.support.annotation.Nullable;
import android.support.design.widget.CoordinatorLayout;
import android.support.design.widget.Snackbar;
import android.support.v4.app.ActivityCompat;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.content.LocalBroadcastManager;
import android.support.v7.widget.SearchView;
import android.text.TextUtils;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.view.animation.Interpolator;
import android.view.animation.LinearInterpolator;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.VolleyError;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.Circle;
import com.google.android.gms.maps.model.CircleOptions;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.gson.Gson;
import com.kawasolutions.Kawa.Activities.RideInfo;
import com.kawasolutions.Kawa.Constants.Key;
import com.kawasolutions.Kawa.Constants.SharedPrefKey;
import com.kawasolutions.Kawa.Constants.URLS;
import com.kawasolutions.Kawa.CustomToast;
import com.kawasolutions.Kawa.DestinationAddress;
import com.kawasolutions.Kawa.Enum.RideStatus;
import com.kawasolutions.Kawa.Enum.TYPEREQUEST;
import com.kawasolutions.Kawa.Enum.ToastType;
import com.kawasolutions.Kawa.MKSnackBar;
import com.kawasolutions.Kawa.Models.DriverProfile;
import com.kawasolutions.Kawa.Models.RideDetails;
import com.kawasolutions.Kawa.Models.UserProfile;
import com.kawasolutions.Kawa.NetworkConnection;
import com.kawasolutions.Kawa.NetworkRequestTest;
import com.kawasolutions.Kawa.R;
import com.kawasolutions.Kawa.Services.RideRequestService;
import com.kawasolutions.Kawa.Utils.SharedPref;
import com.kawasolutions.Kawa.Utils.VolleyErrorMessage;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Random;
import java.util.Timer;
import java.util.TimerTask;

import it.sephiroth.android.library.tooltip.Tooltip;


public class MainFragment extends Fragment implements OnMapReadyCallback,GoogleMap.OnMapClickListener,NetworkRequestTest.NetworkListenerTest{


    private LocationManager locationManager;
    private LocationListener locationListener;
    private Snackbar snackbar;
    private GoogleMap mapView;
    private Marker mapMarker;
    private LatLng updatedLocation = Key.DefaultCoordinates;
    private NetworkConnection network;
    private Button rideNowBtn;
    private Integer[] userIdList;
    private CoordinatorLayout layout;
    private LatLng destinationLocation;
    private DestinationAddress destinationAddress;
    public static boolean active = false;
    private LinearLayout getTaxiLayout;
    private SearchManager searchManager;
    private SearchView searchView;
    private EditText searchEditText;
    private ArrayList<DriverProfile> driverProfiles;
    private HashMap<Marker,DriverProfile> driversMarkerMap;
    private ArrayList<Marker> driverMarkerList;
    private OnFragmentInteractionListener mListener;
    private Boolean gpsStatus = true;
    private Timer autoUpdate;
    public static CountDownTimer timer;
    public Boolean isTimerRunning = false;
    private ProgressDialog progressDialog;
    private Double mapRadius = 800.0;
    private Double extendedRadius = 1200.0;
    private Boolean extendedRange = false;
    private AlertDialog alertDialog;
    private AlertDialog.Builder extendedRangeBuilder = null;

    public MainFragment() {
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof OnFragmentInteractionListener) {
            mListener = (OnFragmentInteractionListener) context;
        } else {
            throw new RuntimeException(context.toString()
                    + " must implement OnFragmentInteractionListener");
        }
        onButtonPressed("");
    }

    // TODO: Rename method, update argument and hook method into UI event
    public void onButtonPressed(String title) {
        if (mListener != null) {
            mListener.onFragmentInteraction(title);
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setHasOptionsMenu(true);
        network = new NetworkConnection(getActivity());
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

        final View view = inflater.inflate(R.layout.fragment_main, container, false);
        rideNowBtn = (Button) view.findViewById(R.id.ride_now);
        layout = (CoordinatorLayout) view.findViewById(R.id.main_fragment_layout);
        getTaxiLayout = (LinearLayout) view.findViewById(R.id.get_taxi_layout);
        final Animation bottomUp = AnimationUtils.loadAnimation(getActivity(), R.anim.bottom_up);
        final Animation bottomDown = AnimationUtils.loadAnimation(getActivity(), R.anim.bottom_down);
        locationManager = (LocationManager) getActivity().getSystemService(Context.LOCATION_SERVICE);
        locationListener = new LocationListener() {
            @Override
            public void onLocationChanged(Location location) {
                if (location != null) {
                    updatedLocation = new LatLng(location.getLatitude(), location.getLongitude());
                    if (mapMarker == null) {
                        mapView.animateCamera(CameraUpdateFactory.newLatLngZoom(updatedLocation, 15));
                        mapMarker = mapView.addMarker(new MarkerOptions().title("Current Location").position(updatedLocation));
                        mapMarker.setIcon(BitmapDescriptorFactory.defaultMarker());
                        getDrivers();
                        //schedule 15 sec timer to update drivers in map
                        scheduleDriverUpdate();
                    } else {
                        mapMarker.setPosition(updatedLocation);
                    }
                }
            }

            @Override
            public void onStatusChanged(String provider, int status, Bundle extras) {
                Log.d("Location Status:","Status changed");
            }

            @Override
            public void onProviderEnabled(String provider) {
                gpsStatus = true;
                hideSnackBar();
                getLocation();
            }

            @Override
            public void onProviderDisabled(String provider) {
                gpsStatus = false;
                if (getActivity() != null && isAdded()) {
                    if(getTaxiLayout.getVisibility() == View.VISIBLE){
                        MKSnackBar.getSnackBar().showSnackBarWithAnchor(getActivity(),layout,getResources().getString(R.string.gps_disabled),Snackbar.LENGTH_INDEFINITE,R.id.get_taxi_layout,"Settings");
                    } else {
                        MKSnackBar.getSnackBar().showSnackBarWithAnchor(getActivity(),layout,getResources().getString(R.string.gps_disabled),Snackbar.LENGTH_INDEFINITE,R.id.ride_now,"Settings");
                    }
                }
            }
        };

        SupportMapFragment mapFragment = (SupportMapFragment) getChildFragmentManager().findFragmentById(R.id.mainmap);
        mapFragment.getMapAsync(this);

        rideNowBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if (locationManager.isProviderEnabled(LocationManager.GPS_PROVIDER) || locationManager.isProviderEnabled(LocationManager.NETWORK_PROVIDER)) {
                    if(network.isConnected()) {
                        if (rideNowBtn.getVisibility() == View.VISIBLE) {
                            rideNowBtn.setVisibility(View.INVISIBLE);
                            rideNowBtn.startAnimation(bottomDown);
                        }
                        if (getTaxiLayout.getVisibility() == View.INVISIBLE) {
                            hideSnackBar();
                            getTaxiLayout.setVisibility(View.VISIBLE);
                            getTaxiLayout.startAnimation(bottomUp);
                        }
                    }else {
                        MKSnackBar.getSnackBar().showSnackBarWithAnchor(getActivity(),layout,getResources().getString(R.string.no_internet),Snackbar.LENGTH_LONG,R.id.ride_now,null);
                    }
                } else {
                    MKSnackBar.getSnackBar().showSnackBarWithAnchor(getActivity(),layout,getResources().getString(R.string.gps_disabled),Snackbar.LENGTH_INDEFINITE,R.id.ride_now,"Settings");
                }
            }
        });

        getTaxiLayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(locationManager.isProviderEnabled(LocationManager.GPS_PROVIDER) || locationManager.isProviderEnabled(LocationManager.NETWORK_PROVIDER)) {
                    if (network.isConnected()) {
                        if (getTaxiLayout.getVisibility() == View.VISIBLE) {
                            getTaxiLayout.setVisibility(View.INVISIBLE);
                            getTaxiLayout.startAnimation(bottomDown);
                        }
                        if (rideNowBtn.getVisibility() == View.INVISIBLE) {
                            rideNowBtn.setVisibility(View.VISIBLE);
                            rideNowBtn.startAnimation(bottomUp);
                        }
                        sendRequestToDrivers(destinationLocation);
                    } else {
                        MKSnackBar.getSnackBar().showSnackBarWithAnchor(getActivity(), layout, getResources().getString(R.string.no_internet), Snackbar.LENGTH_LONG, R.id.get_taxi_layout, null);
                    }
                }else {
                    MKSnackBar.getSnackBar().showSnackBarWithAnchor(getActivity(),layout,getResources().getString(R.string.gps_disabled),Snackbar.LENGTH_INDEFINITE,R.id.get_taxi_layout,"Settings");
                }
            }
        });

        return view;
    }


    public void refreshMap() {

        UserProfile userProfile = SharedPref.getUserObject(getActivity(), SharedPrefKey.fileName);
        if(userProfile != null) {
            HashMap<String, String> mHeaders = new HashMap<>();
            mHeaders.put("authToken", userProfile.getAuthToken());

            HashMap<String,Object> params = new HashMap<>();
            params.put("latitude", updatedLocation.latitude);
            params.put("longitude", updatedLocation.longitude);
            params.put("customerId", userProfile.getCustomerID());

            if(!extendedRange) {
                NetworkRequestTest networkRequestTest = new NetworkRequestTest(getActivity(), URLS.nearbyDrivers, Request.Method.POST, params, mHeaders, TYPEREQUEST.NEARBYDRIVERS, null, "refresh");
                networkRequestTest.setListener(this);
                networkRequestTest.getResult();
            } else {
                NetworkRequestTest networkRequestTest = new NetworkRequestTest(getActivity(), URLS.nearbyDriversExtended, Request.Method.POST, params, mHeaders, TYPEREQUEST.NEARBYDRIVERS, null, "refresh");
                networkRequestTest.setListener(this);
                networkRequestTest.getResult();
            }
        }
    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
    }

    @Override
    public void onStop() {
        super.onStop();
        Log.d("Fragment Detach:", "On Stop");
    }

    public Bitmap createScaledTaxiMarker(){
        BitmapDrawable bitmapDrawable = (BitmapDrawable) getResources().getDrawable(R.drawable.taxi_marker);
        Bitmap b = bitmapDrawable.getBitmap();
        final float scale = getContext().getResources().getDisplayMetrics().density;
        int p = (int) (Key.CAR_SIZE * scale + 0.5f);
        return Bitmap.createScaledBitmap(b,p,p,false);
    }

    @Override
    public void onMapReady(GoogleMap googleMap) {
        mapView = googleMap;
        mapView.animateCamera(CameraUpdateFactory.newLatLngZoom(updatedLocation, 16));
        mapView.setMapType(GoogleMap.MAP_TYPE_NORMAL);
        mapView.setOnMapClickListener(this);
        getLocation();
    }

    public void getLocation() {
        MKSnackBar.getSnackBar().showSnackBarWithAnchor(getActivity(),layout,getResources().getString(R.string.search_driver),Snackbar.LENGTH_INDEFINITE,R.id.ride_now,null);
        configureLocation();
    }

    public void configureLocation() {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            if (ActivityCompat.checkSelfPermission(getActivity(), Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(getActivity(), Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
                requestPermissions(new String[]{Manifest.permission.ACCESS_FINE_LOCATION, Manifest.permission.ACCESS_COARSE_LOCATION, Manifest.permission.INTERNET}, 10);
            } else {
                requestLocationUpdate();
            }
        } else {
            requestLocationUpdate();
        }
    }

    private void requestLocationUpdate() {
        locationManager.requestLocationUpdates(LocationManager.GPS_PROVIDER, 3000, 0, locationListener);
        locationManager.requestLocationUpdates(LocationManager.NETWORK_PROVIDER, 3000, 0, locationListener);
    }


    public void getDrivers() {
        if (network.isConnected()) {
            if(active) {

                UserProfile userProfile = SharedPref.getUserObject(getActivity(), SharedPrefKey.fileName);
                if(userProfile != null) {
                    HashMap<String, String> mHeaders = new HashMap<>();
                    mHeaders.put("authToken", userProfile.getAuthToken());

                    HashMap<String,Object> params = new HashMap<>();
                    params.put("latitude", updatedLocation.latitude);
                    params.put("longitude", updatedLocation.longitude);
                    params.put("customerId", userProfile.getCustomerID());

                    NetworkRequestTest networkRequestTest = new NetworkRequestTest(getActivity(),URLS.nearbyDrivers, Request.Method.POST,params,mHeaders,TYPEREQUEST.NEARBYDRIVERS,null,null);
                    networkRequestTest.setListener(this);
                    networkRequestTest.getResult();

                }
            } else {
                Log.d("Driver Request:","" + "FALSE");
            }
        } else {
            MKSnackBar.getSnackBar().showSnackBarWithAnchor(getActivity(),layout,getResources().getString(R.string.no_internet),Snackbar.LENGTH_INDEFINITE,R.id.ride_now,null);
        }

    }

    public void startDialogTimer(final ProgressDialog dialog){
        timer = new CountDownTimer(70000,1000){

            @Override
            public void onTick(long millisUntilFinished) {
                isTimerRunning = true;
            }

            @Override
            public void onFinish() {
                isTimerRunning = false;
                if(getActivity() != null){
                    getActivity().runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            if(dialog != null){
                                if(dialog.isShowing()){
                                    dialog.dismiss();
                                }
                            }
                            showRequestFailedAlert();
                        }
                    });
                }
            }
        };
        timer.start();
    }

    public void scheduleDriverUpdate(){
        autoUpdate = new Timer();
        autoUpdate.schedule(new TimerTask() {
            @Override
            public void run() {
                if(getActivity() != null) {
                    getActivity().runOnUiThread(new Runnable() {
                        public void run() {
                            refreshMap();
                        }
                    });
                }
            }
        }, 0, 15000);
    }

    public void showRequestFailedAlert() {
        AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(getActivity());
        alertDialogBuilder.setTitle("No Driver Available");
        alertDialogBuilder.setCancelable(false);
        if(extendedRange){
            alertDialogBuilder.setMessage("All nearby drivers are busy but one should free up soon. Check back in a minute.");
            alertDialogBuilder.setPositiveButton("OK", new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int which) {
                    dialog.dismiss();
                }
            });
        }
        else {
            alertDialogBuilder.setMessage("All nearby drivers are busy. Do you want to increase the range? Additional Charges will apply.");
            alertDialogBuilder.setPositiveButton("YES", new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int which) {
                    dialog.dismiss();
                    increaseRange();
                }
            });
            alertDialogBuilder.setNegativeButton("NO", new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int which) {
                    dialog.dismiss();
                }
            });
        }
        AlertDialog alertDialog = alertDialogBuilder.create();
        alertDialog.show();
    }

    @Override
    public void onMapClick(LatLng latLng) {
      hideKeyboard();
    }



    public void hideSnackBar(){
        if(MKSnackBar.snackBar != null){
            if(MKSnackBar.snackBar.isShown()){
                MKSnackBar.snackBar.dismiss();
            }
        }
    }

    @Override
    public void receiveResult(JSONObject result, String url, Context context, TYPEREQUEST typeRequest, @Nullable String userInfo, @Nullable ProgressDialog progressDialog) {
        if(typeRequest == TYPEREQUEST.NEARBYDRIVERS && userInfo == null){
            try {
                if (result != null) {
                    JSONArray allDrivers = result.getJSONArray("driversInArea");
                    if(allDrivers.length() > 0){
                        Boolean showRideToolTip = SharedPref.readBooleanFromPreference(getActivity(),SharedPrefKey.fileName,SharedPrefKey.KEY_RIDE_INFO_WINDOW_SHOWN,false);
                        if(!showRideToolTip) {
                            addToolTipOnRideButton();
                        }
                        parseDriverData(allDrivers);
                        rideNowBtn.setEnabled(true);
                        rideNowBtn.setTextColor(context.getResources().getColor(R.color.colorPrimaryLight));
                        if(snackbar != null) {
                            if (snackbar.isShown()) {
                                snackbar.dismiss();
                            }
                        }
                    }
                    else {
                        rideNowBtn.setEnabled(false);
                        rideNowBtn.setTextColor(context.getResources().getColor(R.color.colorDivider));
                        MKSnackBar.getSnackBar().showSnackBarWithAnchor(getActivity(),layout,getResources().getString(R.string.no_driver_found),Snackbar.LENGTH_INDEFINITE,R.id.ride_now,null);
                        if(!extendedRange) {
                            showAlertForExtendedRange();
                        }
                    }

                } else {
                    Log.d("Network Request","Null Response");
                }
            }catch(Exception e){
                e.printStackTrace();
            }
        } else if (typeRequest == TYPEREQUEST.NEARBYDRIVERS && userInfo.equals("refresh")){
            try {
                if (result != null) {
                    if(driverMarkerList != null) {
                        for (int i = 0; i < driverMarkerList.size(); i++) {
                            Marker marker = driverMarkerList.get(i);
                            marker.remove();
                        }
                    }
                    JSONArray allDrivers = result.getJSONArray("driversInArea");
                    if(allDrivers.length() > 0){
                        if (locationManager.isProviderEnabled(LocationManager.GPS_PROVIDER) || locationManager.isProviderEnabled(LocationManager.NETWORK_PROVIDER)) {
                            hideSnackBar();
                        }
                        parseDriverData(allDrivers);
                        rideNowBtn.setEnabled(true);
                        rideNowBtn.setTextColor(context.getResources().getColor(R.color.colorPrimaryLight));
                    } else {
                        rideNowBtn.setEnabled(false);
                        rideNowBtn.setTextColor(context.getResources().getColor(R.color.colorDivider));
                        //Re-initialize driverprofile,marker hashmap and marker list
                        driverProfiles = new ArrayList<DriverProfile>();
                        driversMarkerMap = new HashMap<>();
                        driverMarkerList = new ArrayList<>();
                        //hideSnackBar();
                      
                        MKSnackBar.getSnackBar().showSnackBarWithAnchor(getActivity(), layout, getResources().getString(R.string.no_driver_found), Snackbar.LENGTH_INDEFINITE, R.id.ride_now, null);
                        if(!extendedRange) {
                            showAlertForExtendedRange();
                        }
                    }
                } else {
                    Log.d("Network Request","Null Response");
                }
            }catch(Exception e){
                e.printStackTrace();
            }
        } else if(typeRequest == TYPEREQUEST.SEND_NOTIFICATION){
            Log.d("DriverResult:","" + result);
            try{
                if(result.has(Key.success)){
                    Integer success = result.getInt(Key.success);
                    if(success == 1){
                        if(progressDialog != null){
                            progressDialog.setMessage("Waiting for Driver Response...");
                        }
                        Integer rideId = result.getInt("rideId");
                        Intent intent = new Intent(context, RideRequestService.class);
                        intent.putExtra("rideId",rideId);
                        context.startService(intent);
                    }
                }
            }catch (Exception e){
                e.printStackTrace();
            }
        }
    }

    @Override
    public void receiveError(VolleyError error, Context context, TYPEREQUEST typeRequest, @Nullable String userInfo, @Nullable ProgressDialog progressDialog) {
        if(typeRequest != TYPEREQUEST.NEARBYDRIVERS) {
            CustomToast.getInstance().showToast(context, VolleyErrorMessage.handleVolleyErrors(context, error), ToastType.ERROR, Toast.LENGTH_SHORT);
        } if(typeRequest != TYPEREQUEST.SEND_NOTIFICATION){
            if(progressDialog != null){
                progressDialog.dismiss();
            }
            CustomToast.getInstance().showToast(context, VolleyErrorMessage.handleVolleyErrors(context, error), ToastType.ERROR, Toast.LENGTH_SHORT);

        }
        Log.d("Nearby Driver Issue:","" + VolleyErrorMessage.handleVolleyErrors(context,error));
    }

    public interface OnFragmentInteractionListener {
        // TODO: Update argument type and name
        void onFragmentInteraction(String title);
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, String[] permissions, int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);

        switch (requestCode) {

            case 10:
                if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    if (ActivityCompat.checkSelfPermission(getActivity(), Manifest.permission.ACCESS_FINE_LOCATION) == PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(getActivity(), Manifest.permission.ACCESS_COARSE_LOCATION) == PackageManager.PERMISSION_GRANTED) {
                        requestLocationUpdate();
                    } else {
                        configureLocation();
                    }
                } else {
                    configureLocation();
                }
        }
    }

    public void showLocationRequiredAlert(){
        AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(getActivity());

        alertDialogBuilder.setTitle("Location Service Required");
        alertDialogBuilder.setMessage("Location service must be enable for this app to function properly.");
        alertDialogBuilder.setCancelable(false);

        alertDialogBuilder.setPositiveButton("OK", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                configureLocation();
            }
        });

        alertDialogBuilder.setNegativeButton("DENY", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                closeApplication();
            }
        });


        AlertDialog alertDialog = alertDialogBuilder.create();
        alertDialog.show();
    }

    public void showAlertForExtendedRange(){
        Boolean activeRide = SharedPref.readBooleanFromPreference(getActivity(), SharedPrefKey.fileName,SharedPrefKey.KEY_ON_RIDE,false);
        activeRide=false;//Increase your range dialog was not showing so...
        if(!activeRide) {
            if (extendedRangeBuilder == null) {
                extendedRangeBuilder = new AlertDialog.Builder(getActivity());
                extendedRangeBuilder.setTitle("Increase your Range");
                extendedRangeBuilder.setMessage("No drivers are found in your range. Do you want to increase the range? Additional Charges will apply.");
                extendedRangeBuilder.setCancelable(false);
                extendedRangeBuilder.setPositiveButton("YES", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.dismiss();
                        extendedRangeBuilder = null;
                        increaseRange();
                    }
                });
                extendedRangeBuilder.setNegativeButton("NO", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.dismiss();
                        extendedRangeBuilder = null;
                    }
                });

                alertDialog = extendedRangeBuilder.create();
                alertDialog.show();
            }
        }
    }

    public void increaseRange(){
        extendedRange = true;
        MKSnackBar.getSnackBar().showSnackBarWithAnchor(getActivity(),layout,getResources().getString(R.string.search_driver),Snackbar.LENGTH_INDEFINITE,R.id.ride_now,null);

        if(mapView != null && updatedLocation != null){
            Circle redCircle = mapView.addCircle(new CircleOptions().center(updatedLocation).radius(extendedRadius).strokeWidth(3).strokeColor(getResources().getColor(R.color.colorAccent)).fillColor(0x9599CCFF));
            if(mapView.getCameraPosition().zoom > 13) {
                mapView.animateCamera(CameraUpdateFactory.zoomOut());
            }
        }
    }

    public void parseDriverData(JSONArray allDrivers) throws JSONException{
        driverProfiles = new ArrayList<DriverProfile>();
        driversMarkerMap = new HashMap<>();
        driverMarkerList = new ArrayList<>();

        for(int i = 0 ; i < allDrivers.length() ; i++) {

            JSONObject driver = allDrivers.getJSONObject(i);

            JSONObject driverDetails = driver.getJSONObject("userId");
            JSONObject vehicleDetails = driver.getJSONObject("vehicleId");

            Integer userId = driverDetails.getInt("userId");
            String firstName = driver.getString("firstName");
            String lastName = driver.getString("lastName");
            String email = driver.optString("email");
            Double latitude = driver.optDouble("latitude");
            Double longitude = driver.optDouble("longitude");
            String phone = driverDetails.getString("contactNo");
            String vehicleType = vehicleDetails.optString("vehicleType");
            String vehicleNo = vehicleDetails.optString("vehicleNo");

            final DriverProfile profile = new DriverProfile();
            profile.setUserId(userId);
            profile.setFirstname(firstName);
            profile.setLastname(lastName);
            profile.setEmail(email);
            profile.setLocation(new LatLng(latitude,longitude));
            profile.setPhone(phone);
            profile.setVehicleType(vehicleType);
            profile.setVehicleNo(vehicleNo);

            if (profile.getLocation() != null) {

                Marker marker = mapView.addMarker(new MarkerOptions().title(profile.getFirstname() + " " + profile.getLastname()) .position(profile.getLocation()).anchor(0.5f,0.5f).snippet(profile.getVehicleNo()).icon(BitmapDescriptorFactory.fromBitmap(createScaledTaxiMarker())));

                //rotate marker
                rotateMarker(marker,generateRandomNumber());
                //marker List
                driverMarkerList.add(marker);
                //Marker and Driver Hashmap
                driversMarkerMap.put(marker, profile);
                //List of available drivers
                driverProfiles.add(profile);

                mapView.setOnMarkerClickListener(new GoogleMap.OnMarkerClickListener() {
                    @Override
                    public boolean onMarkerClick(Marker marker) {
                        Log.w("TAG", "onMarkerClick: clciked "+profile.getUserId());
//                        Toast.makeText(getContext(),profile.getUserId()+"",Toast.LENGTH_LONG).show();
                        marker.showInfoWindow();
                        final String vehicelNumber=marker.getSnippet();

                        AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(getContext());
                        alertDialogBuilder.setTitle("Ride Request");
                        alertDialogBuilder.setMessage("Are you sure you want to request "+ vehicelNumber+" ?");
                        alertDialogBuilder.setCancelable(false);

                        alertDialogBuilder.setPositiveButton("YES", new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                Log.w("TAG", "onMarkerClick: yes ");
                                dialog.dismiss();
                                sendNotificationToParticularDriver(vehicelNumber);
                            }
                        });

                        alertDialogBuilder.setNegativeButton("NO", new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                Log.w("TAG", "onMarkerClick: yes ");
                                dialog.dismiss();
                            }
                        });

                        AlertDialog alertDialog = alertDialogBuilder.create();
                        alertDialog.show();

                        return true;
                    }
                });
            }
        }
    }

    public void rotateMarker(final Marker marker, final float toRotation) {
        final Handler handler = new Handler();
        final long start = SystemClock.uptimeMillis();
        final float startRotation = marker.getRotation();
        final long duration = 1555;

        final Interpolator interpolator = new LinearInterpolator();

        handler.post(new Runnable() {
            @Override
            public void run() {
                long elapsed = SystemClock.uptimeMillis() - start;
                float t = interpolator.getInterpolation((float) elapsed / duration);

                float rot = t * toRotation + (1 - t) * startRotation;

                marker.setRotation(-rot > 180 ? rot / 2 : rot);
                if (t < 1.0) {
                    // Post again 16ms later.
                    handler.postDelayed(this, 16);
                }
            }
        });
    }

    public float generateRandomNumber(){
        float[] numArray = {25,75,90,60,180,270};
        int num = new Random().nextInt(numArray.length);
        return numArray[num];
    }

    @Override
    public void onPause() {
        super.onPause();

        active = false;
        LocalBroadcastManager.getInstance(getActivity()).unregisterReceiver(rideActivityReceiver);

        if (ActivityCompat.checkSelfPermission(getActivity(), Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(getActivity(), Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            return;
        }
        locationManager.removeUpdates(locationListener);

        stopTimer();

        Log.d("Fragment Detach:", "On Pause");

    }

    public void stopTimer(){
        if(autoUpdate != null) {
            autoUpdate.cancel();
            autoUpdate.purge();
        }
    }

    @Override
    public void onResume() {
        super.onResume();
        Log.d("Fragment Status:", "ON RESUME");

        active = true;
        LocalBroadcastManager.getInstance(getActivity()).registerReceiver(rideActivityReceiver,
                new IntentFilter("rideActivity"));
        if(autoUpdate != null){
            scheduleDriverUpdate();
        }
        //Close progress Dialog if Ride Request was expired
        if(!SharedPref.readBooleanFromPreference(getActivity(),SharedPrefKey.fileName,SharedPrefKey.KEY_RIDE_REQUEST,false)){
            if(progressDialog != null){
                progressDialog.dismiss();
            }
        }
        configureLocation();
    }

    @Override
    public void onStart() {
        super.onStart();
        Log.d("Fragment Status:", "ON START");
    }

    @Override
    public void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        super.onCreateOptionsMenu(menu, inflater);

        inflater.inflate(R.menu.search_menu, menu);
        MenuItem item = menu.findItem(R.id.search);
        addSearchView(item);

    }

    public void addSearchView(MenuItem item){
        searchManager = (SearchManager) getActivity().getSystemService(Context.SEARCH_SERVICE);
        searchView = (SearchView) item.getActionView();
        searchView.setSearchableInfo(searchManager.getSearchableInfo(getActivity().getComponentName()));
        searchView.setIconifiedByDefault(false);
        searchView.setFocusable(false);

        searchEditText = (EditText) searchView.findViewById(android.support.v7.appcompat.R.id.search_src_text);
        searchEditText.setBackgroundColor(getResources().getColor(R.color.colorLight));
        searchEditText.setTextColor(getResources().getColor(R.color.colorBlack));
        searchEditText.setHintTextColor(getResources().getColor(R.color.colorDivider));
        searchEditText.setTextSize(14);
        searchView.setOnQueryTextListener(new SearchView.OnQueryTextListener() {
            @Override
            public boolean onQueryTextSubmit(String query) {
                if(updatedLocation != Key.DefaultCoordinates) {
                    if (network.isConnected()) {
                        addresstoLocation(query);
                        return true;
                    } else {
                        CustomToast.getInstance().showToast(getActivity(), getResources().getString(R.string.no_internet), ToastType.ERROR, Toast.LENGTH_SHORT);
                    }
                } else {
                    CustomToast.getInstance().showToast(getActivity(), "Please Enable your GPS ", ToastType.ERROR, Toast.LENGTH_SHORT);

                }
                return true;
            }

            @Override
            public boolean onQueryTextChange(String newText) {
                return false;
            }
        });
    }

    public void addresstoLocation(String address){
//
        Geocoder geocoder = new Geocoder(getActivity());
        List<Address> addresses;

        try{
                addresses = geocoder.getFromLocationName(address,5);
            if(addresses.size() == 0){
                Toast.makeText(getActivity(), "Location not Found", Toast.LENGTH_SHORT).show();
            }
            else {
                Address location = addresses.get(0);
                destinationLocation = new LatLng(location.getLatitude(),location.getLongitude());

                if(destinationAddress != null){
                    destinationAddress.clearMap();
                }
                destinationAddress = new DestinationAddress(getActivity(), mapView, destinationLocation, updatedLocation);
                destinationAddress.markDestination();
                searchView.clearFocus();
            }
        } catch (Exception e){
            e.printStackTrace();
        }

    }

    public BroadcastReceiver rideActivityReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            String status = intent.getExtras().getString("status");
            if(!TextUtils.isEmpty(status)){
                if(status.equals(RideStatus.EXPIRED.toString())){
                    if(progressDialog != null){
                        progressDialog.dismiss();
                    }
                    showRequestFailedAlert();
                } else if(status.equals(RideStatus.ACCEPTED.toString())){
                    if(progressDialog != null){
                        progressDialog.dismiss();
                    }
                    Gson gson = new Gson();
                    String rideDetailsString = intent.getExtras().getString("rideDetails");
                    RideDetails ride = gson.fromJson(rideDetailsString,RideDetails.class);
                    showDriverAcceptedDialog(ride);
                }
            }else {
                Toast.makeText(context, "Status Empty", Toast.LENGTH_SHORT).show();
            }

        }
    };

    public void showDriverAcceptedDialog(final RideDetails ride){
        AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(getActivity());

        alertDialogBuilder.setTitle("Request Success");
        alertDialogBuilder.setMessage("Your driver will be there in a few minutes.");
        alertDialogBuilder.setCancelable(false);
        alertDialogBuilder.setPositiveButton("OK", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.dismiss();

                Intent i = new Intent(getActivity(), RideInfo.class);
                i.putExtra("rideId", ride.getRideId());
                i.putExtra("driverFirstName", ride.getDriverProfile().getFirstname());
                i.putExtra("driverLastName",ride.getDriverProfile().getLastname());
                i.putExtra("driverPhone", ride.getDriverProfile().getPhone());
                i.putExtra("driverLatitude", ride.getDriverProfile().getLocation().latitude);
                i.putExtra("driverLongitude", ride.getDriverProfile().getLocation().longitude);
                i.putExtra("carModel", ride.getDriverProfile().getVehicleType());
                i.putExtra("carNumber", ride.getDriverProfile().getVehicleNo());
                i.putExtra("driverRating", ride.getDriverProfile().getRating());
                i.putExtra("driverImage",ride.getDriverProfile().getImageName());
                i.putExtra("otpCode",ride.getOtpCode());
                i.putExtra("trackRideURL",ride.getTrackRideUrl());
                i.putExtra("extendedRange",ride.getExtendedRange());
                i.putExtra("extendedRangePrice",ride.getExtendedRangePrice());

                if(ride.getDestinationLocation() != null){
                    i.putExtra("destinationLatitude", ride.getDestinationLocation().latitude);
                    i.putExtra("destinationLongitude", ride.getDestinationLocation().longitude);
                }
                i.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_NEW_TASK);
                startActivity(i);
            }
        });

        AlertDialog alertDialog = alertDialogBuilder.create();
        alertDialog.show();
    }


    public void hideKeyboard(){
        InputMethodManager imm = (InputMethodManager) getActivity().getSystemService(Context.INPUT_METHOD_SERVICE);
        if(imm.isActive()) {
            if(getView().getWindowToken() != null) {
                imm.hideSoftInputFromWindow(getView().getWindowToken(), 0);
            }
        }
    }

    public void sendRequestToDrivers(@Nullable LatLng destinationLocation){

        Boolean pendingRequest = SharedPref.readBooleanFromPreference(getActivity(),SharedPrefKey.fileName,SharedPrefKey.KEY_RIDE_REQUEST,false);
        if(!pendingRequest) {
            ArrayList<Integer> driverIDList = new ArrayList<>();
            if (driverProfiles != null) {
                for (int i = 0; i < driverProfiles.size(); i++) {
                    DriverProfile profile = driverProfiles.get(i);
                    driverIDList.add(profile.getUserId());
                }

                UserProfile userProfile = SharedPref.getUserObject(getActivity(), SharedPrefKey.fileName);
                if (userProfile != null) {
                    Double destinationLatitude = null;
                    Double destinationLongitude = null;
                    if (destinationLocation != null) {
                        destinationLatitude = destinationLocation.latitude;
                        destinationLongitude = destinationLocation.longitude;
                    }
                    HashMap<String, Object> params = new HashMap<>();
                    params.put("driverList", driverIDList);
                    params.put("latitude", updatedLocation.latitude);
                    params.put("longitude", updatedLocation.longitude);
                    params.put("destinationLatitude", destinationLatitude);
                    params.put("destinationLongitude", destinationLongitude);
                    params.put("customerId", userProfile.getCustomerID());
                    params.put("extendedRange", extendedRange);

                    HashMap<String, String> mHeaders = new HashMap<>();
                    mHeaders.put("authToken", userProfile.getAuthToken());

                    progressDialog = new ProgressDialog(getActivity());
                    progressDialog.setMessage("Sending Driver Request....");
                    progressDialog.setCancelable(false);
                    NetworkRequestTest networkRequestTest = new NetworkRequestTest(getActivity(), URLS.sendNotification, Request.Method.POST, params, mHeaders, TYPEREQUEST.SEND_NOTIFICATION, progressDialog, null);
                    networkRequestTest.setListener(this);
                    networkRequestTest.getResult();
                }
            } else {
                CustomToast.getInstance().showToast(getActivity(), "No Drivers Available", ToastType.ERROR, Toast.LENGTH_SHORT);
                Log.d("Driver Request:", "Driver Profile Null");
            }
        } else {
            SharedPref.saveToPreferences(getActivity(),SharedPrefKey.fileName,SharedPrefKey.KEY_RIDE_REQUEST,false);
            sendRequestToDrivers(destinationLocation);
//            showPendingRequestDialog();
        }
    }

    public void showPendingRequestDialog(){
        AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(getActivity());
        alertDialogBuilder.setTitle("Request Pending");
        alertDialogBuilder.setMessage("Your previous ride request is still pending. Please wait for a few seconds....");
        alertDialogBuilder.setCancelable(true);

        alertDialogBuilder.setPositiveButton("OK", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.dismiss();
            }
        });

        AlertDialog alertDialog = alertDialogBuilder.create();
        alertDialog.show();
    }

    public void addToolTipOnRideButton(){
        Tooltip.make(getActivity(), new Tooltip.Builder(101)
                .anchor(rideNowBtn, Tooltip.Gravity.TOP)
                .closePolicy(new Tooltip.ClosePolicy()
                        .insidePolicy(true, false)
                        .outsidePolicy(true, false), 8000)
                .activateDelay(2000)
                .showDelay(300)
                .text("Tap on RIDE NOW Button and Confirm your Booking")
                .maxWidth(800)
                .withStyleId(R.style.ToolTipLayoutDefaultStyle)
                .withArrow(true)
                .withOverlay(true)
                .floatingAnimation(Tooltip.AnimationBuilder.DEFAULT)
                .build()
        ).show();
        SharedPref.saveToPreferences(getActivity(),SharedPrefKey.fileName,SharedPrefKey.KEY_RIDE_INFO_WINDOW_SHOWN,true);
    }

    public void closeApplication(){
        Intent intentHome = new Intent(Intent.ACTION_MAIN);
        intentHome.addCategory(Intent.CATEGORY_HOME);
        intentHome.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        startActivity(intentHome);
    }

    public void sendNotificationToParticularDriver(String vehicleNumber){

        UserProfile userProfile = SharedPref.getUserObject(getActivity(), SharedPrefKey.fileName);
        if(userProfile!=null){

            Double destinationLatitude = null;
            Double destinationLongitude = null;
        HashMap<String, Object> params = new HashMap<>();
        params.put("vehicleNumber", vehicleNumber);
        params.put("latitude", updatedLocation.latitude);
        params.put("longitude", updatedLocation.longitude);
        params.put("destinationLatitude", destinationLatitude);
        params.put("destinationLongitude", destinationLongitude);
        params.put("customerId", userProfile.getCustomerID());
        params.put("extendedRange", extendedRange);

        HashMap<String, String> mHeaders = new HashMap<>();
        mHeaders.put("authToken", userProfile.getAuthToken());

        progressDialog = new ProgressDialog(getActivity());
        progressDialog.setMessage("Sending Driver ("+vehicleNumber+") Request....");
        progressDialog.setCancelable(false);
        NetworkRequestTest networkRequestTest = new NetworkRequestTest(getActivity(), URLS.sendNotificationToParticularDriver, Request.Method.POST, params, mHeaders, TYPEREQUEST.SEND_NOTIFICATION, progressDialog, null);
        networkRequestTest.setListener(this);
        networkRequestTest.getResult();
        }else{
            String toastMessage="Driver of taxi "+vehicleNumber+" is busy";
            CustomToast.getInstance().showToast(getActivity(), "Session Expired Please Login to continue.", ToastType.ERROR, Toast.LENGTH_SHORT);
            Log.d("Driver Request:", "userProfiel not found");
        }
    }

}
