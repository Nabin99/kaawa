package com.kawasolutions.Kawa.Fragments.DrawerFragments;

import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.VolleyError;
import com.kawasolutions.Kawa.Activities.RideInfo;
import com.kawasolutions.Kawa.Adapter.RecyclerViewAdapter;
import com.kawasolutions.Kawa.Constants.Key;
import com.kawasolutions.Kawa.Constants.SharedPrefKey;
import com.kawasolutions.Kawa.Constants.URLS;
import com.kawasolutions.Kawa.CustomToast;
import com.kawasolutions.Kawa.Enum.TYPEREQUEST;
import com.kawasolutions.Kawa.Enum.ToastType;
import com.kawasolutions.Kawa.Models.UserProfile;
import com.kawasolutions.Kawa.NetworkRequestTest;
import com.kawasolutions.Kawa.R;
import com.kawasolutions.Kawa.Utils.SharedPref;
import com.kawasolutions.Kawa.Utils.VolleyErrorMessage;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;

/**
 * A simple {@link Fragment} subclass.
 * Activities that contain this fragment must implement the
 * {@link CurrentRideListFragment.OnFragmentInteractionListener} interface
 * to handle interaction events.
 * Use the {@link CurrentRideListFragment#newInstance} factory method to
 * create an instance of this fragment.
 */
public class CurrentRideListFragment extends Fragment implements NetworkRequestTest.NetworkListenerTest {

    private static final String TAG = "CurrentRideListFragment";

    // TODO: Rename parameter arguments, choose names that match
    // the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
    private static final String ARG_PARAM1 = "param1";
    private static final String ARG_PARAM2 = "param2";

    // TODO: Rename and change types of parameters
    private String mParam1;
    private String mParam2;

    private ProgressDialog progressDialog;

    private ArrayList<String> mTaxiNumbers=new ArrayList<>();
    private ArrayList<String> mOTPCodes=new ArrayList<>();
    private ArrayList<String> mDates=new ArrayList<>();
    private ArrayList<String> mNames=new ArrayList<>();
    private ArrayList<String> mCarTypes=new ArrayList<>();
    private ArrayList<String> mRideIds=new ArrayList<>();
    private ArrayList<String> mRideStatus=new ArrayList<>();

    private RecyclerView recyclerView;
    private LinearLayout noDataLayout;
    private TextView noCancellableRide;
    private RecyclerViewAdapter rvAdapter;

    private OnFragmentInteractionListener mListener;

    public CurrentRideListFragment() {
        // Required empty public constructor
    }

    /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters.
     *
     * @param param1 Parameter 1.
     * @param param2 Parameter 2.
     * @return A new instance of fragment CurrentRideListFragment.
     */
    // TODO: Rename and change types and number of parameters
    public static CurrentRideListFragment newInstance(String param1, String param2) {
        CurrentRideListFragment fragment = new CurrentRideListFragment();
        Bundle args = new Bundle();
        args.putString(ARG_PARAM1, param1);
        args.putString(ARG_PARAM2, param2);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            mParam1 = getArguments().getString(ARG_PARAM1);
            mParam2 = getArguments().getString(ARG_PARAM2);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        Log.d(TAG, "onCreateView: Started");
        final View view=inflater.inflate(R.layout.activity_current_rides,container,false);


        recyclerView=view.findViewById(R.id.recyclerView);
        noCancellableRide=(TextView)view.findViewById(R.id.no_cancellable_trips);
        noDataLayout=(LinearLayout)view.findViewById(R.id.no_data_layout);
        rvAdapter=new RecyclerViewAdapter(mTaxiNumbers,mOTPCodes,mNames,mDates,mCarTypes,mRideIds,mRideStatus,getContext());
        recyclerView.setAdapter(rvAdapter);
        recyclerView.setLayoutManager(new LinearLayoutManager(getContext()));
//        view.findViewById(R.id.OTPCode).setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//                TextView tv=(TextView)view.findViewById(R.id.textOTPCode);
//                String otpc=tv.getText().toString();
//                Toast.makeText(getContext(),otpc,Toast.LENGTH_LONG).show();
//            }
//        });
        initListView();
        return view;
    }

    // TODO: Rename method, update argument and hook method into UI event
    public void onButtonPressed(String title) {
        if (mListener != null) {
            mListener.onFragmentInteraction(title);
        }
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof OnFragmentInteractionListener) {
            mListener = (OnFragmentInteractionListener) context;
        } else {
            throw new RuntimeException(context.toString()
                    + " must implement OnFragmentInteractionListener");
        }
        onButtonPressed(getResources().getString(R.string.drawer_title_current_rides));
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }

    public void parseJSONResponse(JSONObject response){

        try {
            JSONArray details = response.getJSONArray("lastTripList");

            if(details.length() > 0) {
                if(noDataLayout.getVisibility() == View.VISIBLE){
                    noDataLayout.setVisibility(View.INVISIBLE);
                }

                for (int i = 0; i < details.length(); i++) {

                    JSONObject lastRideDetail = details.getJSONObject(i);
                    String rideId = lastRideDetail.getString("rideId");
                    String driverName = lastRideDetail.getString("firstName") + " " + lastRideDetail.getString("lastName");
                    String car_no = lastRideDetail.getString("vehicleNo");
                    String date = lastRideDetail.getString("rideDate");
                    String successStatus = lastRideDetail.getString("successStatus");
                    String cancelStatus = lastRideDetail.getString("cancelStatus");
                    String status=lastRideDetail.getString("status");
                    String otpCode=lastRideDetail.getString("otpCode");
                    String carType=lastRideDetail.getString("vehicleType");
                    boolean startStatus=lastRideDetail.getBoolean("startStatus");

                    mTaxiNumbers.add(car_no);
                    mOTPCodes.add(otpCode);
                    mDates.add(date);
                    mNames.add(driverName);
                    mCarTypes.add(carType);
                    mRideIds.add(rideId);
                    if(startStatus){
                    mRideStatus.add("Ride Has Started");
                    }else{
                        mRideStatus.add("Ride has not Started");
                    }

                    rvAdapter.notifyDataSetChanged();
                }
            } else {
                if(noDataLayout.getVisibility() == View.INVISIBLE){
                    noDataLayout.setVisibility(View.VISIBLE);
                    noCancellableRide.setText(getResources().getString(R.string.noCancellableRides));
                }


            }
        } catch (JSONException e) {
            e.printStackTrace();
        }

    }

    @Override
    public void receiveResult(JSONObject result, String url, Context context, TYPEREQUEST typeRequest, @Nullable String userInfo, @Nullable ProgressDialog progressDialog) {
        if(progressDialog != null){
            progressDialog.dismiss();
        }
        if(typeRequest == TYPEREQUEST.ACTIVE_RIDES){
            try {
                if (result != null){
                    if (result.has(Key.success)){
                        Integer success = (Integer) result.get("success");
                        if (success == 1){
                            parseJSONResponse(result);
                        } else {
                            Log.d(TAG, "receiveResult: Error while getting active Rides");
                        }
                    }
                }
            }
            catch (JSONException e) {
                e.printStackTrace();
            }
        }
//        if(typeRequest== TYPEREQUEST.CANCEL_RIDE){
//            try {
//                if (result.has(Key.success)) {
//                    Integer success = (Integer) result.get(Key.success);
//                    if (success == 1) {
//                        CustomToast.getInstance().showToast(context, context.getResources().getString(R.string.ride_cancel), ToastType.SUCCESS, Toast.LENGTH_LONG);
//                        //Clear Ride details from sharedPref
//                        SharedPref.saveToPreferences(context, SharedPrefKey.fileName, SharedPrefKey.KEY_ON_RIDE, false);
//                        SharedPref.saveToPreferences(context, SharedPrefKey.fileName, SharedPrefKey.KEY_RIDE_DETAILS, "");
//                        SharedPref.saveToPreferences(context, SharedPrefKey.fileName, SharedPrefKey.KEY_RIDE_STARTED, false);
//
//                        if(RideInfo.updateTimer != null){
//                            RideInfo.updateTimer.cancel();
//                            RideInfo.updateTimer.purge();
//                        }
//
//                    } else {
//                        CustomToast.getInstance().showToast(context, "Something went Wrong..", ToastType.ERROR, Toast.LENGTH_SHORT);
//                    }
//                } else {
//                    Integer error = (Integer) result.get("error");
//                    if(error == 1)// Error 1 means ride has already started
//                    {
//                        SharedPref.saveToPreferences(context,SharedPrefKey.fileName,SharedPrefKey.KEY_RIDE_STARTED,true);
//                        CustomToast.getInstance().showToast(context, "You cannot cancel the ride. Ride has already started", ToastType.ERROR, Toast.LENGTH_SHORT);
//                    } else if(error == 2)//Error 2 means ride has already completed
//                    {
//                        CustomToast.getInstance().showToast(context, "This ride has already completed...", ToastType.SUCCESS, Toast.LENGTH_LONG);
//                        //Clear Ride details from sharedPref
//                        SharedPref.saveToPreferences(context, SharedPrefKey.fileName, SharedPrefKey.KEY_ON_RIDE, false);
//                        SharedPref.saveToPreferences(context, SharedPrefKey.fileName, SharedPrefKey.KEY_RIDE_DETAILS, "");
//
//                    }
//                }
//                initListView();
//            } catch (Exception e){
//                e.printStackTrace();
//            }
//        }
    }

    @Override
    public void receiveError(VolleyError error, Context context, TYPEREQUEST typeRequest, @Nullable String userInfo, @Nullable ProgressDialog progressDialog) {
        if(progressDialog != null){
            progressDialog.dismiss();
        }
        CustomToast.getInstance().showToast(context, VolleyErrorMessage.handleVolleyErrors(context,error), ToastType.ERROR, Toast.LENGTH_SHORT);
//        Toast.makeText(context,"Helo",Toast.LENGTH_LONG).show();
    }

    /**
     * This interface must be implemented by activities that contain this
     * fragment to allow an interaction in this fragment to be communicated
     * to the activity and potentially other fragments contained in that
     * activity.
     * <p>
     * See the Android Training lesson <a href=
     * "http://developer.android.com/training/basics/fragments/communicating.html"
     * >Communicating with Other Fragments</a> for more information.
     */
    public interface OnFragmentInteractionListener {
        // TODO: Update argument type and name
        void onFragmentInteraction(String title);
    }

    private void initListView(){
        Log.d(TAG, "initListView: preparing list");

        UserProfile userProfile= SharedPref.getUserObject(getActivity(), SharedPrefKey.fileName);
        if(userProfile!=null) {
            String url = URLS.activeRides + userProfile.getCustomerID();
            HashMap<String,String> mHeaders= new HashMap<>();
            mHeaders.put("authToken", userProfile.getAuthToken());

            progressDialog = new ProgressDialog(getActivity());
            progressDialog.setMessage("Getting Current Rides..Please Wait.");
            progressDialog.setCancelable(false);

            NetworkRequestTest networkRequestTest = new NetworkRequestTest(getActivity(), url, Request.Method.GET, null, mHeaders, TYPEREQUEST.ACTIVE_RIDES, progressDialog, null);
            networkRequestTest.setListener(this);
            networkRequestTest.getResult();
        }

    }


}
