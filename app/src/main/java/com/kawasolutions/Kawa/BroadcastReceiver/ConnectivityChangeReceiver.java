package com.kawasolutions.Kawa.BroadcastReceiver;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Bundle;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.Toast;

import com.kawasolutions.Kawa.Constants.Key;
import com.kawasolutions.Kawa.R;

/**
 * Created by apple on 4/21/16.
 */
public class ConnectivityChangeReceiver extends BroadcastReceiver {

    @Override
    public void onReceive(Context context, Intent intent) {
      debugIntent(intent,context);
    }

    public ConnectivityChangeReceiver() {
        super();
    }

    public void debugIntent(Intent intent, Context context){

        Bundle extras = intent.getExtras();
        if (extras != null) {
            NetworkInfo networkInfo = intent.getParcelableExtra(ConnectivityManager.EXTRA_NETWORK_INFO);
            if(!networkInfo.isConnected()){
                Key.isNetworkConnected = false;
                Toast.makeText(context, "No Internet Connection", Toast.LENGTH_SHORT).show();
                //Log.v(tag, "key [" + key + "]: " + extras.get(key));

                /*LayoutInflater inflater = LayoutInflater.from(context);
                View layout = inflater.inflate(R.layout.custom_toast,null);
                LinearLayout l = (LinearLayout) layout.findViewById(R.id.toast_layout_root);
                //View s = layout.findViewById(R.id.toast_layout_root);
                Toast toast = new Toast(context);
                toast.setGravity(Gravity.CENTER_VERTICAL, 0, 0);
                toast.setDuration(Toast.LENGTH_LONG);
                toast.setView(l);
                toast.show();*/

            }else{
                Key.isNetworkConnected = true;
            }
        }
    }


}
