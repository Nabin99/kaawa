package com.kawasolutions.Kawa;

import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.net.ConnectivityManager;
import android.support.annotation.Nullable;
import android.util.Log;
import android.view.WindowManager;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.Volley;
import com.facebook.HttpMethod;
import com.google.android.gms.common.api.Api;
import com.kawasolutions.Kawa.Activities.LoginActivity;
import com.kawasolutions.Kawa.Activities.MainActivity;
import com.kawasolutions.Kawa.Constants.SharedPrefKey;
import com.kawasolutions.Kawa.Constants.URLS;
import com.kawasolutions.Kawa.Enum.TYPEREQUEST;
import com.kawasolutions.Kawa.Singleton.VolleySingleton;
import com.kawasolutions.Kawa.Utils.SharedPref;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;

/**
 * Created by nabin on 1/27/17.
 */

public class NetworkRequest{

    public interface NetworkListener {
        void receiveResult(JSONObject result, String url, Context context, TYPEREQUEST typeRequest,@Nullable String userInfo,@Nullable ProgressDialog progressDialog);

        void receiveError(VolleyError error, Context context, TYPEREQUEST typeRequest,@Nullable String userInfo,@Nullable ProgressDialog progressDialog);
    }

    private String url;
    private HashMap<String, Object> parameters;
    private HashMap<String, String> headers;
    private int method;
    private NetworkListener listener;
    private String loaderMsg;
    private Context context;
    private Boolean isLoaderHidden;
    private TYPEREQUEST typeRequest;
    public static ProgressDialog progressDialog;
    private VolleySingleton volleySingleton;
    private RequestQueue requestQueue;
    public Boolean keepProgressDialogRunning = false;
    @Nullable
    private String userInfo;

    public NetworkRequest(Context context, String url, int method, TYPEREQUEST typeRequest, Boolean isLoaderHidden, String loaderMsg, @Nullable String userInfo) {
        this.context = context;
        this.url = url;
        this.method = method;
        this.typeRequest = typeRequest;
        this.isLoaderHidden = isLoaderHidden;
        this.loaderMsg = loaderMsg;
        this.userInfo = userInfo;
    }

    public NetworkRequest(Context context, String url, int method, TYPEREQUEST typeRequest, HashMap<String, Object> parameters,Boolean isLoaderHidden, String loaderMsg,@Nullable String userInfo) {
        this.context = context;
        this.url = url;
        this.method = method;
        this.typeRequest = typeRequest;
        this.parameters = parameters;
        this.isLoaderHidden = isLoaderHidden;
        this.loaderMsg = loaderMsg;
        this.userInfo = userInfo;
    }

    public NetworkRequest(Context context, String url, int method, TYPEREQUEST typeRequest, HashMap<String, Object> parameters,HashMap<String,String> headers,Boolean isLoaderHidden, String loaderMsg,@Nullable String userInfo) {
        this.context = context;
        this.url = url;
        this.method = method;
        this.typeRequest = typeRequest;
        this.parameters = parameters;
        this.headers = headers;
        this.isLoaderHidden = isLoaderHidden;
        this.loaderMsg = loaderMsg;
        this.userInfo = userInfo;
    }

    public void setListener(NetworkListener listener) {
        this.listener = listener;
    }

    public void getResult() {

        volleySingleton = VolleySingleton.getInstance();
        requestQueue = volleySingleton.getRequestQueue();
        JsonObjectRequest jsonObjectRequest = null;
        JSONObject paramObject = null;

        if(parameters != null) {
            if (!parameters.isEmpty()) {
                paramObject = new JSONObject(parameters);
            }
        }

        if(parameters == null && headers == null){
            jsonObjectRequest = new JsonObjectRequest(method, url, new Response.Listener<JSONObject>() {
                @Override
                public void onResponse(JSONObject response) {
                    if(!keepProgressDialogRunning) {
                           // progressDialog.dismiss();
                    }
                    if (response != null) {
                        if (response.has("authError")){
                            try {
                                Integer error = response.getInt("authError");
                                if(error == 1){
                                    showAuthErrorAlert();
                                }
                            } catch (JSONException e) {
                                e.printStackTrace();
                            }
                        } else {
                            listener.receiveResult(response, url, context, typeRequest, userInfo, progressDialog);
                        }
                    } else {
                        Log.d("Volley REsponse:","Volley Response is null");
                    }
                }
            }, new Response.ErrorListener() {
                @Override
                public void onErrorResponse(VolleyError error) {
                        progressDialog.dismiss();
                    if (error != null) {
                        listener.receiveError(error, context, typeRequest,userInfo,progressDialog);
                    }
                }
            });

        } else if (parameters != null && headers == null){
            jsonObjectRequest = new JsonObjectRequest(method, url,paramObject, new Response.Listener<JSONObject>() {
                @Override
                public void onResponse(JSONObject response) {
                    if(!keepProgressDialogRunning) {
                            progressDialog.dismiss();
                    }
                    if (response != null) {
                        if (response.has("authError")){
                            try {
                                Integer error = response.getInt("authError");
                                if(error == 1){
                                    showAuthErrorAlert();
                                }
                            } catch (JSONException e) {
                                e.printStackTrace();
                            }
                        } else {
                            listener.receiveResult(response, url, context, typeRequest, userInfo, progressDialog);
                        }                    } else {
                        Log.d("Volley REsponse:","Volley Response is null");
                    }
                }
            }, new Response.ErrorListener() {
                @Override
                public void onErrorResponse(VolleyError error) {
                        progressDialog.dismiss();
                    if (error != null) {
                        listener.receiveError(error, context, typeRequest,userInfo,progressDialog);
                    }
                }
            });
        } else if (parameters != null && headers!= null){
            jsonObjectRequest = new JsonObjectRequest(method, url,paramObject, new Response.Listener<JSONObject>() {
                @Override
                public void onResponse(JSONObject response) {
                    if(!keepProgressDialogRunning) {
                            progressDialog.dismiss();
                    }
                    if (response != null) {
                        if (response.has("authError")){
                            try {
                                Integer error = response.getInt("authError");
                                if(error == 1){
                                    showAuthErrorAlert();
                                }
                            } catch (JSONException e) {
                                e.printStackTrace();
                            }
                        } else {
                            listener.receiveResult(response, url, context, typeRequest, userInfo, progressDialog);
                        }                    } else {
                        Log.d("Volley REsponse:","Volley Response is null");
                    }
                }
            }, new Response.ErrorListener() {
                @Override
                public void onErrorResponse(VolleyError error) {
                        progressDialog.dismiss();
                    if (error != null) {
                        listener.receiveError(error, context, typeRequest,userInfo,progressDialog);
                    }
                }
            }) {
                @Override
                public Map<String, String> getHeaders() throws AuthFailureError {
                    return headers;
                }
            };
        } else if (parameters == null && headers!= null){
            jsonObjectRequest = new JsonObjectRequest(method, url, new Response.Listener<JSONObject>() {
                @Override
                public void onResponse(JSONObject response) {
                    if(!keepProgressDialogRunning) {
                            progressDialog.dismiss();
                    }
                    if (response != null) {
                        if (response.has("authError")){
                            try {
                                Integer error = response.getInt("authError");
                                if(error == 1){
                                    showAuthErrorAlert();
                                }
                            } catch (JSONException e) {
                                e.printStackTrace();
                            }
                        } else {
                            listener.receiveResult(response, url, context, typeRequest, userInfo, progressDialog);
                        }                    } else {
                        Log.d("Volley REsponse:","Volley Response is null");
                    }
                }
            }, new Response.ErrorListener() {
                @Override
                public void onErrorResponse(VolleyError error) {
                        progressDialog.dismiss();
                    if (error != null) {
                        listener.receiveError(error, context, typeRequest,userInfo,progressDialog);
                    }
                }
            }) {
                @Override
                public Map<String, String> getHeaders() throws AuthFailureError {
                    return headers;
                }
            };
        } else {
            Log.d("Network Request Class","NetworkRequest Class Error");
        }
        progressDialog = new ProgressDialog(context);
        if(jsonObjectRequest != null) {
            jsonObjectRequest.setRetryPolicy(new DefaultRetryPolicy(0,0,DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
            requestQueue.add(jsonObjectRequest);
            if (!isLoaderHidden) {

                progressDialog.setMessage(loaderMsg);
                progressDialog.setCancelable(false);
                progressDialog.show();
            }
        } else {
            Log.d("Network Request","JSON object request is null");
        }

    }

    private void showAuthErrorAlert(){
        //Clear user Info
        SharedPref.saveToPreferences(context, SharedPrefKey.fileName, SharedPrefKey.KEY_USERPROFILE, "");

        AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(context);
        alertDialogBuilder.setTitle("Session Expired");
        alertDialogBuilder.setMessage("Your session has expired.. Please login again to continue");
        alertDialogBuilder.setCancelable(false);

        alertDialogBuilder.setPositiveButton("OK", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.dismiss();

                Intent intent = new Intent(context, LoginActivity.class);
                intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_NEW_TASK);
                context.startActivity(intent);
            }
        });


        AlertDialog alertDialog = alertDialogBuilder.create();
        alertDialog.getWindow().setType(WindowManager.LayoutParams.TYPE_SYSTEM_ALERT);
        Log.d("Alert Dialog test:","test test");
        alertDialog.show();
    }

    @Override
    public String toString() {
        return "NetworkRequest{" +
                "url='" + url + '\'' +
                ", parameters=" + parameters +
                ", headers=" + headers +
                ", method=" + method +
                ", loaderMsg='" + loaderMsg + '\'' +
                ", context=" + context +
                ", isLoaderHidden=" + isLoaderHidden +
                ", typeRequest=" + typeRequest +
                ", progressDialog=" + progressDialog +
                ", volleySingleton=" + volleySingleton +
                ", requestQueue=" + requestQueue +
                ", listener=" + listener +
                '}';
    }
}
