package com.kawasolutions.Kawa.Models;

import android.support.annotation.Nullable;

import java.io.Serializable;

/**
 * Created by apple on 4/25/16.
 */
public class UserProfile implements Serializable{

    @Nullable
    private String authToken,email,gender,imageName,phone,username;
    @Nullable
    private Integer totalRequest,successCount,unsuccessCount;
    @Nullable
    private Double rating;
    private Integer customerID,userID,roleID;
    private String firstname,lastname;
    private Boolean loggedIn;

    public UserProfile(){

    }

    public Integer getCustomerID() {
        return customerID;
    }

    public void setCustomerID(Integer customerID) {
        this.customerID = customerID;
    }

    @Nullable
    public String getAuthToken() {
        return authToken;
    }

    public void setAuthToken(@Nullable String authToken) {
        this.authToken = authToken;
    }

    public Integer getRoleID() {
        return roleID;
    }

    public void setRoleID(Integer roleID) {
        this.roleID = roleID;
    }

    public Integer getUserID() {
        return userID;
    }

    public void setUserID(Integer userID) {
        this.userID = userID;
    }

    public String getFirstname() {
        return firstname;
    }

    public void setFirstname(String firstname) {
        this.firstname = firstname;
    }

    public String getLastname() {
        return lastname;
    }

    public void setLastname(String lastname) {
        this.lastname = lastname;
    }

    @Nullable
    public String getEmail() {
        return email;
    }

    public void setEmail(@Nullable String email) {
        this.email = email;
    }

    @Nullable
    public String getGender() {
        return gender;
    }

    public void setGender(@Nullable String gender) {
        this.gender = gender;
    }

    @Nullable
    public Double getRating() {
        return rating;
    }

    public void setRating(@Nullable Double rating) {
        this.rating = rating;
    }

    @Nullable
    public Integer getTotalRequest() {
        return totalRequest;
    }

    public void setTotalRequest(@Nullable Integer totalRequest) {
        this.totalRequest = totalRequest;
    }

    @Nullable
    public Integer getSuccessCount() {
        return successCount;
    }

    public void setSuccessCount(@Nullable Integer successCount) {
        this.successCount = successCount;
    }

    @Nullable
    public Integer getUnsuccessCount() {
        return unsuccessCount;
    }

    public void setUnsuccessCount(@Nullable Integer unsuccessCount) {
        this.unsuccessCount = unsuccessCount;
    }

    @Nullable
    public String getPhone() {
        return phone;
    }

    public void setPhone(@Nullable String phone) {
        this.phone = phone;
    }

    @Nullable
    public String getUsername() {
        return username;
    }

    @Nullable
    public String getImageName() {
        return imageName;
    }

    public void setImageName(@Nullable String imageName) {
        this.imageName = imageName;
    }

    public void setUsername(@Nullable String username) {
        this.username = username;
    }

    public Boolean getLoggedIn() {
        return loggedIn;
    }

    public void setLoggedIn(Boolean loggedIn) {
        this.loggedIn = loggedIn;
    }

    @Override
    public String toString() {
        return "UserProfile{" +
                "authToken='" + authToken + '\'' +
                ", email='" + email + '\'' +
                ", gender='" + gender + '\'' +
                ", imageName='" + imageName + '\'' +
                ", rating='" + rating + '\'' +
                ", totalRequest='" + totalRequest + '\'' +
                ", successCount='" + successCount + '\'' +
                ", unsuccessCount='" + unsuccessCount + '\'' +
                ", phone='" + phone + '\'' +
                ", username='" + username + '\'' +
                ", customerID='" + customerID + '\'' +
                ", userID='" + userID + '\'' +
                ", roleID='" + roleID + '\'' +
                ", firstname='" + firstname + '\'' +
                ", lastname='" + lastname + '\'' +
                ", loggedIn=" + loggedIn +
                '}';
    }
}
