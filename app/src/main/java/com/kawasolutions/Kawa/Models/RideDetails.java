package com.kawasolutions.Kawa.Models;

import android.support.annotation.Nullable;

import com.google.android.gms.maps.model.LatLng;

import java.io.Serializable;

/**
 * Created by apple on 2/17/17.
 */

public class RideDetails implements Serializable{
    @Nullable
    private String rideTime;
    private Integer rideId;
    private DriverProfile driverProfile;
    @Nullable
    private LatLng destinationLocation;
    @Nullable
    private LatLng myLocation;
    private String otpCode;
    private String trackRideUrl;
    private Integer customerId;
    private Boolean extendedRange;
    private Double extendedRangePrice;

    public Integer getRideId() {
        return rideId;
    }

    public Integer getCustomerId() {
        return customerId;
    }

    public void setCustomerId(Integer customerId) {
        this.customerId = customerId;
    }

    public void setRideId(Integer rideId) {
        this.rideId = rideId;
    }

    public DriverProfile getDriverProfile() {
        return driverProfile;
    }

    public void setDriverProfile(DriverProfile driverProfile) {
        this.driverProfile = driverProfile;
    }

    @Nullable
    public LatLng getDestinationLocation() {
        return destinationLocation;
    }

    public void setDestinationLocation(@Nullable LatLng destinationLocation) {
        this.destinationLocation = destinationLocation;
    }

    @Nullable
    public LatLng getMyLocation() {
        return myLocation;
    }

    public void setMyLocation(@Nullable LatLng myLocation) {
        this.myLocation = myLocation;
    }

    @Nullable
    public String getRideTime() {
        return rideTime;
    }

    public void setRideTime(@Nullable String rideTime) {
        this.rideTime = rideTime;
    }

    public String getOtpCode() {
        return otpCode;
    }

    public void setOtpCode(String otpCode) {
        this.otpCode = otpCode;
    }

    public String getTrackRideUrl() {
        return trackRideUrl;
    }

    public void setTrackRideUrl(String trackRideUrl) {
        this.trackRideUrl = trackRideUrl;
    }

    public Boolean getExtendedRange() {
        return extendedRange;
    }

    public void setExtendedRange(Boolean extendedRange) {
        this.extendedRange = extendedRange;
    }

    public Double getExtendedRangePrice() {
        return extendedRangePrice;
    }

    public void setExtendedRangePrice(Double extendedRangePrice) {
        this.extendedRangePrice = extendedRangePrice;
    }
}
