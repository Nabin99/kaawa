package com.kawasolutions.Kawa.Models;

import android.support.annotation.Nullable;

import com.google.android.gms.maps.model.LatLng;

import java.io.Serializable;

/**
 * Created by apple on 2/21/17.
 */

public class EndOfRide implements Serializable{

    private Integer rideId;
    private Integer customerId;
    private String driverName;
    private String startLocation;
    private String endLocation;
    private String carNumber;
    private String date;
    private String distance;
    private String chargeEstimate;
    private String meterPrice;
    @Nullable
    private String driverImage;
    private String rideMap;
    private Boolean extendedRange;
    private Double extendedRangePrice = 0.0;

    public Integer getRideId() {
        return rideId;
    }

    public void setRideId(Integer rideId) {
        this.rideId = rideId;
    }

    public String getChargeEstimate() {
        return chargeEstimate;
    }

    public void setChargeEstimate(String chargeEstimate) {
        this.chargeEstimate = chargeEstimate;
    }

    public String getDistance() {
        return distance;
    }

    public void setDistance(String distance) {
        this.distance = distance;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public String getCarNumber() {
        return carNumber;
    }

    public void setCarNumber(String carNumber) {
        this.carNumber = carNumber;
    }

    public String getEndLocation() {
        return endLocation;
    }

    public void setEndLocation(String endLocation) {
        this.endLocation = endLocation;
    }

    public String getStartLocation() {
        return startLocation;
    }

    public void setStartLocation(String startLocation) {
        this.startLocation = startLocation;
    }

    public String getDriverName() {
        return driverName;
    }

    public void setDriverName(String driverName) {
        this.driverName = driverName;
    }

    @Nullable
    public String getDriverImage() {
        return driverImage;
    }

    public void setDriverImage(@Nullable String driverImage) {
        this.driverImage = driverImage;
    }

    public String getMeterPrice() {
        return meterPrice;
    }

    public void setMeterPrice(String meterPrice) {
        this.meterPrice = meterPrice;
    }

    public Integer getCustomerId() {
        return customerId;
    }

    public void setCustomerId(Integer customerId) {
        this.customerId = customerId;
    }

    public String getRideMap() {
        return rideMap;
    }

    public void setRideMap(String rideMap) {
        this.rideMap = rideMap;
    }

    public Boolean getExtendedRange() {
        return extendedRange;
    }

    public void setExtendedRange(Boolean extendedRange) {
        this.extendedRange = extendedRange;
    }

    public Double getExtendedRangePrice() {
        return extendedRangePrice;
    }

    public void setExtendedRangePrice(Double extendedRangePrice) {
        this.extendedRangePrice = extendedRangePrice;
    }
}
