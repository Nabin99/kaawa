package com.kawasolutions.Kawa.Models;

import com.google.android.gms.maps.model.LatLng;

/**
 * Created by apple on 2/20/17.
 */

public class RideActivity {

    private LatLng driverLocation;
    private Boolean rideStarted;
    private Boolean rideSuccess;
    private Boolean rideCancelled;
    private Boolean rideActive;
    private Boolean expireStatus;

    private static RideActivity instance;

    public static RideActivity getInstance(){
        if(instance == null){
            return new RideActivity();
        }
        return instance;
    }

    public LatLng getDriverLocation() {
        return driverLocation;
    }

    public void setDriverLocation(LatLng driverLocation) {
        this.driverLocation = driverLocation;
    }

    public Boolean getRideStarted() {
        return rideStarted;
    }

    public void setRideStarted(Boolean rideStarted) {
        this.rideStarted = rideStarted;
    }

    public Boolean getRideSuccess() {
        return rideSuccess;
    }

    public void setRideSuccess(Boolean rideSuccess) {
        this.rideSuccess = rideSuccess;
    }

    public Boolean getRideCancelled() {
        return rideCancelled;
    }

    public void setRideCancelled(Boolean rideCancelled) {
        this.rideCancelled = rideCancelled;
    }

    public Boolean getRideActive() {
        return rideActive;
    }

    public void setRideActive(Boolean rideActive) {
        this.rideActive = rideActive;
    }

    public Boolean getExpireStatus() {
        return expireStatus;
    }

    public void setExpireStatus(Boolean expireStatus) {
        this.expireStatus = expireStatus;
    }

    @Override
    public String toString() {
        return "RideActivity{" +
                "driverLocation=" + driverLocation +
                ", rideStarted=" + rideStarted +
                ", rideSuccess=" + rideSuccess +
                ", rideCancelled=" + rideCancelled +
                ", rideActive=" + rideActive +
                ", instance=" + instance +
                '}';
    }


}
