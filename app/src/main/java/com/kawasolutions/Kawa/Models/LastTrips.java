package com.kawasolutions.Kawa.Models;

import android.support.annotation.Nullable;

/**
 * Created by apple on 5/12/16.
 */
public class LastTrips {

    private String driverName;
    private String carNo;
    private String date;
    private String driverId;
    private String rideId;
    private String status;
    private String cancelStatus;
    @Nullable
    private String startLocation;
    @Nullable
    private String endLocation;
    private String carType;
    @Nullable
    private String imageName;
    @Nullable
    private Double price;
    private Double extendedPrice;

    public LastTrips() {
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public String getDriverName() {
        return driverName;
    }

    public void setDriverName(String driverName) {
        this.driverName = driverName;
    }


    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getRideId() {
        return rideId;
    }

    public void setRideId(String rideId) {
        this.rideId = rideId;
    }

    public String getDriverId() {
        return driverId;
    }

    public void setDriverId(String driverId) {
        this.driverId = driverId;
    }

    public String getCancelStatus() {
        return cancelStatus;
    }

    public void setCancelStatus(String cancelStatus) {
        this.cancelStatus = cancelStatus;
    }

    public String getCarNo() {
        return carNo;
    }

    public void setCarNo(String carNo) {
        this.carNo = carNo;
    }

    public String getCarType() {
        return carType;
    }

    public void setCarType(String carType) {
        this.carType = carType;
    }

    @Nullable
    public String getEndLocation() {
        return endLocation;
    }

    public void setEndLocation(@Nullable String endLocation) {
        this.endLocation = endLocation;
    }

    @Nullable
    public String getStartLocation() {
        return startLocation;
    }

    public void setStartLocation(@Nullable String startLocation) {
        this.startLocation = startLocation;
    }

    @Nullable
    public String getImageName() {
        return imageName;
    }

    public void setImageName(@Nullable String imageName) {
        this.imageName = imageName;
    }

    @Nullable
    public Double getPrice() {
        return price;
    }

    public void setPrice(@Nullable Double price) {
        this.price = price;
    }

    public Double getExtendedPrice() {
        return extendedPrice;
    }

    public void setExtendedPrice(Double extendedPrice) {
        this.extendedPrice = extendedPrice;
    }
}
