package com.kawasolutions.Kawa.Adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.text.format.DateFormat;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.kawasolutions.Kawa.Fragments.DrawerFragments.CurrentRideListFragment;
import com.kawasolutions.Kawa.R;
import com.kawasolutions.Kawa.RideCancellation;

import java.util.ArrayList;

public class RecyclerViewAdapter extends RecyclerView.Adapter<RecyclerViewAdapter.ViewHolder>{
    private static final String TAG = "RecyclerViewAdapter";

    private ArrayList<String> mTaxiNumbers=new ArrayList<>();
    private ArrayList<String> mOTPCodes=new ArrayList<>();
    private ArrayList<String> mNames=new ArrayList<>();
    private ArrayList<String> mDates=new ArrayList<>();
    private ArrayList<String> mcarTypes=new ArrayList<>();
    private ArrayList<String> mRideIds=new ArrayList<>();
    private ArrayList<String> mRideStatus=new ArrayList<>();
    private Context mContext;

    public RecyclerViewAdapter(ArrayList<String> mTaxiNumbers, ArrayList<String> mOTPCodes,ArrayList<String> mNames,
                               ArrayList<String> mDates, ArrayList<String> mcarTypes, ArrayList<String> mRideIds,
                               ArrayList<String> mRideStatus,Context mContext) {
        this.mTaxiNumbers = mTaxiNumbers;
        this.mNames=mNames;
        this.mOTPCodes = mOTPCodes;
        this.mDates=mDates;
        this.mcarTypes=mcarTypes;
        this.mRideIds=mRideIds;
        this.mContext = mContext;
        this.mRideStatus=mRideStatus;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {

        View view= LayoutInflater.from(parent.getContext()).inflate(R.layout.content_recycler_view,parent,false);
        ViewHolder viewHolder=new ViewHolder(view);
        return viewHolder;
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, final int position) {
        Log.d(TAG, "onBindViewHolder: called");

        holder.taxiNumber.setText(mTaxiNumbers.get(position));
        holder.otpCode.setText(mOTPCodes.get(position));
        holder.date.setText(convertDate(mDates.get(position),"yyyy/MM/dd hh:mm a"));
        holder.name.setText(mNames.get(position));
        holder.type.setText(mcarTypes.get(position));
        holder.rideId.setText(mRideIds.get(position));
        holder.rideStatus.setText(mRideStatus.get(position));
        holder.otpCode.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Log.d(TAG, "onClick: clicked on: "+mOTPCodes.get(position));
            }
        });
        holder.currentRidesLayout.setOnClickListener(new View.OnClickListener(){

            @Override
            public void onClick(View v) {
                Log.d(TAG, "onClick: clicked on: "+mTaxiNumbers.get(position));
            }
        });
        holder.ivBtnCancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
//                Toast.makeText(mContext,mRideIds.get(position),Toast.LENGTH_LONG).show();
                RideCancellation rideCancellation=new RideCancellation(mContext);
                rideCancellation.cancel(Integer.parseInt(mRideIds.get(position)));
            }
        });
    }

    @Override
    public int getItemCount() {
        return mTaxiNumbers.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder{

        TextView taxiNumber;
        TextView otpCode;
        TextView date;
        TextView name;
        TextView type;
        TextView rideId;
        ImageView ivBtnCancel;
        TextView rideStatus;
        LinearLayout currentRidesLayout;
        public ViewHolder(View itemView) {
            super(itemView);
            taxiNumber=itemView.findViewById(R.id.car_no);
            otpCode=itemView.findViewById(R.id.OTPCode);
            date=itemView.findViewById(R.id.date);
            name=itemView.findViewById(R.id.driver_name);
            type=itemView.findViewById(R.id.car_type);
            rideId=itemView.findViewById(R.id.rideId);
            ivBtnCancel=itemView.findViewById(R.id.btnCancel);
            rideStatus=itemView.findViewById(R.id.rideStatus);
            currentRidesLayout=itemView.findViewById(R.id.layoutContentCurrentRides);
        }
    }

    public static String convertDate(String dateInMilliseconds,String dateFormat) {
        return DateFormat.format(dateFormat, Long.parseLong(dateInMilliseconds)).toString();
    }
}
