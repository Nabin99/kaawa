package com.kawasolutions.Kawa.Adapter;

import android.content.res.ColorStateList;
import android.content.res.Resources;
import android.graphics.Bitmap;
import android.graphics.Color;
import android.graphics.drawable.BitmapDrawable;
import android.support.v7.widget.RecyclerView;
import android.text.Html;
import android.text.Spanned;
import android.text.SpannedString;
import android.text.format.DateFormat;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.android.volley.RequestQueue;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.ImageLoader;
import com.kawasolutions.Kawa.Constants.Key;
import com.kawasolutions.Kawa.Constants.URLS;
import com.kawasolutions.Kawa.Models.LastTrips;
import com.kawasolutions.Kawa.R;
import com.kawasolutions.Kawa.Singleton.VolleySingleton;

import org.w3c.dom.Text;

import java.util.ArrayList;

import de.hdodenhof.circleimageview.CircleImageView;

/**
 * Created by apple on 5/12/16.
 */
public class LastRideAdapter extends RecyclerView.Adapter<LastRideAdapter.MyViewHolder>{

    private ArrayList<LastTrips> lastTripsList;
    VolleySingleton volleySingleton = VolleySingleton.getInstance();
    ImageLoader imageLoader = volleySingleton.getImageLoader();

    public class MyViewHolder extends RecyclerView.ViewHolder{

        public TextView driverName, carNo, date,status,cartype,startLocation,endLocation,image,priceBreakdown;
        public CircleImageView driverImageView;

        public MyViewHolder(View itemView) {
            super(itemView);
            driverName = (TextView) itemView.findViewById(R.id.driver_name);
            carNo = (TextView) itemView.findViewById(R.id.car_no);
            date = (TextView) itemView.findViewById(R.id.date);
            status = (TextView) itemView.findViewById(R.id.ride_status);
            cartype = (TextView) itemView.findViewById(R.id.car_type);
            startLocation = (TextView) itemView.findViewById(R.id.start_location);
            endLocation = (TextView) itemView.findViewById(R.id.end_location);
            driverImageView = (CircleImageView) itemView.findViewById(R.id.driver_image);
            priceBreakdown = (TextView) itemView.findViewById(R.id.price_breakdown);
        }
    }

    public LastRideAdapter(ArrayList<LastTrips> lastTripsList) {
        this.lastTripsList = lastTripsList;
    }

    @Override
    public LastRideAdapter.MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
      View itemView = LayoutInflater.from(parent.getContext()).inflate(R.layout.last_trips_row,parent,false);
        return new MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(final LastRideAdapter.MyViewHolder holder, int position) {

        LastTrips lastTrips = lastTripsList.get(position);
        holder.driverName.setText(lastTrips.getDriverName());
        holder.carNo.setText(lastTrips.getCarNo());
        holder.date.setText(convertDate(lastTrips.getDate(),"yyyy/MM/dd hh:mm a"));
        String content = "<font color = '#c5c5c5'>&#8226;</font>" + lastTrips.getCarType();
        holder.cartype.setText(Html.fromHtml(content));

        if(lastTrips.getStartLocation() != null){
            String startLoc = "<font color = '#388E3C'>&#8226;</font>" + lastTrips.getStartLocation();
            holder.startLocation.setText(Html.fromHtml(startLoc));
        } else {
            holder.startLocation.setText("");
        }
        if(lastTrips.getEndLocation() != null){
            String endLoc = "<font color = '#b12512'>&#8226;</font>" + lastTrips.getEndLocation();
            holder.endLocation.setText(Html.fromHtml(endLoc));
        } else {
            holder.endLocation.setText("");
        }

        if(lastTrips.getCancelStatus() == "true"){
            String text = "<font color = '#BF360C'> Cancelled </font>";
            holder.status.setText(Html.fromHtml(text));
            holder.priceBreakdown.setText(null);
        }
        else if(lastTrips.getStatus() == "true"){
            if(lastTrips.getPrice() != null){
                String price = "Rs " + (lastTrips.getPrice() + lastTrips.getExtendedPrice() + Key.SERVICE_CHARGE);
                holder.status.setText(price);
                holder.priceBreakdown.setText(lastTrips.getPrice() + " + " + lastTrips.getExtendedPrice() + " + " + Key.SERVICE_CHARGE);
            } else {
                holder.status.setText("Completed");
                holder.priceBreakdown.setText(null);
            }
        } else {
            holder.status.setText("Not Completed");
            holder.priceBreakdown.setText(null);
        }

        if(lastTrips.getImageName() != null) {
            String imageURL = URLS.profileImageURL + lastTrips.getImageName();
            imageLoader.get(imageURL, new ImageLoader.ImageListener() {
                @Override
                public void onResponse(ImageLoader.ImageContainer response, boolean isImmediate) {
                    holder.driverImageView.setImageBitmap(response.getBitmap());
                }

                @Override
                public void onErrorResponse(VolleyError error) {
                    holder.driverImageView.setImageResource(R.drawable.profile_image_boy);
                }
            });
        }

    }

    @Override
    public int getItemCount() {
        return lastTripsList.size();
    }

    public static String convertDate(String dateInMilliseconds,String dateFormat) {
        return DateFormat.format(dateFormat, Long.parseLong(dateInMilliseconds)).toString();
    }

}
