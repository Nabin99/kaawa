package com.kawasolutions.Kawa;

import android.app.Application;
import android.content.Context;
import android.support.multidex.MultiDexApplication;

import org.acra.ACRA;
import org.acra.ReportField;
import org.acra.ReportingInteractionMode;
import org.acra.annotation.ReportsCrashes;
import org.acra.sender.HttpSender;

@ReportsCrashes(
        formUri = "https://kawarides.cloudant.com/acra-kawa/_design/acra-storage/_update/report",
        reportType = HttpSender.Type.JSON,
        httpMethod = HttpSender.Method.POST,
        formUriBasicAuthLogin = "issevelestonfordstoweral",
        formUriBasicAuthPassword = "9b9b65c6e8a14d24cc55af9cf04c055051e778fc",
        customReportContent = {
                ReportField.APP_VERSION_CODE,
                ReportField.APP_VERSION_NAME,
                ReportField.ANDROID_VERSION,
                ReportField.PACKAGE_NAME,
                ReportField.REPORT_ID,
                ReportField.BUILD,
                ReportField.STACK_TRACE
        },
        mode = ReportingInteractionMode.TOAST,
        resToastText = R.string.toast_crash
)
public class KaawaApplication extends MultiDexApplication{

    private static KaawaApplication sInstance;

    @Override
    public void onCreate() {
        super.onCreate();
        ACRA.init(this);
        sInstance = this;
    }

    public static KaawaApplication getInstance(){
        return sInstance;
    }

    public static Context getAppContext(){
        return sInstance.getApplicationContext();
    }
}
