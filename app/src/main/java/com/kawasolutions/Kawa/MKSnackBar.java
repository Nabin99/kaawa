package com.kawasolutions.Kawa;

import android.content.Context;
import android.content.Intent;
import android.provider.Settings;
import android.support.annotation.Nullable;
import android.support.design.widget.CoordinatorLayout;
import android.support.design.widget.Snackbar;
import android.view.Gravity;
import android.view.View;

/**
 * Created by apple on 2/8/17.
 */

public class MKSnackBar {

    private static MKSnackBar instance = null;
    public static Snackbar snackBar;

    private MKSnackBar(){

    }

    public static MKSnackBar getInstance(){
        if(instance == null){
            instance =  new MKSnackBar();
        }
        return instance;
    }

    public static MKSnackBar getSnackBar(){
        return new MKSnackBar();
    }

    public void showSnackBarWithAnchor(final Context context, CoordinatorLayout layout, String msg, int snackLength, int anchorID, @Nullable String action){
        snackBar = Snackbar.make(layout,msg,snackLength);
        if(action == "Settings") {
            snackBar.setAction("Settings", new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Intent intent = new Intent(Settings.ACTION_LOCATION_SOURCE_SETTINGS);
                    context.startActivity(intent);
                }
            });
        }
            View view = snackBar.getView();
            CoordinatorLayout.LayoutParams params = (CoordinatorLayout.LayoutParams) view.getLayoutParams();
            params.gravity = Gravity.CENTER | Gravity.TOP;
            params.setAnchorId(anchorID);
            view.setLayoutParams(params);
        if(!snackBar.isShown()) {
            snackBar.show();
        }
    }

    public void showSnackBar(Context context,CoordinatorLayout layout,String msg,int snackLength){

    }


}
