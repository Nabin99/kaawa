/**
 * Copyright 2015 Google Inc. All Rights Reserved.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.kawasolutions.Kawa.GCM;

import android.app.KeyguardManager;
import android.app.Notification;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.media.RingtoneManager;
import android.net.Uri;
import android.os.Bundle;
import android.os.Handler;
import android.os.Looper;
import android.os.PowerManager;
import android.support.annotation.Nullable;
import android.support.v4.app.NotificationCompat;
import android.support.v4.content.LocalBroadcastManager;
import android.util.Base64;
import android.util.Log;
import android.widget.RemoteViews;

import com.android.volley.Request;
import com.google.android.gms.gcm.GcmListenerService;
import com.google.android.gms.maps.model.LatLng;
import com.google.gson.Gson;
import com.google.gson.JsonObject;
import com.kawasolutions.Kawa.Activities.BulkNotificationActivity;
import com.kawasolutions.Kawa.Activities.Home;
import com.kawasolutions.Kawa.Activities.LoginActivity;
import com.kawasolutions.Kawa.Activities.MainActivity;
import com.kawasolutions.Kawa.Activities.RateDriverActivity;
import com.kawasolutions.Kawa.Activities.RideInfo;
import com.kawasolutions.Kawa.Constants.NotificationKeys;
import com.kawasolutions.Kawa.Constants.SharedPrefKey;
import com.kawasolutions.Kawa.Constants.URLS;
import com.kawasolutions.Kawa.DriversMark;
import com.kawasolutions.Kawa.Enum.TYPEREQUEST;
import com.kawasolutions.Kawa.Fragments.DrawerFragments.MainFragment;
import com.kawasolutions.Kawa.Models.AcceptedDriverInfo;
import com.kawasolutions.Kawa.Models.DriverProfile;
import com.kawasolutions.Kawa.Models.EndOfRide;
import com.kawasolutions.Kawa.Models.RideDetails;
import com.kawasolutions.Kawa.Models.UserProfile;
import com.kawasolutions.Kawa.NetworkRequest;
import com.kawasolutions.Kawa.NotificationFunction;
import com.kawasolutions.Kawa.R;
import com.kawasolutions.Kawa.Utils.SharedPref;

import org.json.JSONObject;

import java.io.IOException;
import java.net.URL;
import java.sql.Driver;
import java.util.HashMap;

public class MyGcmListenerService extends GcmListenerService {

    private String userId;
    private String driverId;
    private String driverName;
    private String driverFirstName;
    private String driverLastName;
    private String driverPhone;
    private String driverLatitude;
    private String driverLongitude;
    private String carModel;
    private String customerId;
    private String carNumber;
    @Nullable
    private String driverRating;
    @Nullable
    private String email;
    private String rideId;
    private String message;
    private String startLatitude;
    private String startLongitude;
    private String endLatitude;
    private String endLongitude;
    private String date;
    @Nullable
    private String destinationLatitude;
    @Nullable
    private String destinationLongitude;
    private AcceptedDriverInfo acceptedDriverInfo;
    private String distance;
    private String chargeEstimate;
    @Nullable
    private String driverImage;
    private String rideTime;
    private String greetingMessage;

    private String meterPrice;
    private String startLocation;
    private String endLocation;


    private static final String TAG = "MyGcmListenerService";

    /**
     * Called when message is received.
     *
     * @param from SenderID of the sender.
     * @param data Data bundle containing message data as key/value pairs.
     *             For Set of keys use data.keySet().
     */
    // [START receive_message]
    @Override
    public void onMessageReceived(String from, Bundle data) {

        Log.d("Message:","" + data);
        String type = data.getString("type");

//        if(type.equals("request")) {
//            RideDetails rideDetails = getRideDetails(data);
//            saveDetailsToSharedPref(rideDetails);
//            if(MainFragment.active){
//                if(NetworkRequest.progressDialog != null){
//                    if(NetworkRequest.progressDialog.isShowing()){
//                        NetworkRequest.progressDialog.dismiss();
//                    }
//                }
//                Intent intent = new Intent("request-success");
//                Bundle bundle = new Bundle();
//                Gson gson = new Gson();
//                bundle.putString("RideDetailsObject", gson.toJson(rideDetails));
//                intent.putExtras(bundle);
//                LocalBroadcastManager.getInstance(this).sendBroadcast(intent);
//            } else {
//                //sendNotification(rideDetails);
//            }

//        } else if (type.equals("rate_request")){
//            wakeUpDevice();
//            EndOfRide endOfRide = getRateRequestData(data);
//            //Show Rate Activity only if user is logged in and it is directed to the current user
//            UserProfile userProfile = SharedPref.getUserObject(getApplicationContext(), SharedPrefKey.fileName);
//         //   Integer rateCustomerId = SharedPref.readIntegerFromPreference(getApplicationContext(),SharedPrefKey.fileName,SharedPrefKey.KEY_RATE_CUSTOMER_ID);
//            if(userProfile != null) {
//                if (userProfile.getCustomerID().equals(endOfRide.getCustomerId())){
//                    closeUpdateTimerInRideInfo();
//                    saveRateDetailsToSharedPref(endOfRide);
//                    String message = data.getString("message");
//                    showRateNotification(message);
//                    if(RideInfo.active){
//                        Intent intent = new Intent(this, RateDriverActivity.class);
//                        Bundle bundle = new Bundle();
//                        Gson gson = new Gson();
//                        bundle.putString("endOfRideDetails", gson.toJson(endOfRide));
//                        intent.putExtras(bundle);
//                        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_NEW_TASK);
//                        startActivity(intent);
//                    }
//                } else {
//                    Log.d("Rate Request Customer:","Not intended for this user");
//                }
//            } else {
//                Log.d("Rate Request Customer:","No user logged in");
//            }
 //       } else
            if (type.equals("bulkNotification")) {
            message = data.getString("message");
            sendBulkNotification(message);
        } else if (type.equals("greeting")){
            Log.d("GCM Messages:","" + "Greeting message");
            greetingMessage = data.getString("message");
            String images = data.getString("image");
            showGreetingNotification(greetingMessage,images);
        } else if (type.equals("appUpdate")){
            SharedPref.saveToPreferences(this,SharedPrefKey.fileName,SharedPrefKey.KEY_NEW_APP_UPDATE,true);
        }

    }

    public void closeUpdateTimerInRideInfo(){
        if(RideInfo.updateTimer != null){
            RideInfo.updateTimer.cancel();
            RideInfo.updateTimer.purge();
        }
    }

    public void showRateNotification(String message){
        Intent i = new Intent(this, MainActivity.class);
        i.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_NEW_TASK);
        PendingIntent pendingIntent = PendingIntent.getActivity(this, 0 /* Request code */, i,
                PendingIntent.FLAG_ONE_SHOT);
        Uri defaultSoundUri = RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION);
        NotificationCompat.Builder notificationBuilder = new NotificationCompat.Builder(this)
                .setSmallIcon(R.drawable.taxi_marker)
                .setLargeIcon(getLargeTaxiIcon())
                .setStyle(new NotificationCompat.BigTextStyle().bigText(message))
                .setContentTitle("Kawa")
                .setContentText(message)
                .setAutoCancel(true)
                .setTicker(message)
                .setSound(defaultSoundUri)
                .setContentIntent(pendingIntent);

        NotificationManager notificationManager =
                (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);

        notificationManager.notify(NotificationKeys.RATE_REQUEST_TAG,0,notificationBuilder.build());
    }

    private Bitmap getLargeTaxiIcon(){
        return BitmapFactory.decodeResource(getResources(),R.drawable.taxi_marker);
    }

    public void showGreetingNotification(String message,String image){
        Intent i = new Intent(this, MainActivity.class);
        i.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_NEW_TASK);
        PendingIntent pendingIntent = PendingIntent.getActivity(this, 0 /* Request code */, i,
                PendingIntent.FLAG_ONE_SHOT);
        Uri defaultSoundUri = RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION);
        NotificationCompat.Builder notificationBuilder = new NotificationCompat.Builder(this);
        if(image.equals("")) {
            notificationBuilder
                    .setSmallIcon(R.drawable.taxi_marker)
                    .setLargeIcon(getLargeTaxiIcon())
                    .setContentTitle("Kawa")
                    .setStyle(new NotificationCompat.BigTextStyle().bigText(message))
                    .setContentText(message)
                    .setAutoCancel(true)
                    .setTicker(message)
                    .setSound(defaultSoundUri)
                    .setContentIntent(pendingIntent);
        } else {
            Bitmap bmp = null;
            try {
                URL url = new URL(image);
                bmp = BitmapFactory.decodeStream(url.openConnection().getInputStream());
            } catch (IOException e) {
                e.printStackTrace();
            }
//            byte[] decodedString = Base64.decode(image, Base64.DEFAULT);
//            Bitmap decodedByte = BitmapFactory.decodeByteArray(decodedString, 0, decodedString.length);
                NotificationCompat.BigPictureStyle notiStyle = new NotificationCompat.BigPictureStyle();
                notiStyle.bigPicture(bmp);
                notiStyle.setSummaryText(message);
                notificationBuilder.setSmallIcon(R.drawable.taxi_marker)
                        .setLargeIcon(getLargeTaxiIcon())
                        .setContentTitle("Kawa")
                        .setAutoCancel(true)
                        .setContentText(message)
                        .setSound(defaultSoundUri)
                        .setTicker(message)
                        .setPriority(Notification.PRIORITY_MAX)
                        .setContentIntent(pendingIntent)
                        .setStyle(notiStyle);

        }

        NotificationManager notificationManager =
                (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);

        notificationManager.notify(NotificationKeys.GREETING_TAG,NotificationKeys.GREETING_ID,notificationBuilder.build());
    }

    public void saveRateDetailsToSharedPref(EndOfRide endOfRide){
        Gson gson = new Gson();
        String rateDetailsString = gson.toJson(endOfRide);
        SharedPref.saveToPreferences(getApplicationContext(), SharedPrefKey.fileName,SharedPrefKey.KEY_RATE_REQUEST,true);
        SharedPref.saveToPreferences(getApplicationContext(),SharedPrefKey.fileName,SharedPrefKey.KEY_RATE_DETAILS,rateDetailsString);
        SharedPref.saveToPreferences(getApplicationContext(), SharedPrefKey.fileName,SharedPrefKey.KEY_RATE_CUSTOMER_ID,endOfRide.getCustomerId());

    }

    private void sendBulkNotification(String message) {
        Intent intent = new Intent(this, BulkNotificationActivity.class);
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        intent.putExtra("message", message);

        PendingIntent pendingIntent = PendingIntent.getActivity(this, 0 /* Request code */, intent,
                PendingIntent.FLAG_ONE_SHOT);

        Uri defaultSoundUri = RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION);
        NotificationCompat.Builder notificationBuilder = new NotificationCompat.Builder(this)
                .setSmallIcon(R.drawable.taxi_marker)
                .setLargeIcon(getLargeTaxiIcon())
                .setContentTitle("Kawa")
                .setStyle(new NotificationCompat.BigTextStyle().bigText(message))
                .setContentText(message)
                .setAutoCancel(true)
                .setSound(defaultSoundUri)
                .setPriority(Notification.PRIORITY_MAX)
                .setContentIntent(pendingIntent);

        NotificationManager notificationManager =
                (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);

        notificationManager.notify(0, notificationBuilder.build());
    }

    public void saveDetailsToSharedPref(RideDetails rideDetails){
        Gson gson = new Gson();
        String rideDetailsString = gson.toJson(rideDetails);
        SharedPref.saveToPreferences(getApplicationContext(), SharedPrefKey.fileName,SharedPrefKey.KEY_ON_RIDE,true);
        SharedPref.saveToPreferences(getApplicationContext(),SharedPrefKey.fileName,SharedPrefKey.KEY_RIDE_DETAILS,rideDetailsString);

        UserProfile userProfile = SharedPref.getUserObject(getApplicationContext(), SharedPrefKey.fileName);
        if(userProfile != null) {
            SharedPref.saveToPreferences(getApplicationContext(), SharedPrefKey.fileName,SharedPrefKey.KEY_RIDE_CUSTOMER_ID,userProfile.getCustomerID());
        }
    }

    public RemoteViews getComplexNotificationViews(Bitmap bmp){
        RemoteViews notificationView = new RemoteViews(getApplicationContext().getPackageName(),R.layout.custom_notification);
        notificationView.setImageViewBitmap(R.id.image,bmp);
        return notificationView;
    }

//    private void sendNotification(RideDetails rideDetails) {
//
//        Intent intent = new Intent(this, MainActivity.class);
//        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_NEW_TASK);
//
//        PendingIntent pendingIntent = PendingIntent.getActivity(this, 0 /* Request code */, intent,
//                PendingIntent.FLAG_ONE_SHOT);
//
//        Uri defaultSoundUri = RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION);
//        NotificationCompat.Builder notificationBuilder = new NotificationCompat.Builder(this)
//                .setSmallIcon(R.drawable.taxi_marker)
//                .setContentTitle("Kawa")
//                .setStyle(new NotificationCompat.BigTextStyle().bigText(message))
//                .setContentText(rideDetails.getMessage())
//                .setAutoCancel(true)
//                .setSound(defaultSoundUri)
//                .setPriority(Notification.PRIORITY_MAX)
//                .setContentIntent(pendingIntent);
//
//        NotificationManager notificationManager =
//                (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);
//
//        notificationManager.notify(NotificationKeys.REQUEST_TAG,rideDetails.getRideId(),notificationBuilder.build());
//        setTimerForNotificationCancellation(NotificationKeys.REQUEST_TAG, rideDetails.getRideId());
//    }

    public void setTimerForNotificationCancellation(final String tag, final Integer id){
        Handler handler = new Handler(Looper.getMainLooper());
        handler.postDelayed(new Runnable() {
            @Override
            public void run() {
                NotificationFunction.cancelNotification(getApplicationContext(),tag,id);
            }
        },60000);
    }

//    public RideDetails getRideDetails(Bundle data){
//         userId = data.getString("userId");
//         message = data.getString("message");
//         rideId = data.getString("rideId");
//         driverId = data.getString("driverId");
//         driverFirstName = data.getString("driverFirstName");
//         driverLastName = data.getString("driverLastName");
//         driverPhone = data.getString("driverContactNo");
//         email = data.getString("email");
//         driverLatitude = data.getString("latitude");
//         driverLongitude = data.getString("longitude");
//         carModel = data.getString("vehicleType");
//         carNumber = data.getString("vehicleNo");
//         driverRating = data.getString("driverAverageRating");
//         rideTime = data.getString("rideTime");
//         driverImage = data.getString("image");
//         destinationLatitude = data.getString("destinationLatitude");
//         destinationLongitude = data.getString("destinationLongitude");
//         String otpCode = data.getString("otpCode");
//            String trackingURL = data.getString("track_ride");
//
//        Log.d("Destination Request:","" + destinationLatitude + " " + destinationLongitude);
//
//        DriverProfile driverProfile = new DriverProfile();
//        driverProfile.setUserId(Integer.valueOf(userId));
//        driverProfile.setDriverId(Integer.valueOf(driverId));
//        driverProfile.setFirstname(driverFirstName);
//        driverProfile.setLastname(driverLastName);
//        driverProfile.setPhone(driverPhone);
//        driverProfile.setEmail(email);
//        driverProfile.setLocation(new LatLng(Double.valueOf(driverLatitude),Double.valueOf(driverLongitude)));
//        driverProfile.setVehicleType(carModel);
//        driverProfile.setVehicleNo(carNumber);
//        driverProfile.setRating(driverRating);
//        driverProfile.setImageName(driverImage);
//
//        RideDetails rideDetails = new RideDetails();
//        rideDetails.setRideId(Integer.valueOf(rideId));
//        rideDetails.setDriverProfile(driverProfile);
//        rideDetails.setTrackRideUrl(trackingURL);
//
//        LatLng destinationLocation = null;
//        if(destinationLatitude != null || destinationLongitude != null){
//            destinationLocation = new LatLng(Double.valueOf(destinationLatitude),Double.valueOf(destinationLongitude));
//        }
//        Log.d("DEstination Request :","" + destinationLocation);
//        rideDetails.setDestinationLocation(destinationLocation);
//        rideDetails.setMessage(message);
//        rideDetails.setRideTime(rideTime);
//        rideDetails.setOtpCode(otpCode);
//
//        return rideDetails;
//    }


    public EndOfRide getRateRequestData(Bundle data){
        rideId = data.getString("rideId");
        driverName = data.getString("driverName");
        startLatitude = data.getString("startLatitude");
        startLongitude = data.getString("startLongitude");
        endLatitude = data.getString("endLatitude");
        endLongitude = data.getString("endLongitude");
        carNumber = data.getString("vehicleNo");
        date = data.getString("rideDate");
        distance = data.getString("distance");
        chargeEstimate = data.getString("chargeEstimate");
        driverImage = data.getString("image");
        meterPrice = data.getString("meterPrice");
        customerId = data.getString("customerId");
        startLocation = data.getString("startLocation");
        endLocation = data.getString("endLocation");


        EndOfRide endOfRide = new EndOfRide();
        endOfRide.setRideId(Integer.valueOf(rideId));
        endOfRide.setDriverName(driverName);
        endOfRide.setStartLocation(startLocation);
        endOfRide.setEndLocation(endLocation);
        endOfRide.setCarNumber(carNumber);
        endOfRide.setDate(date);
        endOfRide.setDistance(distance);
        endOfRide.setChargeEstimate(chargeEstimate);
        endOfRide.setDriverImage(driverImage);
        endOfRide.setMeterPrice(meterPrice);
        endOfRide.setCustomerId(Integer.valueOf(customerId));

        return endOfRide;
    }

    public void wakeUpDevice() {
        PowerManager pm = (PowerManager) getApplicationContext().getSystemService(Context.POWER_SERVICE);
        if (!pm.isScreenOn()) {
            PowerManager.WakeLock wakeLock = pm.newWakeLock((PowerManager.SCREEN_BRIGHT_WAKE_LOCK | PowerManager.FULL_WAKE_LOCK | PowerManager.ACQUIRE_CAUSES_WAKEUP), "TAG");
            wakeLock.acquire();

            KeyguardManager keyguardManager = (KeyguardManager) getApplicationContext().getSystemService(Context.KEYGUARD_SERVICE);
            KeyguardManager.KeyguardLock keyguardLock = keyguardManager.newKeyguardLock("TAG");
            keyguardLock.disableKeyguard();
        }

    }
}
