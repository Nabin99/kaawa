package com.kawasolutions.Kawa;

import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.app.SearchManager;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.CountDownTimer;
import android.support.design.widget.CoordinatorLayout;
import android.support.design.widget.Snackbar;
import android.support.v4.content.LocalBroadcastManager;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.Button;
import android.widget.FrameLayout;
import android.widget.RelativeLayout;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.Volley;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MarkerOptions;
import com.kawasolutions.Kawa.Activities.MainActivity;
import com.kawasolutions.Kawa.Constants.Key;
import com.kawasolutions.Kawa.Constants.SharedPrefKey;
import com.kawasolutions.Kawa.Constants.URLS;
import com.kawasolutions.Kawa.Enum.TYPEREQUEST;
import com.kawasolutions.Kawa.Fragments.DrawerFragments.MainFragment;
import com.kawasolutions.Kawa.Models.DriverProfile;
import com.kawasolutions.Kawa.Singleton.VolleySingleton;
import com.kawasolutions.Kawa.Utils.SharedPref;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.lang.reflect.Array;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

/**
 * Created by apple on 4/26/16.
 */
public class DriversMark implements NetworkRequestTest.NetworkListenerTest{

    private Context context;
    private CoordinatorLayout layout;
    private GoogleMap map;
    private LatLng location;
    private Button rideNowBtn;
    private Integer[] driverUserIdList;
    private VolleySingleton volleySingleton;
    private RequestQueue requestQueue;
    private Snackbar snackbar;
    private Integer[] listDrivers;
    public static ProgressDialog progressDialog;
    private String destinationLatitude;
    private String destinationLongitude;
    public static CountDownTimer timer;
    public static boolean isTimerRunning = false;


    private ArrayList<DriverProfile> driverProfiles;

    public DriversMark() {

    }

    public DriversMark(Context context,CoordinatorLayout layout,GoogleMap map,LatLng location,Button rideNowBtn,Snackbar snackbar){
        this.context = context;
        this.layout = layout;
        this.map = map;
        this.snackbar = snackbar;
        this.location = location;
        this.rideNowBtn = rideNowBtn;
    }

    public void getDrivers(){

        Double latitude = location.latitude;
        Double longitude = location.longitude;
        HashMap<String,Object> params = new HashMap<>();
        params.put("latitude", latitude);
        params.put("longitude", longitude);

        NetworkRequestTest networkRequestTest = new NetworkRequestTest(context,URLS.nearbyDrivers, Request.Method.POST,params,null,TYPEREQUEST.NEARBYDRIVERS,null,null);
        networkRequestTest.setListener(this);
        networkRequestTest.getResult();

        JsonObjectRequest jsonObjectRequest = new JsonObjectRequest(Request.Method.POST, URLS.nearbyDrivers,
                new JSONObject(params), new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject response) {
                Log.d("REsponse:","" + response);
                try {
                    if (response != null) {
                        JSONArray allDrivers = response.getJSONArray("driversInArea");
                        if(allDrivers.length() > 0){
                            listDrivers = parseDriverData(allDrivers);
                            rideNowBtn.setEnabled(true);
                            rideNowBtn.setTextColor(context.getResources().getColor(R.color.colorPrimaryLight));
                            snackbar.dismiss();
                         } else {
                            rideNowBtn.setEnabled(false);
                            rideNowBtn.setTextColor(context.getResources().getColor(R.color.colorDivider));
                            snackbar = Snackbar.make(layout,context.getResources().getString(R.string.no_driver_found),Snackbar.LENGTH_LONG);
                            View view = snackbar.getView();
                            CoordinatorLayout.LayoutParams params = (CoordinatorLayout.LayoutParams) view.getLayoutParams();
                            params.gravity = Gravity.TOP;
                            params.setAnchorId(R.id.ride_now);
                            view.setLayoutParams(params);
                            snackbar.show();
                        }

                    } else {

                    }
                }catch(Exception e){

                }

            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                snackbar = Snackbar.make(layout,context.getResources().getString(R.string.server_error),Snackbar.LENGTH_LONG);
                View view = snackbar.getView();
                CoordinatorLayout.LayoutParams params = (CoordinatorLayout.LayoutParams) view.getLayoutParams();
                params.gravity = Gravity.TOP;
                params.setAnchorId(R.id.ride_now);
                view.setLayoutParams(params);
                snackbar.show();
            }
        }) {
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                String authToken = SharedPref.readFromPreference(context,SharedPrefKey.fileName,"pref_authToken","");
                HashMap<String,String> mHeaders = new HashMap<>();
                mHeaders.put("authToken",authToken);
                return mHeaders;
            }
        };
        //jsonObjectRequest.setRetryPolicy(new DefaultRetryPolicy(4000,0,0));
        requestQueue.add(jsonObjectRequest);

    }

    public Integer[] parseDriverData(JSONArray allDrivers) throws JSONException {


        driverUserIdList = new Integer[allDrivers.length()];
        driverProfiles = new ArrayList<DriverProfile>();

        for(int i = 0 ; i < allDrivers.length() ; i++) {

            JSONObject driver = allDrivers.getJSONObject(i);
            Log.d("driverLength:","" + driver);
            JSONObject driverDetails = driver.getJSONObject("userId");
            JSONObject vehicleDetails = driver.getJSONObject("vehicleId");

            DriverProfile profile = new DriverProfile();
            profile.setFirstname(driver.getString("firstName"));
            profile.setLastname(driver.getString("lastName"));
            //profile.setEmail(driver.getString("email"));
            //profile.setLatitude(driver.getDouble("latitude"));
            //profile.setLongitude(driver.getDouble("longitude"));
            //profile.setUserId(driverDetails.getString("userId"));
            profile.setPhone(driverDetails.getString("contactNo"));
            profile.setVehicleType(vehicleDetails.getString("vehicleType"));
            profile.setVehicleNo(vehicleDetails.getString("vehicleNo"));

            //driverUserIdList[i] = Integer.parseInt(profile.getUserId());


            driverProfiles.add(profile);

            map.addMarker(new MarkerOptions() .title(profile.getFirstname() + " " + profile.getLastname()) .position(profile.getLocation()) .snippet(profile.getVehicleNo())  .icon(BitmapDescriptorFactory.fromResource(R.drawable.taxi_marker)));
        }

        return driverUserIdList;
    }

    public Integer[] getDriverUserId(){
        Log.d("All Drivers:","" + driverUserIdList);
        return driverUserIdList;
    }


    public void sendRequestToGCM(LatLng destinationLocation){

        String customerId = SharedPref.readFromPreference(context, SharedPrefKey.fileName,"pref_customerID","");


        if(destinationLocation != null) {
            destinationLatitude = String.valueOf(destinationLocation.latitude);
            destinationLongitude = String.valueOf(destinationLocation.longitude);
        }
        if (listDrivers != null && customerId != null) {
            HashMap<String, Object> params = new HashMap<>();
            params.put("driverList", listDrivers);
            //params.put("latitude",latitude);
            //params.put("longitude",longitude);
            params.put("destinationLatitude",destinationLatitude);
            params.put("destinationLongitude", destinationLongitude);
            params.put("customerId", customerId);

            JsonObjectRequest jsonObjectRequest = new JsonObjectRequest(Request.Method.POST, URLS.sendNotification,new JSONObject(params), new Response.Listener<JSONObject>() {
                @Override
                public void onResponse(JSONObject response) {

                    Log.d("Response:", "" + response);
                    //progressDialog.dismiss();
                    startTimer();

                }
            }, new Response.ErrorListener() {
                @Override
                public void onErrorResponse(VolleyError error) {
                        if(progressDialog.isShowing()){
                           // progressDialog.dismiss();
                        }
                }
            }) {
                @Override
                public Map<String, String> getHeaders() throws AuthFailureError {
                    String authToken = SharedPref.readFromPreference(context, SharedPrefKey.fileName, "pref_authToken", "");
                    HashMap<String, String> mHeaders = new HashMap<>();
                    mHeaders.put("authToken", authToken);
                    return mHeaders;
                }
            };

            Volley.newRequestQueue(context).add(jsonObjectRequest);
            progressDialog = new ProgressDialog(context);
            progressDialog.setMessage("Requesting Drivers....");
            progressDialog.setCancelable(false);
            progressDialog.show();
            progressDialog.setOnCancelListener(new DialogInterface.OnCancelListener() {
                @Override
                public void onCancel(DialogInterface dialog) {
                    dialog.dismiss();
                    if(isTimerRunning){
                        timer.cancel();
                    }
                }
            });
        } else {
            Toast.makeText(context, "No Drivers Available", Toast.LENGTH_SHORT).show();
        }

    }

    public void startTimer(){
        timer = new CountDownTimer(70000,1000){

            @Override
            public void onTick(long millisUntilFinished) {
                isTimerRunning = true;
            }

            @Override
            public void onFinish() {
                progressDialog.dismiss();
                isTimerRunning = false;
                showRequestFailedAlert();
            }
        };
        timer.start();
    }

    public void showRequestFailedAlert() {
        timer.cancel();
        AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(context);
        alertDialogBuilder.setTitle("No Driver Available");
        alertDialogBuilder.setMessage("All nearby drivers are busy but one should free up soon. Check back in a minute.");
        alertDialogBuilder.setCancelable(false);

        alertDialogBuilder.setPositiveButton("OK", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.dismiss();
                Intent intent = new Intent("map-reload");
                LocalBroadcastManager.getInstance(context).sendBroadcast(intent);
            }
        });


        AlertDialog alertDialog = alertDialogBuilder.create();
        alertDialog.show();
    }

    public int driversCount(){
        return listDrivers.length;
    }


    @Override
    public void receiveResult(JSONObject result, String url, Context context, TYPEREQUEST typeRequest,String userInfo,ProgressDialog progressDialog) {
    if(progressDialog != null){
        progressDialog.dismiss();
    }
    }

    @Override
    public void receiveError(VolleyError error, Context context, TYPEREQUEST typeRequest,String userInfo,ProgressDialog progressDialog) {
    if(progressDialog != null){
        progressDialog.dismiss();
    }
    }
}
