package com.kawasolutions.Kawa;

import android.app.NotificationManager;
import android.content.Context;

/**
 * Created by apple on 2/19/17.
 */

public class NotificationFunction {

    public static void cancelNotification(Context context,String tag, Integer id){
        NotificationManager notificationManager = (NotificationManager) context.getSystemService(Context.NOTIFICATION_SERVICE);
        notificationManager.cancel(tag,id);
    }
}
