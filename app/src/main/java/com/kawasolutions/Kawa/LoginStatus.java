package com.kawasolutions.Kawa;

import android.content.Context;
import android.util.Log;

import com.kawasolutions.Kawa.Constants.SharedPrefKey;
import com.kawasolutions.Kawa.Utils.SharedPref;

/**
 * Created by apple on 4/22/16.
 */
public class LoginStatus {

    Context context;

    public LoginStatus(Context context){
        this.context = context;
    }

    public boolean isLoggedIn(){

        if(SharedPref.readFromPreference(context, SharedPrefKey.fileName,"pref_login","").equalsIgnoreCase("loggedIn")){
            return true;
        }
        else{
            return false;
        }
    }
}
