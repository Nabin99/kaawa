package com.kawasolutions.Kawa.Constants;

/**
 * Created by apple on 2/19/17.
 */

public class NotificationKeys {
    public static final String REQUEST_TAG = "request";
    public static final String GREETING_TAG = "greeting";
    public static final String RATE_REQUEST_TAG = "rateRequestTag";
    public static final Integer GREETING_ID = 1;
}
