package com.kawasolutions.Kawa.Constants;

/**
 * Created by apple on 4/20/16.
 */
public class URLS {

    //43.229.228.15:8080
    //103.198.9.215
    public static final String DOMAIN = "http://192.168.0.107:8080/";
//    public static final String DOMAIN = "http://kawarides.com:8080/";
    public static final String MAIN_URL = DOMAIN + "Kaawa/api/",
                loginPost = MAIN_URL + "login/customer/",
                registerPost = MAIN_URL + "register/customer/",
                registerToken = MAIN_URL +"notification/token_registration/",
                nearbyDrivers = MAIN_URL + "driver/drivers_in_area/",
                nearbyDriversExtended = MAIN_URL + "driver/drivers_in_area/extended_radius/",
                sendNotification = MAIN_URL + "notification/gcm_notification",
                checkEndRide = MAIN_URL + "ride/check_end_ride",
                updateDriverRating = MAIN_URL + "ride/driver_rating",
                rideActivityStatus = MAIN_URL + "ride/ride_activity_status/",
                cancelRide = MAIN_URL + "ride/cancel_ride",
                lastTrips = MAIN_URL + "customer/customer_last_trips/",
                unratedLastTrip = MAIN_URL + "customer/last_trip/",
                profileImageURL = DOMAIN + "Kaawa/images/",
                forgotPasswordURL = DOMAIN + "Kaawa/mail/forgot_password/",
                driverImageURL = DOMAIN + "Kaawa/images/",
                verifyNumber = MAIN_URL + "sparrow_sms/mobile_verification/",
                emergencyNotify = MAIN_URL + "emergency/police/",
                logout = MAIN_URL + "customer/logout/",
                checkAppVersion = MAIN_URL + "version/latest/1",
                supportData = MAIN_URL + "info/support",
                about = MAIN_URL + "info/about",
                addAppVersion = MAIN_URL + "",
                updateUserLocation = MAIN_URL + "customer/update_location/",
                activeRides=MAIN_URL+"customer/customer_active_trips/",
                sendNotificationToParticularDriver=MAIN_URL+"notification/gcm_notification_to_particular_driver",
                getServiceChargeAmount=MAIN_URL+"serviceCharge/getAmount";
                        ;
}
