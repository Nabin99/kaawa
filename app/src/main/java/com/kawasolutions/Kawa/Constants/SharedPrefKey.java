package com.kawasolutions.Kawa.Constants;

/**
 * Created by apple on 4/22/16.
 */
public class SharedPrefKey {

    public static final String fileName = "Kaawa";
    public static final String SENT_TOKEN_TO_SERVER = "";
    public static final String KEY_USERPROFILE = "userProfile";
    public static final String KEY_ON_RIDE = "onRide";
    public static final String KEY_RIDE_CUSTOMER_ID = "rideUserId";
    public static final String KEY_RIDE_STARTED = "rideStarted";
    public static final String KEY_RIDE_ENDED = "rideEnded";
    public static final String KEY_RIDE_CANCELED = "rideCancelled";
    public static final String KEY_RIDE_DETAILS = "rideDetails";
    public static final String KEY_RIDE_ID = "rideId";
    public static final String KEY_RATE_REQUEST = "rateRequest";
    public static final String KEY_RATE_DETAILS = "rateDetails";
    public static final String KEY_RATE_CUSTOMER_ID = "rateUserId";

    public static final String KEY_NEW_APP_UPDATE = "appUpdate";
    public static final String SHOW_UPDATE_PAGE = "updateNow";
    public static final String KEY_NEW_APP_VERSION = "newAppVersion";

    public static final String KEY_RIDE_INFO_WINDOW_SHOWN = "rideInfoWindow";
    public static final String KEY_RIDE_REQUEST = "rideRequest";

    public static final String KEY_NEXT_RIDE_CLICKED="nextRideClicked";


}
