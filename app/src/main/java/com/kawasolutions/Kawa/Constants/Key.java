package com.kawasolutions.Kawa.Constants;

import com.google.android.gms.maps.model.LatLng;

/**
 * Created by apple on 4/22/16.
 */
public class Key {

    public static final String success = "success";
    public static final String error = "error";
    public static boolean isNetworkConnected = false;
    public static LatLng DefaultCoordinates = new LatLng(27.7172,85.3240);
    public static Double SERVICE_CHARGE = 20.0;
    public static final int CAR_SIZE = 45;
}
