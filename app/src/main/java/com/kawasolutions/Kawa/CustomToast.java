package com.kawasolutions.Kawa;

import android.content.Context;
import android.graphics.Color;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.kawasolutions.Kawa.Enum.ToastType;

/**
 * Created by apple on 2/7/17.
 */

public class CustomToast {

    private static CustomToast toastInstance = null;

    private CustomToast(){

    }

    public void showToast(Context context, String message, ToastType type, int toastLength){

        LayoutInflater inflater = (LayoutInflater) context.getSystemService( Context.LAYOUT_INFLATER_SERVICE );
        // Call toast.xml file for toast layout
        View toastRoot = inflater.inflate(R.layout.custom_toast, null);
        ImageView imageView = (ImageView) toastRoot.findViewById(R.id.toast_image);
        TextView textView = (TextView) toastRoot.findViewById(R.id.toast_text);
        if(type == ToastType.SUCCESS){
            imageView.setImageDrawable(context.getResources().getDrawable(R.drawable.ic_success));
            textView.setTextColor(context.getResources().getColor(R.color.colorDarkGreen));
        } else if (type == ToastType.ERROR){
            imageView.setImageDrawable(context.getResources().getDrawable(R.drawable.ic_error));
            textView.setTextColor(context.getResources().getColor(R.color.colorDarkRed));
        } else if(type == ToastType.INFO){
            imageView.setImageDrawable(context.getResources().getDrawable(R.drawable.ic_info));
        } else {
            imageView.setVisibility(View.GONE);
        }
        textView.setText(message);

        Toast toast = new Toast(context);
        // Set layout to toast
        toast.setView(toastRoot);
        toast.setDuration(toastLength);
        toast.show();
    }

    public static CustomToast getInstance(){
        if(toastInstance == null){
            toastInstance =  new CustomToast();
        }
        return toastInstance;
    }
}
