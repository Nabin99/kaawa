package com.kawasolutions.Kawa.Singleton;

import java.util.Timer;

/**
 * Created by apple on 5/8/17.
 */

public class TimerSingleton {
    private static Timer timer = null;

    public static Timer getInstance(){
        if(timer == null){
            timer = new Timer();
        }
        return timer;
    }
}
