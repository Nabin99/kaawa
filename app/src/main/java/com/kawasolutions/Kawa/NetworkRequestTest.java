package com.kawasolutions.Kawa;

import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.support.annotation.Nullable;
import android.util.Log;
import android.view.WindowManager;

import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.kawasolutions.Kawa.Activities.LoginActivity;
import com.kawasolutions.Kawa.Constants.SharedPrefKey;
import com.kawasolutions.Kawa.Enum.TYPEREQUEST;
import com.kawasolutions.Kawa.Singleton.VolleySingleton;
import com.kawasolutions.Kawa.Utils.SharedPref;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;

/**
 * Created by apple on 4/27/17.
 */

public class NetworkRequestTest {

    public interface NetworkListenerTest {
        void receiveResult(JSONObject result, String url, Context context, TYPEREQUEST typeRequest,@Nullable String userInfo,@Nullable ProgressDialog progressDialog);

        void receiveError(VolleyError error, Context context, TYPEREQUEST typeRequest,@Nullable String userInfo,@Nullable ProgressDialog progressDialog);
    }

    private String url;
    @Nullable
    private HashMap<String, Object> parameters;
    @Nullable
    private HashMap<String, String> headers;
    private int method;
    private NetworkRequestTest.NetworkListenerTest listener;
    private Context context;
    private TYPEREQUEST typeRequest;
    @Nullable
    public  ProgressDialog progressDialog;
    private VolleySingleton volleySingleton;
    private RequestQueue requestQueue;
    @Nullable
    private String userInfo;

    public NetworkRequestTest(Context context, String url, int method,@Nullable HashMap<String,Object> parameters,@Nullable HashMap<String,String> headers,TYPEREQUEST typeRequest,@Nullable ProgressDialog progressDialog, @Nullable String userInfo) {
        this.context = context;
        this.url = url;
        this.method = method;
        this.typeRequest = typeRequest;
        this.progressDialog = progressDialog;
        this.parameters = parameters;
        this.headers = headers;
        this.userInfo = userInfo;
    }

    public void setListener(NetworkRequestTest.NetworkListenerTest listener) {
        this.listener = listener;
    }

    public void getResult() {
        volleySingleton = VolleySingleton.getInstance();
        requestQueue = volleySingleton.getRequestQueue();
        JsonObjectRequest jsonObjectRequest = null;
        JSONObject paramObject = null;

        if(parameters != null) {
            if (!parameters.isEmpty()) {
                paramObject = new JSONObject(parameters);
            }
        }

        if(parameters == null && headers == null){
            jsonObjectRequest = new JsonObjectRequest(method, url, new Response.Listener<JSONObject>() {
                @Override
                public void onResponse(JSONObject response) {
                    if (response != null) {
                        if (response.has("authError")){
                            try {
                                Integer error = response.getInt("authError");
                                if(error == 1){
                                    showAuthErrorAlert();
                                }
                            } catch (JSONException e) {
                                e.printStackTrace();
                            }
                        } else {
                            listener.receiveResult(response, url, context, typeRequest, userInfo, progressDialog);
                        }
                    } else {
                        Log.d("Volley REsponse:","Volley Response is null");
                    }
                }
            }, new Response.ErrorListener() {
                @Override
                public void onErrorResponse(VolleyError error) {
                    if (error != null) {
                        listener.receiveError(error, context, typeRequest,userInfo,progressDialog);
                    }
                }
            });

        } else if (parameters != null && headers == null){
            jsonObjectRequest = new JsonObjectRequest(method, url,paramObject, new Response.Listener<JSONObject>() {
                @Override
                public void onResponse(JSONObject response) {
                    if (response != null) {
                        if (response.has("authError")){
                            try {
                                Integer error = response.getInt("authError");
                                if(error == 1){
                                    showAuthErrorAlert();
                                }
                            } catch (JSONException e) {
                                e.printStackTrace();
                            }
                        } else {
                            listener.receiveResult(response, url, context, typeRequest, userInfo, progressDialog);
                        }
                    } else {
                        Log.d("Volley REsponse:","Volley Response is null");
                    }
                }
            }, new Response.ErrorListener() {
                @Override
                public void onErrorResponse(VolleyError error) {
                    if (error != null) {
                        listener.receiveError(error, context, typeRequest,userInfo,progressDialog);
                    }
                }
            });
        } else if (parameters != null && headers!= null){
            jsonObjectRequest = new JsonObjectRequest(method, url,paramObject, new Response.Listener<JSONObject>() {
                @Override
                public void onResponse(JSONObject response) {
                    if (response != null) {
                        if (response.has("authError")){
                            try {
                                Integer error = response.getInt("authError");
                                if(error == 1){
                                    showAuthErrorAlert();
                                }
                            } catch (JSONException e) {
                                e.printStackTrace();
                            }
                        } else {
                            listener.receiveResult(response, url, context, typeRequest, userInfo, progressDialog);
                        }                    } else {
                        Log.d("Volley REsponse:","Volley Response is null");
                    }
                }
            }, new Response.ErrorListener() {
                @Override
                public void onErrorResponse(VolleyError error) {
                    if (error != null) {
                        listener.receiveError(error, context, typeRequest,userInfo,progressDialog);
                    }
                }
            }) {
                @Override
                public Map<String, String> getHeaders() throws AuthFailureError {
                    return headers;
                }
            };
        } else if (parameters == null && headers!= null){
            jsonObjectRequest = new JsonObjectRequest(method, url, new Response.Listener<JSONObject>() {
                @Override
                public void onResponse(JSONObject response) {
                    if (response != null) {
                        if (response.has("authError")){
                            try {
                                Integer error = response.getInt("authError");
                                if(error == 1){
                                    showAuthErrorAlert();
                                }
                            } catch (JSONException e) {
                                e.printStackTrace();
                            }
                        } else {
                            listener.receiveResult(response, url, context, typeRequest, userInfo, progressDialog);
                        }                    } else {
                        Log.d("Volley REsponse:","Volley Response is null");
                    }
                }
            }, new Response.ErrorListener() {
                @Override
                public void onErrorResponse(VolleyError error) {
                    if (error != null) {
                        listener.receiveError(error, context, typeRequest,userInfo,progressDialog);
                    }
                }
            }) {
                @Override
                public Map<String, String> getHeaders() throws AuthFailureError {
                    return headers;
                }
            };
        } else {
            Log.d("Network Request Class","NetworkRequest Class Error");
        }

        if(jsonObjectRequest != null) {
            jsonObjectRequest.setRetryPolicy(new DefaultRetryPolicy(0,0,DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
            requestQueue.add(jsonObjectRequest);
            if (progressDialog != null) {
                progressDialog.show();
            }
        } else {
            Log.d("Network Request","JSON object request is null");
        }

    }

    private void showAuthErrorAlert(){
        //Clear user Info
        SharedPref.saveToPreferences(context, SharedPrefKey.fileName, SharedPrefKey.KEY_USERPROFILE, "");

        AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(context);
        alertDialogBuilder.setTitle("Session Expired");
        alertDialogBuilder.setMessage("Your session has expired.. Please login again to continue");
        alertDialogBuilder.setCancelable(false);

        alertDialogBuilder.setPositiveButton("OK", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.dismiss();

                Intent intent = new Intent(context, LoginActivity.class);
                intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_NEW_TASK);
                context.startActivity(intent);
            }
        });


        AlertDialog alertDialog = alertDialogBuilder.create();
        alertDialog.getWindow().setType(WindowManager.LayoutParams.TYPE_SYSTEM_ALERT);
        Log.d("Alert Dialog test:","test test");
        alertDialog.show();
    }

    @Override
    public String toString() {
        return "NetworkRequestTest{" +
                "url='" + url + '\'' +
                ", parameters=" + parameters +
                ", headers=" + headers +
                ", method=" + method +
                ", listener=" + listener +
                ", context=" + context +
                ", typeRequest=" + typeRequest +
                ", progressDialog=" + progressDialog +
                ", volleySingleton=" + volleySingleton +
                ", requestQueue=" + requestQueue +
                ", userInfo='" + userInfo + '\'' +
                '}';
    }
}
