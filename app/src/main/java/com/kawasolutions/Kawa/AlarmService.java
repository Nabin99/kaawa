package com.kawasolutions.Kawa;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.util.Log;

import com.kawasolutions.Kawa.Constants.SharedPrefKey;
import com.kawasolutions.Kawa.Utils.SharedPref;

/**
 * Created by apple on 3/6/17.
 */

public class AlarmService extends BroadcastReceiver {
    @Override
    public void onReceive(Context context, Intent intent) {
        SharedPref.saveToPreferences(context, SharedPrefKey.fileName,SharedPrefKey.SHOW_UPDATE_PAGE,true);
    }
}
