package com.kawasolutions.Kawa;

import android.Manifest;
import android.app.Service;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.os.Build;
import android.os.Bundle;
import android.os.IBinder;
import android.support.annotation.Nullable;

import android.provider.Settings;
import android.support.design.widget.Snackbar;
import android.support.v4.app.ActivityCompat;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.Button;
import android.widget.FrameLayout;
import android.widget.Toast;

import com.google.android.gms.maps.GoogleMap;
import com.kawasolutions.Kawa.Activities.Home;

/**
 * Created by apple on 4/25/16.
 */
public class GPSTracker {

    private Context context;
    public LocationManager locationManager;
    private LocationListener locationListener;
    private GoogleMap map;
    private FrameLayout fullLayout;


    public GPSTracker(){

    }

    public GPSTracker(Context context,GoogleMap map,FrameLayout fullLayout){
        this.context = context;
        this.map = map;
        this.fullLayout = fullLayout;

    }

    public void getLocation() {
        locationManager = (LocationManager) context.getSystemService(Context.LOCATION_SERVICE);

       // LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        //View view = inflater.inflate(R.layout.content_home,null,false);

        //fullLayout = (FrameLayout) view.findViewById(R.id.layout_frame);

        locationListener = new LocationListener() {
            @Override
            public void onLocationChanged(Location location) {
                Toast.makeText(context, "locationChanged", Toast.LENGTH_SHORT).show();
            }

            @Override
            public void onStatusChanged(String provider, int status, Bundle extras) {

            }

            @Override
            public void onProviderEnabled(String provider) {

            }

            @Override
            public void onProviderDisabled(String provider) {
                Snackbar snackBar = Snackbar.make(fullLayout,"GPS is Disabled",Snackbar.LENGTH_INDEFINITE);
                snackBar.setAction("Settings", new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        Intent intent = new Intent(Settings.ACTION_LOCATION_SOURCE_SETTINGS);
                        context.startActivity(intent);
                    }
                });
                snackBar.show();
            }
        };
        configurelocation();


        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            if (ActivityCompat.checkSelfPermission(context, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(context, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {

                ActivityCompat.requestPermissions((Home) context, new String[]{
                        Manifest.permission.ACCESS_FINE_LOCATION, Manifest.permission.ACCESS_COARSE_LOCATION, Manifest.permission.INTERNET
                }, 10);

                return;
            } else {
                configurelocation();
            }
        }
    }

    public void configurelocation(){
        locationManager.requestLocationUpdates("gps",6000,0,locationListener);
    }


}
